<footer class="main-footer">
    <div class="container-fluid constrained-width">
        <div class="row noMarginRow">
            <div class="col-lg-3 col-sm-6 link-box text-center-xs">
                <div class="footer-title">About Us</div>
                <div class="menu-item"><a href="{{ route('index') }}" title="About MyClassified">About MyClassified</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="MyClassified for Business">MyClassified for Business</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Our Partners">Our Partners</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Press Contact">Press Contact</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Careers">Careers</a></div>
            </div>
            <div class="col-lg-3 col-sm-6 link-box text-center-xs">
                <div class="footer-title">Help &amp; Contact</div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Frequently Asked Questions (FAQ's)">FAQ's</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Safety advices and information">Safety Advice</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="'Help me' guides">Help Me Guides</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Contact us">Contact Us</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Insurance information">Insurance</a></div>
            </div>
            <div class="col-lg-3 col-sm-12 link-box text-center-xs">
                <div class="footer-title">More From Us</div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Our blog">Blog</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Car price index">Car Price Index</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Car guides and more information">Car Guides - The Inside Track</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Upcycle revolution">Upcycle Revolution</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Advices for pet rehoming">Pet Rehoming Advice</a></div>
                <div class="menu-item"><a href="{{ route('index') }}" title="Popular searchess">Popular Searches</a></div>
            </div>
        </div><!-- Row ends here -->
    </div>
    <div class="divider"></div>
    <div class="container-fluid"
        <div class="row noMarginRow margin-bottom-small">
            <div class="col-xs-12">
                <div class="social-network">
                    <div class="social-network-title">Follow MyClassified</div>
                    <div class="social-network-content">
                        <a href="#" class="social facebook">Like</a><a href="#" class="social twitter">Tweet</a><a href="#" class="social pinterest">Pin It</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 site-info-container">
                <div class="site-info">
                    &copy; Copyright 2018 - <?php echo date('Y'); ?> MyClassified.co.uk Limited. All rights reserved.
                    <span class="site-info-links"><a href="{{ route('index') }}">Terms of use</a><span class="hidden-xs">, </span><a href="{{ route('index') }}">Privacy Notice</a><span class="hidden-xs"> &amp; </span><a href="{{ route('index') }}">Cookies Policy</a><span class="hidden-xs">.</span></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{asset('js/selector.js')}}"></script>
<script type="text/javascript" src="{{asset('js/classifieds.js')}}"></script>