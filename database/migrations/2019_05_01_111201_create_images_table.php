<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('type', 7)->default('gallery');
            $table->integer('adId')->unsigned();
            $table->foreign('adId')->references('adId')->on('ads')->onDelete('cascade');
        });
        DB::table('images')->insert( array( 'id' => 1,  'image' => '/img/public/ad1-img1.jpg', 'type' => 'cover',   'adId' => 1 ) );
        DB::table('images')->insert( array( 'id' => 2,  'image' => '/img/public/ad1-img2.jpg', 'type' => 'gallery', 'adId' => 1 ) );
        DB::table('images')->insert( array( 'id' => 3,  'image' => '/img/public/ad1-img3.jpg', 'type' => 'gallery', 'adId' => 1 ) );
        DB::table('images')->insert( array( 'id' => 4,  'image' => '/img/public/ad2-img1.jpg', 'type' => 'cover',   'adId' => 2 ) );
        DB::table('images')->insert( array( 'id' => 5,  'image' => '/img/public/ad2-img2.jpg', 'type' => 'gallery', 'adId' => 2 ) );
        DB::table('images')->insert( array( 'id' => 6,  'image' => '/img/public/ad3-img1.jpg', 'type' => 'cover',   'adId' => 3 ) );
        DB::table('images')->insert( array( 'id' => 7,  'image' => '/img/public/ad3-img2.jpg', 'type' => 'gallery', 'adId' => 3 ) );
        DB::table('images')->insert( array( 'id' => 8,  'image' => '/img/public/ad4-img1.jpg', 'type' => 'cover',   'adId' => 4 ) );
        DB::table('images')->insert( array( 'id' => 9,  'image' => '/img/public/ad4-img2.jpg', 'type' => 'gallery', 'adId' => 4 ) );
        DB::table('images')->insert( array( 'id' => 10, 'image' => '/img/public/ad5-img1.jpg', 'type' => 'cover',   'adId' => 5 ) );
        DB::table('images')->insert( array( 'id' => 11, 'image' => '/img/public/ad5-img2.jpg', 'type' => 'gallery', 'adId' => 5 ) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
