$(function(){
    $('.category-item.has-sub-category').click(function(){
        $(this).blur();
        // Remove styling from other items
        if( !$(this).hasClass('active') ) {
            $('.category-item').removeClass('active');
            $('.category-item-overlay').addClass('hidden');
        }
        // Add the styles to the selected item
        $('.category-item-overlay').toggleClass('hidden');
        $(this).toggleClass('active');
    });
    $('.category-item-overlay').click(function(){
        // Remove styling from other items
        $('.category-item.has-sub-category').removeClass('active');
        $('.category-item-overlay').addClass('hidden');
    });
    $('.menu-btn-toggle').click(function(){
        $('.header-dropdown-overlay').removeClass('hidden');
        $('.header-dropdown').addClass('open');
    });
    $('.header-dropdown-overlay').click(function(){
        $(this).addClass('hidden');
        $('.header-dropdown').removeClass('open');
    });
    $('.category-section-title-xs').click(function(){
        $('.category-section').toggleClass('on');
        $(this).toggleClass('on');
    });
    $('.top-info-title a').click(function(){
        var open = $(this).attr('dataopen');
        $(open).toggleClass('on');
        return false;
    });
    checkBox = $('input[type="checkbox"].theme-control');
    $(checkBox).each(function(){
        $(this).wrap( "<span class='theme-checkbox'></span>" );
        if($(this).is(':checked')){
            $(this).parent().addClass("selected");
        }
    });
    $(checkBox).click(function(){
        $(this).parent().toggleClass("selected");
    });

});
$(document).ready(function(){
    setTimeout(function(){
        $('body').removeAttr('class');
        $('html').removeAttr('class');
    }, 600);
});
/*!
 * Serialize all form data into a query string
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node}   form The form to serialize
 * @return {String}      The serialized form data
 */
function serialize(e){for(var n=[],t=0;t<e.elements.length;t++){var o=e.elements[t];if(o.name&&!o.disabled&&"file"!==o.type&&"reset"!==o.type&&"submit"!==o.type&&"button"!==o.type)if("select-multiple"===o.type)for(var p=0;p<o.options.length;p++)o.options[p].selected&&n.push(encodeURIComponent(o.name)+"="+encodeURIComponent(o.options[p].value));else("checkbox"!==o.type&&"radio"!==o.type||o.checked)&&n.push(encodeURIComponent(o.name)+"="+encodeURIComponent(o.value))}return n.join("&")}

function resetPasswordForm(){
    $('#change_password_form').reset();
    $('.error').html('');
}
function changePassword(e){
    var data = serialize( e );
    var action = e.getAttribute('action');
    $('.error').html('');
    $('.change-password-button').attr('disabled','disabled');
    $.ajax({
        url: action,
        type: 'POST',
        data: data,
        dataType: "json",
        success: function( data ) {
            e.reset();
            $('.password-updater').append('<div class="content norm text-success password-updated" style="font-weight: bold">' + data.message + '</div>');
            setTimeout(function(){
                $('.password-updated').remove();
            }, 5000);
            $('#_cp').modal('hide');
        },
        error: function( response ){
            data = JSON.parse( response.responseText );
            if( data.errors.old_password ) {
                $('#opw').select().focus();
                $('.pwd-error').html( data.errors.old_password );
            }
            if( data.errors.new_password ) {
                $('#npw').select().focus();
                $('.pwd-error').html( data.errors.new_password );
            }
            if( data.errors.con_password ) {
                $('#cpw').select().focus();
                $('.pwd-error').html( data.errors.con_password );
            }
            if( data.errors.no_user_login ) {
                window.location.reload();
            }
        },
        complete: function(){
            $('.change-password-button').removeAttr('disabled');
        }
    });
}

function addContactNumber(e){
    var data = serialize( e );
    var action = e.getAttribute('action');
    var cn = $('#contact_number').val();
    $('.cn-error').html('');
    $('.add-contact-number').attr('disabled','disabled');
    $.ajax({
        url: action,
        type: 'POST',
        data: data,
        dataType: "json",
        success: function( data ) {
            e.reset();
            $('.contact-number').append('<div class="content norm text-success contact-added" style="font-weight: bold">' + data.message + '</div>');
            setTimeout(function(){
                $('.contact-added').remove();
            }, 5000);
            $('#_an').modal('hide');
            $('#_un').modal('hide');
            $('.ocn').html( cn );
        },
        error: function( response ){
            data = JSON.parse( response.responseText );
            if( data.errors.contact_number != '' && data.errors.contact_number !== null && data.errors.contact_number != null ) {
                $('#contact_number').select().focus();
                $('.cn-error').html( data.errors.contact_number );
            }
            if( data.errors.no_user_login ) {
                window.location.reload();
            }
        },
        complete: function(){
            $('.add-contact-number').removeAttr('disabled');
        }
    });
}
