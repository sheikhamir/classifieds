<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['post']);
    }

    public function index() {
        $h1_title = 'Welcome to Homepage';
        //return view('pages.index', compact( 'h1_title' ));
        return view('pages.index')->with('title', $h1_title);
    }


    public function about() {
        return view('pages.about');
    }
    public function services() {
        $data = array(
            'title' => 'Our Services',
            'services' => 'We provide awesome gaming equipments!'
        );
        return view('pages.services')->with($data);
    }

    public function post() {
        $draftId = md5(uniqid());
        $data = array(
            'title' => 'Post an Ad',
            'draftId' => $draftId
        );
        return view('pages.adPost')->with($data);
    }
}
