<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    // Table name
    protected $table = 'ads';
    // Primary Key
    public $primaryKey = 'adId';
}
