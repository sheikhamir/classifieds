<div class="container-fluid search-bar search-bar-mobile">
    <div class="row text-center-xs constrained-width search-bar-mobile-inner">
        <!--<div class="col-md-10 col-md-offset-1">-->
        <div class="col-sm-8 col-sm-offset-2 no-padding">
            <div class="search-bar-content">
                <form class="search-bar-form" action="{{ route('search') }}" method="GET" name="mobile-form">
                    <input type="hidden" name="searchType" value="all"/>
                    <div class="search-bar-controls search-input">
                        <input name="searchKeywords" type="text" class="search-bar-input search-bar-group" placeholder="I'm looking for..." required="required" autocomplete="off" aria-autocomplete="off" value="<?php if(isset($_searchKeyword) && !empty($_searchKeyword)){echo $_searchKeyword;} ?>"/>
                    </div><div class="search-bar-controls postal-input">
                        <input name="postalCode" type="text" class="search-bar-input" placeholder="Postal code" autocomplete="off" aria-autocomplete="off" value="<?php if(isset($_postalCode) && !empty($_postalCode)){echo $_postalCode;} ?>"/>
                    </div><div class="search-bar-controls submit-btn">
                        <button type="submit" class="search-bar-submit-btn"><span class="hidden-lg">Search Now</span><span class="visible-lg-inline">Search</span></button></div>
                </form>
            </div>
        </div>
    </div>
</div>