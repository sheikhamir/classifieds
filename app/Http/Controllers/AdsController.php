<?php

namespace App\Http\Controllers;

use Faker\Provider\File;
use Illuminate\Http\Request;
use App\Ads;
use App\Categories;
use App\SubCategories;
use App\ExtraSubCategories;
use App\ExtraExtraSubCategories;

class AdsController extends Controller
{

    private $image_sizes;

    public function __construct()
    {
        $this->middleware('auth')->only(['post']);
        $this->image_sizes = ['normal', 'thumb-150x150', 'thumb-200x200', 200, 400]; // Only width
    }

    public function show($adSlug='')
    {
        // Fetch the ad data
        $adDetails = Ads::where('adSlug', $adSlug)->first();
        // Increase the number of views
        $this->viewed( $adDetails );
        // Load related ads
        $related = $this->related( $adDetails );
        // Data
        $data = [
            'ad' => $adDetails,
            'related' => $related
        ];
        // Render the view
        return view('pages.adView')->with( $data );
    }

    private function related( $ad )
    {
        // Related agendas
        $title = $ad->adTitle;
        $category = $ad->category;
        $sub_category = $ad->subCategory;
        $sub_sub_category = $ad->extraSubCategory;
        $sub_sub_sub_category = $ad->extraExtraSubCategory;

        $from_title = Ads::select( 'adId' )->where( function( $query ) use ( $title ) {
            $query->where( 'adTitle', 'LIKE', "%{$title}" )
                ->orWhere( 'adTitle', 'LIKE', "{$title}%" )
                ->orWhere( 'adTitle', 'LIKE', "%{$title}%" );
        })->where('live',1)->inRandomOrder()->offset(0)->limit(5)->get();
        $from_category = Ads::select( 'adId' )->where( function( $query ) use ( $category ) {
            $query->where( 'category', 'LIKE', "%{$category}" )
                ->orWhere( 'category', 'LIKE', "{$category}%" )
                ->orWhere( 'category', 'LIKE', "%{$category}%" );
        })->where('live',1)->inRandomOrder()->offset(0)->limit(5)->get();
        $merge = $from_title->merge( $from_category );
        $from_sub_category = Ads::select( 'adId' )->where( function( $query ) use ( $sub_category ) {
            $query->where( 'subCategory', 'LIKE', "%{$sub_category}" )
                ->orWhere( 'subCategory', 'LIKE', "{$sub_category}%" )
                ->orWhere( 'subCategory', 'LIKE', "%{$sub_category}%" );
        })->where('live',1)->inRandomOrder()->offset(0)->limit(5)->get();
        $merge = $merge->merge( $from_sub_category );
        if ( $sub_sub_category != '' ):
            $from_ss_category = Ads::select( 'adId' )->where( function( $query ) use ( $sub_sub_category ) {
                $query->where( 'extraSubCategory', 'LIKE', "%{$sub_sub_category}" )
                    ->orWhere( 'extraSubCategory', 'LIKE', "{$sub_sub_category}%" )
                    ->orWhere( 'extraSubCategory', 'LIKE', "%{$sub_sub_category}%" );
            })->inRandomOrder()->offset(0)->limit(5)->get();
            $merge = $merge->merge( $from_ss_category );
            if ( $sub_sub_sub_category != '' ):
                $from_sss_category = Ads::select( 'adId' )->where( function( $query ) use ( $sub_sub_sub_category ) {
                    $query->where( 'extraExtraSubCategory', 'LIKE', "%{$sub_sub_sub_category}" )
                        ->orWhere( 'extraExtraSubCategory', 'LIKE', "{$sub_sub_sub_category}%" )
                        ->orWhere( 'extraExtraSubCategory', 'LIKE', "%{$sub_sub_sub_category}%" );
                })->where('live',1)->inRandomOrder()->offset(0)->limit(5)->get();
                $merge = $merge->merge( $from_sss_category );
            endif;
        endif;

        $shuffled = $merge->shuffle()->slice(0, 10);

        return $shuffled;
    }

    private function viewed( $ad )
    {
        $views = intval( $ad->views ) + 1;
        $update_ad = Ads::where('adId', $ad->adId)
            ->update([ 'views' => $views ]);
        if ( $update_ad ):
            return true;
        else:
            return false;
        endif;
    }

    public function post(Request $request)
    {
        var_dump( $request->request );
    }

    public function image_upload(Request $request)
    {
        $rules = [
            'ad-images'   => 'required',
            'ad-images.*' => 'mimes:jpg,jpeg,png,bmp',
            'draft'       => 'required'
        ];
        $messages = [
            'ad-images.required'   => 'No ad images',
            'ad-images.mimes'      => 'You can only select .jpeg, .jpg, .png and .bmp images',
            'draft.required'       => 'Sorry for the inconvenience, please refresh the page and try again!'
        ];
        $this->validate( $request, $rules, $messages);

        // Generates a unique id from microseconds
        $time = uniqid();

        // Fetch draft id from ajax
        $adId = $request->draft;

        /** Upload path */
        $year        = date('Y');
        $month       = date('m');
        $base_path   = public_path() . '/user';
        $upload_path_p = "$base_path/$year";
        if ( !is_dir( $upload_path_p ) ):
            mkdir( $upload_path_p );
            create_empty_html_file( $upload_path_p );
        endif;
        $upload_path = "$upload_path_p/$month";
        if ( !is_dir( $upload_path ) ):
            mkdir( $upload_path );
            create_empty_html_file( $upload_path );
        endif;
        $url_path = "user/$year/$month";
        $i = 0;
        if( $request->hasfile( 'ad-images' ) ) {
            $outputImage = '';
            $images = array();
            foreach ( $request->file( 'ad-images' ) as $file ) {
                $i = $i + 1;
                $name_half = md5( $i.$adId );
                $file_path = $file->getPathName();
                $original_name = $file->getClientOriginalName(); // Original name
                $original_ext  = $file->extension(); // Original extension
                $name_base = $name_half . uniqid();
                foreach ( $this->image_sizes as $size ) {
                    $new_name = "$name_base-$size.jpg"; // New name
                    // Convert image to jpg
                    if (preg_match( '/jpg|jpeg/i', $original_ext ) )
                        $imageTmp = imagecreatefromjpeg( $file );
                    else if (preg_match( '/png/i', $original_ext ) )
                        $imageTmp = imagecreatefrompng( $file );
                    else if (preg_match( '/bmp/i', $original_ext ) )
                        $imageTmp = function_exists('imagecreatefrombmp') ? imagecreatefrombmp( $file ) : 'PHP 7.0 or above required!';
                    else
                        return 0;
                    // quality is a value from 0 (worst) to 100 (best)
                    $quality = 90;
                    $outputImage = "{$upload_path}/{$new_name}";
                    $image_url = "$url_path/$new_name";
                    if ( $size != 'normal' ) {
                        list( $width, $height ) = getimagesize( $file_path );
                        if ( preg_match( '/thumb/', $size ) ) {
                            // thumbnail-50x50
                            $dimensions = explode( 'x', explode( '-', $size )[1] );
                            $new_width = $dimensions[0];
                            $new_height = $dimensions[1];
                            $tmp = imagecreatetruecolor( $new_width, $new_height );
                            #imagecopy( $tmp, $imageTmp, 0, 0, $new_width/2, $new_height/2, $new_width, $new_height);
                            imagecopyresized( $tmp, $imageTmp, 0, 0, $new_width/2, $new_height/2, $new_width, $new_height, $width, $height);
                            /*imagecopy(
                                $dest,
                                $src,
                                0,    // 0x of your destination
                                0,    // 0y of your destination
                                50,   // middle x of your source
                                50,   // middle y of your source
                                30,  // 30px of width
                                20   // 20px of height
                            );*/
                            $images[] = url( $image_url );
                        } else {
                            $new_width = $size;
                            $new_height = ( $height / $width ) * $new_width;
                            $tmp = imagecreatetruecolor( $new_width, $new_height );
                            imagecopyresampled( $tmp, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
                            $images[] = url( $image_url );
                        }
                    } else {
                        unset( $tmp );
                        $images[] = url( $image_url );
                    }
                    if ( isset ( $tmp ) ) {
                        imagejpeg( $tmp, $outputImage, $quality );
                        imagedestroy( $imageTmp );
                        imagedestroy( $tmp );
                    } else {
                        imagejpeg( $imageTmp, $outputImage, $quality );
                        imagedestroy( $imageTmp );
                    }
                }
                //echo "{$outputImage}\n".'<br/>';
                //$file->move( public_path() . '/user/test/', $n_name );
                #$data[] = $n_name;
            }
            if ( $request->ajax() ) {
                header("Content-Type: text/json");
                return $images;
            }
        }

        #$file = new File();
        #$file->filenames = json_encode( $data );
        #$file->save();

        #return back()->with('success', 'Data Your files has been successfully added');
        #var_dump($request->file('ad-images'));
    }

    public function remove_image(Request $request)
    {
        $rules = [
            'image_name'   => 'required',
            'draft'        => 'required'
        ];
        $messages = [
            'image_name.required' => 'No image selected',
            'draft'               => 'Please refresh the page!'
        ];
        $this->validate( $request, $rules, $messages);

        // Generates a unique id from microseconds
        $time = uniqid();

        // Fetch draft id from ajax
        $adId = $request->draft;

        /** Upload path */
        $year        = date('Y');
        $month       = date('m');
        $base_path   = public_path() . '/user';
        $upload_path_p = "$base_path/$year";
        if ( !is_dir( $upload_path_p ) ):
            mkdir( $upload_path_p );
            create_empty_html_file( $upload_path_p );
        endif;
        $upload_path = "$upload_path_p/$month";
        if ( !is_dir( $upload_path ) ):
            mkdir( $upload_path );
            create_empty_html_file( $upload_path );
        endif;
        $url_path = "user/$year/$month";
        $i = 0;
        if( $request->hasfile( 'ad-images' ) ) {
            $outputImage = '';
            $images = array();
            foreach ( $request->file( 'ad-images' ) as $file ) {
                $i = $i + 1;
                $name_half = md5( $i.$adId );
                $file_path = $file->getPathName();
                $original_name = $file->getClientOriginalName(); // Original name
                $original_ext  = $file->extension(); // Original extension
                $name_base = $name_half . uniqid();
                foreach ( $this->image_sizes as $size ) {
                    // quality is a value from 0 (worst) to 100 (best)
                    $quality = 90;
                    $outputImage = "{$upload_path}/{$new_name}";
                }
            }
            if ( $request->ajax() ) {
                header("Content-Type: text/json");
                return $images;
            }
        }

        #$file = new File();
        #$file->filenames = json_encode( $data );
        #$file->save();

        #return back()->with('success', 'Data Your files has been successfully added');
        #var_dump($request->file('ad-images'));
    }

    public function draft(Request $request)
    {
        var_dump( $request->request );
    }

    public function cats(Request $request)
    {
        $error = false;
        $result = array();
        $rules = [
            'type'  => 'required',
            'value' => 'required'
        ];
        $messages = [
            'type.required'  => 'Please select a category type!',
            'value.required' => 'Please select a category!'
        ];

        $this->validate($request, $rules, $messages);

        switch ( $request->input('type') ) {
            case 'c':
                $type = 'category';
                $item = $this->is_category( $request['value'] );
                if ( !$item )
                    $error = true;
                $result = [
                    'categories' => Categories::all()
                ];
                break;
            case 'sc':
                $type = 'subCategory';
                $item = $this->is_sub_category( $request['value'] );
                if ( !$item )
                    $error = true;
                break;
            case 'ssc':
                $type = 'extraSubCategory';
                $item = $this->is_ss_category( $request['value'] );
                if ( !$item )
                    $error = true;
                break;
            case 'sssc':
                $type = 'extraExtraSubCategory';
                $item = $this->is_sss_category( $request['value'] );
                if ( !$item )
                    $error = true;
                break;
            default:
                $type = 'category';
                $item = $this->is_category( $request['value'] );
                if ( !$item )
                    $error = true;
                break;
        }

        if ( $error ) {
            $error_msg = 'There was an error!';
            $data = [
                'message' => 'The given data was invalid.',
                'errors' => [
                    $type => $error_msg
                ]
            ];
            if ( $request->ajax() ) {
                return response()->json($data, 422);
            } else {
                return back()->with('resent', true);
            }
        } else {
            return $result;
        }
    }


    /**
     * @param $slug
     * @return bool
     */
    public function is_category( $slug )
    {
        $result = Categories::where( 'slug', $slug );
        return $result->exists() ? $result->get()->categoryId : false;
    }

    /**
     * @param $sub_category_slug
     * @return bool
     */
    public function is_sub_category( $sub_category_slug )
    {
        $result = SubCategories::where( 'slug', $sub_category_slug );
        return $result->exists() ? $result->get()->subCategoryId : false;
    }

    /**
     * @param $sub_sub_category_slug
     * @return bool
     */
    public function is_ss_category( $sub_sub_category_slug )
    {
        $result = ExtraSubCategories::where( 'slug', $sub_sub_category_slug );
        return $result->exists() ? $result->get()->id : false;
    }

    /**
     * @param $sub_sub_sub_category_slug
     * @return bool
     */
    public function is_sss_category( $sub_sub_sub_category_slug )
    {
        $result = ExtraExtraSubCategories::where( 'slug', $sub_sub_sub_category_slug );
        return $result->exists() ? $result->get()->id : false;
    }

    /**
     * @param $category_id
     * @return bool
     */
    public function has_s_category( $category_id )
    {
        $result = SubCategories::where( 'categoryId', $category_id );
        return $result->exists() ? $result->get() : false;
    }

    /**
     * @param $sub_category_id
     * @return bool
     */
    public function has_ss_category( $sub_category_id )
    {
        $result = ExtraSubCategories::where( 'subCategoryId', $sub_category_id );
        return $result->exists() ? $result->get() : false;
    }

    /**
     * @param $sub_sub_category_id
     * @return bool
     */
    public function has_sss_category( $sub_sub_category_id )
    {
        $result = ExtraExtraSubCategories::where( 'escid', $sub_sub_category_id );
        return $result->exists() ? $result->get() : false;
    }
}
