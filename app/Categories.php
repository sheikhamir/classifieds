<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    // Table name
    protected $table = 'categories';
    // Primary Key
    public $primaryKey = 'categoryId';
}
