<!-- Sidebar -->
<div class="sidebar-wrapper">
    <ul class="sidebar-nav">

        <li><div class="divider"></div></li>

        <li><a href="{{ route('admin.dashboard') }}" title="Dashboard" class="{{ isset($activate_dashboard) ? $activate_dashboard : '' }}">
        <div class="icon item-info"><span class="glyphicon glyphicon-home"></span></div><div class="title item-info">Dashboard</div></a></li>

        <li><a href="{{ route('admin.dashboard') }}" title="Manage all ads posted" class="{{ isset($activate_ads) ? $activate_ads : '' }}">
        <div class="icon item-info"><span class="glyphicon glyphicon-bullhorn"></span></div><div class="title item-info">Manage Ads</div></a></li>

        <li><a href="{{ route('admin.dashboard') }}" title="Manage ad categories" class="{{ isset($activate_categories) ? $activate_categories : '' }}">
        <div class="icon item-info"><span class="glyphicon glyphicon-tags"></span></div><div class="title item-info">Categories</div></a></li>

        <li><a href="{{ route('admin.dashboard') }}" title="Searches overview" class="{{ isset($activate_searches) ? $activate_searches : '' }}">
        <div class="icon item-info"><span class="glyphicon glyphicon-search"></span></div><div class="title item-info">Searches</div></a></li>

        <li><a href="{{ route('admin.dashboard') }}" title="Users" class="{{ isset($activate_users) ? $activate_users : '' }}">
                <div class="icon item-info"><span class="material-icons">supervised_user_circle</span></div><div class="title item-info">Users</div></a></li>

        <li><a href="{{ route('admin.dashboard') }}" title="Site settings" class="{{ isset($activate_site_settings) ? $activate_site_settings : '' }}">
        <div class="icon item-info"><span class="material-icons">build</span></div><div class="title item-info">Site Settings</div></a></li>

        <li><div class="divider"></div></li>

        <li><a href="{{ route('admin.dashboard') }}" title="My profile" class="{{ isset($activate_my_profile) ? $activate_my_profile : '' }}">
        <div class="icon item-info"><span class="glyphicon glyphicon-cog"></span></div><div class="title item-info">My Profile</div></a></li>

        <li><a href="{{ route('logout') }}" title="Logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        <div class="icon item-info"><span class="material-icons">lock</span></div><div class="title item-info">Logout</div></a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        </li>

        <li><div class="divider"></div></li>

        <li><a href="javascript:void(0)" title="Collapse" id="menu-toggle">
        <div class="icon item-info"><span class="glyphicon glyphicon-expand"></span></div><div class="title item-info">Collapse</div></a></li>
    </ul>
</div>