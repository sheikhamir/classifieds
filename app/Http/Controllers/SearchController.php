<?php

namespace App\Http\Controllers;

use App\ExtraSubCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Search;
use App\Ads;
use App\AdsMeta;
use App\Categories;
use App\SubCategories;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /* Search keyword */
    public $keyword;
    /* Search category / type */
    public $category;
    /* Search sub-category (if exist) */
    public $sub_category;
    /* Search sub-sub-category (if exist)  */
    public $sub_sub_category;
    /* Search sub-sub-sub-category (if exist)  */
    public $sub_sub_sub_category;
    /* Search postal code */
    public $postal_code;
    /* Search tenant [user / guest] */
    public $tenant;
    /* Search query returned ads*/
    public $listings;
    /* Category selected or not [ bool ]*/
    public $category_selected;
    /* Sub-ategory selected or not [ bool ]*/
    public $sub_category_selected;
    /* Sub-sub-category selected or not [ bool ]*/
    public $sub_sub_category_selected;
    /* Sub-sub-sub-category selected or not [ bool ]*/
    public $sub_sub_sub_category_selected;
    /* Total number of ads returned by search query*/
    public $number_of_ads;
    /* Total number of ads with images returned by search query*/
    public $ads_with_images;
    /* Categories returned by search query */
    public $categories;
    /* Sub-categories returned by search query */
    public $sub_categories;
    /* Sub-sub-categories returned by search query */
    public $sub_sub_categories;
    /* Sub-sub-subcategories returned by search query */
    public $sub_sub_sub_categories;
    /* Checkbox to search for featured ads [true/false] */
    public $marked_featured;
    /* Checkbox to search for urgent ads [true/false] */
    public $marked_urgent;
    /* Ads which are on spotlight */
    public $marked_spotlight;
    /* Ads which have images */
    public $marked_with_images;
    /* Ads sorting options */
    public $sort_options;
    /* Ads sorting order [id desc / price asc / price desc] */
    public $sort_option_selected;
    /* Ads with price max to min */
    public $price_filter;
    public $price_min;
    public $price_max;
    /* Filters for featured, urgent and spotlighted ads */
    public $filters;

    public function __construct(Request $request)
    {
        /* Sort by */
        $this->sort_options = [
            'date-ascending' => 'Sort by: Most recent first', // This will be the default value
            'price-ascending' => 'Sort by: Price: Low to High',
            'price-descending' => 'Sort by: Price: High to Low'
        ];

        $this->sort_option_selected = ( isset( $_GET['sort'] ) AND !empty( $_GET['sort'] ) AND in_array( $_GET['sort'], array_flip( $this->sort_options ) ) ) ? $_GET['sort'] : array_values( array_flip( $this->sort_options ) )[0];

        /* Search keywords */
        if ( !isset( $request->_searchKeyword ) && $request->input('searchKeywords') !== NULL ) {
            $this->keyword = $request->input('searchKeywords');
        } elseif( isset( $request->_searchKeyword ) ) {
            $this->keyword = $request->_searchKeyword;
        } else {
            $this->keyword = '';
        }
        /* Search category */
        if ( isset ( $request->searchType ) ) {
            $this->category = $request->searchType;
        } else {
            $this->category = $request->input('searchType');
        }

        /* Check if search query has postal code */
        if ( isset ( $request->postalCode ) ) {
            $this->postal_code = $request->postalCode;
        } else {
            $this->postal_code = $request->input('postalCode');
        }

        $this->tenant = ( Auth::guest() ) ? 'guest' : Auth::user()->email;
        /** Search filters */
        // Filter: Show featured ads only [true / false]
        $this->marked_featured = $request->input( 'featured' ) !== null ? true : false;
        // Filter: Show urgent ads only [true / false]
        $this->marked_urgent = $request->input( 'urgent' ) !== null ? true : false;
        // Filter: Show Spotlight ads only [true / false]
        $this->marked_spotlight = $request->input( 'spotlight' ) !== null ? true : false;
        // Filter: Show ads with images only [true / false]
        $this->marked_with_images = $request->input( 'has-images' ) !== null ? true : false;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function search (Request $request, $category='', $sub_category='', $sub_sub_category='', $sub_sub_sub_category='')
    {
        #return strip_tags($this->keyword);
        /* Check if searching for category */
        $this->category = empty( $category ) ? 'all' : $category;
        /* Check if searching for sub-category */
        $this->sub_category = empty( $sub_category ) ? 'all' : $sub_category;
        /* Check if searching for sub-sub-category */
        $this->sub_sub_category = empty( $sub_sub_category ) ? 'all' : $sub_sub_category;
        /* Check if searching for sub-sub-sub-category */
        $this->sub_sub_sub_category = empty( $sub_sub_sub_category ) ? 'all' : $sub_sub_sub_category;
        $this->category_selected = ( $this->category == 'all' ) ? false : true;
        $this->sub_category_selected = ( $this->sub_category == 'all' ) ? false : true;
        $this->sub_sub_category_selected = ( $this->sub_sub_category == 'all' ) ? false : true;
        $this->sub_sub_sub_category_selected = ( $this->sub_sub_sub_category == 'all' ) ? false : true;
        // Log searches
        $this->log_search();
        // Start searching the table
        $this->init_search(20);

        return view('pages.search')
            ->with([
                'ads' => $this->listings,
                'cats' => $this->categories,
                'scats' => $this->sub_categories,
                'sscats' => $this->sub_sub_categories,
                'ssscats' => $this->sub_sub_sub_categories,
                '_searchKeyword' => $this->keyword,
                '_postalCode' => $this->postal_code,
                '_searchType' => $this->category,
                '_search_sub_category' => $this->sub_category,
                '_search_sub_sub_category' => $this->sub_sub_category,
                '_search_sub_sub_sub_category' => $this->sub_sub_sub_category,
                '_category_selected' => $this->category_selected,
                '_s_category_selected' => $this->sub_category_selected,
                '_ss_category_selected' => $this->sub_sub_category_selected,
                '_sss_category_selected' => $this->sub_sub_sub_category_selected,
                '_number_of_ads' => $this->number_of_ads,
                '_ads_with_images' => $this->ads_with_images,
                '_featuredAdsCount' => $this->filters->marked_featured,
                '_urgentAdsCount' => $this->filters->marked_urgent,
                '_spotlightAdsCount' => $this->filters->marked_spotlight,
                '_sortOptions' => $this->sort_options,
                '_sortOptionSelected' => $this->sort_option_selected
            ]);
    }
    public function show ($id)
    {
        // Fetch the post
        $post = Ads::find($id);
        //$post = Post::where('title', 'post one' )->get();
        return view('blog.show')->with('post', $post);
    }
    /**
     * Search Logs
     *
     * Log all the searches made by the user and the guest
     *
     */
    public function log_search ()
    {
        $keyword       = strip_tags( $this->keyword );
        $category      = strip_tags( $this->category );
        $postal_code   = ( $this->postal_code != null ) ? strip_tags( $this->postal_code ) : $this->postal_code;
        $tenant        = $this->tenant;
        $search_exists = Search::where( 'keywords', '=', $keyword )
                        ->where( 'searchType', '=', $category )
                        ->where( 'postalCode', '=', $postal_code )
                        ->where( 'user',       '=', $tenant )
                        ->first();
        if ( $search_exists === NULL ) { // Search does not exist
            // Insert the search in the database
            $search             = new Search();
            $search->keywords   = $keyword;
            $search->searchType = $category;
            $search->postalCode = $postal_code;
            $search->user       = $tenant;
            $search->save();
            return true;
        } else { // Search exists
            // Update the search in the database
            $searches         = $search_exists->searches + 1; // Increment number of searches by 1
            $search           = Search::find( $search_exists->id );
            $search->searches = $searches;
            $search->save();
            return true;
        }
    }

    public function init_search ( $paginate = 25 )
    {
        $keyword = $this->keyword;
        $selected_category = $this->category;
        $selected_sub_category = $this->sub_category;
        $selected_sub_sub_category = $this->sub_sub_category;
        $selected_sub_sub_sub_category = $this->sub_sub_sub_category;
        $postal_code = $this->postal_code;
        $sub_categories = false;
        $sub_sub_categories = false;
        $sub_sub_sub_categories = false;
        #$order_by_default = "select( 'adId' )->orderBy( 'featured', 'DESC' )->orderBy( 'spotlight', 'DESC' )->orderBy( 'urgent', 'DESC' )->orderBy( 'adId', 'DESC' )";
        // Find the ads related to the search term
        if( empty( $keyword ) && empty( $postal_code ) ) { // Search everything
            if ( select_all( $selected_category ) ) { // Category not specified
                $listings   = Ads::select( 'adId' )
                            ->orderBy( 'featured', 'DESC' )
                            ->orderBy( 'spotlight', 'DESC' )
                            ->orderBy( 'urgent', 'DESC' )
                            ->orderBy( 'adId', 'DESC' );
                $filters    = Ads::selectRaw( "COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight" );
                $withImage  = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" );
                $categories = Ads::select( DB::raw( 'DISTINCT category' ) )
                            ->orderBy( 'category', 'DESC' )
                            ->get();
            } else { // Category selected
                if ( select_all( $selected_sub_category ) ) { // Sub category not selected
                    $listings = Ads::select('adId')
                                ->where('category', $selected_category);
                    $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category);
                    $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                                ->where('category', $selected_category);
                    $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->orderBy('category', 'DESC')
                                ->get();
                    $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                } else { // Sub category selected
                    if ( select_all( $selected_sub_sub_category ) ) { // Sub sub category not selected
                        $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category);
                        $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category);
                        $withImage = Ads::selectRaw("COUNT(IF(withPicture='1','1', NULL)) as totalAds")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category);
                        $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->orderBy('category', 'DESC')
                                ->get();
                        $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                        $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                    } else { // Sub sub category selected
                        if ( select_all( $selected_sub_sub_sub_category ) ) { // Sub sub sub category not selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category);
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category);
                            $withImage = Ads::selectRaw("COUNT(IF(withPicture='1','1', NULL)) as totalAds")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category);
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                        } else { // Sub sub sub category selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category);
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category);
                            $withImage = Ads::selectRaw("COUNT(IF(withPicture='1','1', NULL)) as totalAds")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category);
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                        }
                    }
                }
            }
        } elseif ( !empty( $keyword ) && empty( $postal_code ) ) { // Search for query
            if ( $selected_category == 'all' ) { // Category not selected
                $listings = Ads::select( 'adId' )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" );
                            });
                $filters  = Ads::selectRaw( "COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight" )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" );
                            });
                $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" );
                            });
                $categories = Ads::select(DB::raw('DISTINCT category'))
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" );
                            })
                            ->orderBy('category', 'DESC')
                            ->get();
            } else { // Category selected
                if ( select_all( $selected_sub_category ) ) { // Sub category not selected
                    $listings = Ads::select('adId')
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('featured', 'DESC')
                        ->orderBy('spotlight', 'DESC')
                        ->orderBy('urgent', 'DESC')
                        ->orderBy('adId', 'DESC');
                    $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                        });
                    $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                        });
                    $categories = Ads::select(DB::raw('DISTINCT category'))
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('category', 'DESC')
                        ->get();
                    $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('subCategory', 'DESC')
                        ->get();
                } else { // Sub category selected
                    if ( select_all( $selected_sub_sub_category ) ) { // Sub sub category selected
                        $listings = Ads::select('adId')
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                            });
                        $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                            });
                        $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                            });
                        $categories = Ads::select(DB::raw('DISTINCT category'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('category', 'DESC')
                            ->get();
                        $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('subCategory', 'DESC')
                            ->get();
                        $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('extraSubCategory', 'DESC')
                            ->get();
                    } else { // Sub sub category selected
                        if ( select_all( $selected_sub_sub_sub_category ) ) { // Sub sub sub category not selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                });
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                });
                            $withImage = Ads::selectRaw("COUNT(IF(withPicture='1','1', NULL)) as totalAds")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                });
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                        } else { // Sub sub sub category selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                });
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                });
                            $withImage = Ads::selectRaw("COUNT(IF(withPicture='1','1', NULL)) as totalAds")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                });
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                        }
                    }
                }
            }
        } elseif ( empty( $keyword ) && !empty( $postal_code ) ) { // Search for postal code
            if ( select_all( $selected_category ) ) { // Category not selected
                $listings = Ads::select( 'adId' )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'postalCode', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                            })
                            ->orderBy( 'featured', 'DESC' )
                            ->orderBy( 'spotlight', 'DESC' )
                            ->orderBy( 'urgent', 'DESC' )
                            ->orderBy( 'adId', 'DESC' );
                $filters  = Ads::selectRaw( "COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight" )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'postalCode', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                            });
                $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                    ->where( function( $query ) use ( $keyword ) {
                        $query->where( 'postalCode', 'LIKE', "%{$keyword}" )
                            ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                            ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                    });
                $categories = Ads::select(DB::raw('DISTINCT category'))
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'postalCode', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                            })
                            ->orderBy( 'category', 'DESC' )
                            ->get();
            } else { // Category selected
                if ( select_all( $selected_sub_category ) ) { // Sub category not selected
                    $listings = Ads::select('adId')
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('featured', 'DESC')
                        ->orderBy('spotlight', 'DESC')
                        ->orderBy('urgent', 'DESC')
                        ->orderBy('adId', 'DESC');
                    $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        });
                    $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        });
                    $categories = Ads::select(DB::raw('DISTINCT category'))
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('category', 'DESC')
                        ->get();
                    $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('subCategory', 'DESC')
                        ->get();
                } else { // Sub category selected
                    if ( select_all( $selected_sub_sub_category ) ) { // Sub sub category not selected
                        $listings = Ads::select('adId')
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            });
                        $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            });
                        $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            });
                        $categories = Ads::select(DB::raw('DISTINCT category'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('category', 'DESC')
                            ->get();
                        $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('subCategory', 'DESC')
                            ->get();
                        $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('extraSubCategory', 'DESC')
                            ->get();
                    } else { // Sub sub category selected
                        if ( select_all( $selected_sub_sub_sub_category ) ) { // Sub sub sub category not selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                            $sub_sub_sub_categories = Ads::select(DB::raw('DISTINCT extraExtraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraExtraSubCategory', 'DESC')
                                ->get();
                        } else { // Sub sub sub category selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                            $sub_sub_sub_categories = Ads::select(DB::raw('DISTINCT extraExtraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraExtraSubCategory', 'DESC')
                                ->get();
                        }
                    }
                }
            }
        } else { // Search for query and postal code
            if ( select_all( $selected_category ) ) { // Category not selected
                $listings = Ads::select( 'adId' )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                            })
                            ->orderBy( 'featured', 'DESC' )
                            ->orderBy( 'spotlight', 'DESC' )
                            ->orderBy( 'urgent', 'DESC' )
                            ->orderBy( 'adId', 'DESC' );
                $filters  = Ads::selectRaw( "COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight" )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                            });
                $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                    ->where( function( $query ) use ( $keyword ) {
                        $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                            ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                            ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                            ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                            ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                            ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" )
                            ->orWhere( 'postalCode', 'LIKE', "%{$keyword}" )
                            ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                            ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                    });
                $categories = Ads::select( DB::raw('DISTINCT category') )
                            ->where( function( $query ) use ( $keyword ) {
                                $query->where( 'adTitle', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adTitle', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adTitle', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'adDescription', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'adDescription', 'LIKE', "%{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}" )
                                    ->orWhere( 'postalCode', 'LIKE', "{$keyword}%" )
                                    ->orWhere( 'postalCode', 'LIKE', "%{$keyword}%" );
                            })
                            ->orderBy( 'category', 'DESC' )
                            ->get();
            } else { // Category selected
                if ( select_all( $selected_sub_category ) ) { // Sub category not selected
                    $listings = Ads::select('adId')
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('featured', 'DESC')
                        ->orderBy('spotlight', 'DESC')
                        ->orderBy('urgent', 'DESC')
                        ->orderBy('adId', 'DESC');
                    $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        });
                    $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        });
                    $categories = Ads::select(DB::raw('DISTINCT category'))
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('category', 'DESC')
                        ->get();
                    $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                        ->where('category', $selected_category)
                        ->where(function ($query) use ($keyword) {
                            $query->where('adTitle', 'LIKE', "%{$keyword}")
                                ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                        })
                        ->orderBy('subCategory', 'DESC')
                        ->get();
                } else { // Sub category selected
                    if ( select_all( $selected_sub_sub_category ) ) { // Sub sub category not selected
                        $listings = Ads::select('adId')
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            });
                        $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            });
                        $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            });
                        $categories = Ads::select(DB::raw('DISTINCT category'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('category', 'DESC')
                            ->get();
                        $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('subCategory', 'DESC')
                            ->get();
                        $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                            ->where('category', $selected_category)
                            ->where('subCategory', $selected_sub_category)
                            ->where(function ($query) use ($keyword) {
                                $query->where('adTitle', 'LIKE', "%{$keyword}")
                                    ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                    ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                    ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                    ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                    ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                    ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                            })
                            ->orderBy('extraSubCategory', 'DESC')
                            ->get();
                    } else { // Sub sub category selected
                        if ( select_all( $selected_sub_sub_sub_category ) ) { // Sub sub sub category not selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT subCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                            $sub_sub_sub_categories = Ads::select(DB::raw('DISTINCT extraExtraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraExtraSubCategory', 'DESC')
                                ->get();
                        } else { // Sub sub sub category selected
                            $listings = Ads::select('adId')
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $filters = Ads::selectRaw("COUNT(IF(featured='1','1', NULL)) as marked_featured, COUNT(IF(urgent='1',1, NULL)) as marked_urgent, COUNT(IF(spotlight='1',1, NULL)) as marked_spotlight")
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $withImage = Ads::selectRaw( "COUNT(IF(withPicture='1','1', NULL)) as totalAds" )
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                });
                            $categories = Ads::select(DB::raw('DISTINCT category'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('category', 'DESC')
                                ->get();
                            $sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraSubCategory', 'DESC')
                                ->get();
                            $sub_sub_categories = Ads::select(DB::raw('DISTINCT extraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('subCategory', 'DESC')
                                ->get();
                            $sub_sub_sub_categories = Ads::select(DB::raw('DISTINCT extraExtraSubCategory'))
                                ->where('category', $selected_category)
                                ->where('subCategory', $selected_sub_category)
                                ->where('extraSubCategory', $selected_sub_sub_category)
                                ->where('extraExtraSubCategory', $selected_sub_sub_sub_category)
                                ->where(function ($query) use ($keyword) {
                                    $query->where('adTitle', 'LIKE', "%{$keyword}")
                                        ->orWhere('adTitle', 'LIKE', "{$keyword}%")
                                        ->orWhere('adTitle', 'LIKE', "%{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}")
                                        ->orWhere('adDescription', 'LIKE', "{$keyword}%")
                                        ->orWhere('adDescription', 'LIKE', "%{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}")
                                        ->orWhere('postalCode', 'LIKE', "{$keyword}%")
                                        ->orWhere('postalCode', 'LIKE', "%{$keyword}%");
                                })
                                ->orderBy('extraExtraSubCategory', 'DESC')
                                ->get();
                        }
                    }
                }
            }
        }
        // Only fetch live ads
        $listings->where( 'live', 1 );
        $categories->where( 'live', 1 );
        if ( $sub_categories != false ) $sub_categories->where( 'live', 1 );
        if ( $sub_sub_categories != false ) $sub_sub_categories->where( 'live', 1 );
        if ( $sub_sub_sub_categories != false ) $sub_sub_sub_categories->where( 'live', 1 );
        $filters->where( 'live', 1 );
        $withImage->where( 'live', 1 );

        if ( $this->marked_featured ) {
            $listings->where( 'featured', '1' );
            $categories->where( 'featured', '1' );
            if ( $sub_categories != false ) $sub_categories->where( 'featured', '1' );
            if ( $sub_sub_categories != false ) $sub_sub_categories->where( 'featured', '1' );
            if ( $sub_sub_sub_categories != false ) $sub_sub_sub_categories->where( 'featured', '1' );
            $filters->where( 'featured', '1' );
            $withImage->where( 'featured', '1' );
        }
        if ( $this->marked_urgent ) {
            $listings->where( 'urgent', '1' );
            $categories->where( 'urgent', '1' );
            if ( $sub_categories != false ) $sub_categories->where( 'urgent', '1' );
            if ( $sub_sub_categories != false ) $sub_sub_categories->where( 'urgent', '1' );
            if ( $sub_sub_sub_categories != false ) $sub_sub_sub_categories->where( 'urgent', '1' );
            $filters->where( 'urgent', '1' );
            $withImage->where( 'urgent', '1' );
        }
        if ( $this->marked_spotlight ) {
            $listings->where( 'spotlight', '1' );
            $categories->where( 'spotlight', '1' );
            if ( $sub_categories != false ) $sub_categories->where( 'spotlight', '1' );
            if ( $sub_sub_categories != false ) $sub_sub_categories->where( 'spotlight', '1' );
            if ( $sub_sub_sub_categories != false ) $sub_sub_sub_categories->where( 'spotlight', '1' );
            $filters->where( 'spotlight', '1' );
            $withImage->where( 'spotlight', '1' );
        }

        /*'date-ascending' => 'Most recent first', // This will be the default value
            'price-ascending' => 'Price: Low to High',
            'price-descending' => 'Price: High to Low'*/

        // Order
        $listings->orderBy('featured', 'DESC');
        switch ( $this->sort_option_selected ) {
            case "price-ascending":
                $listings->orderBy( 'adAskingPrice', 'ASC' );
                break;

            case "price-descending":
                $listings->orderBy( 'adAskingPrice', 'DESC' );
                break;
            default:
                $listings->orderBy( 'adId', 'DESC' );
                break;
        }
        $listings->orderBy('urgent', 'DESC')->orderBy('spotlight', 'DESC');

        $this->number_of_ads = $listings->get()->count();
        $this->ads_with_images = $withImage->first()->totalAds;
        $this->listings = $listings->paginate( $paginate );
        $this->categories = $categories;
        $this->sub_categories = $sub_categories;
        $this->sub_sub_categories = $sub_sub_categories == false ? false : $sub_sub_categories;
        $this->sub_sub_sub_categories = $sub_sub_sub_categories == false ? false : $sub_sub_categories;
        $this->filters = $filters->first();
    }

    function select_all ( $item ) {
        return ( !empty( $item ) && $item == 'all' ) ? true : false;
    }
}
