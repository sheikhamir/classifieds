<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('categoryId');
            $table->string('name');
            $table->string('slug');
            $table->string('info');
        });
        DB::table('categories')->insert(
            array(
                'categoryId' => 1,
                'name' => 'Motors',
                'slug' => 'motors',
                'info' => 'Vehicles &amp; parts'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 2,
                'name' => 'For Sale',
                'slug' => 'for-sale',
                'info' => 'Everyday items'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 3,
                'name' => 'Properties',
                'slug' => 'properties',
                'info' => 'Homes for all'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 4,
                'name' => 'Jobs',
                'slug' => 'jobs',
                'info' => 'Discover your next role'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 5,
                'name' => 'Services',
                'slug' => 'services',
                'info' => 'From trades to tutors'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 6,
                'name' => 'Community',
                'slug' => 'community',
                'info' => 'Bands, clubs &amp; classes'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 7,
                'name' => 'Pets',
                'slug' => 'pets',
                'info' => 'Find a cute pet'
            )
        );
        DB::table('categories')->insert(
            array(
                'categoryId' => 8,
                'name' => 'Dating',
                'slug' => 'dating',
                'info' => 'Find a partner'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
