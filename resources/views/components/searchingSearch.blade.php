<div class="row text-center-xs constrained-width search-bar searching">
    <!--<div class="col-md-10 col-md-offset-1">-->
    <div class="col-sm-8 col-sm-offset-2 no-padding">
        <div class="search-bar-content">
            <form class="search-bar-form" action="{{ route('search') }}" method="GET">
                <div class="search-bar-controls search-input">
                    <div class="search-bar-group-container">
                        <div class="custom-select search-bar-group selector">
                            <select name="searchType">
                                <option value="all">All</option><?php
                                $_searchType = isset( $_searchType ) ? $_searchType : false;
                                render_search_categories_as_options( $_searchType );
                                ?>
                            </select>
                        </div>
                        <input name="searchKeywords" type="text" class="search-bar-input search-bar-group" placeholder="I'm looking for..." required="required" value="<?php if(isset($_searchKeyword) && !empty($_searchKeyword)){echo $_searchKeyword;} ?>"/>
                    </div>
                </div><div class="search-bar-controls postal-input">
                    <input name="postalCode" type="text" class="search-bar-input" placeholder="Postal code / Location" value="<?php if(isset($_postalCode) && !empty($_postalCode)){echo $_postalCode;} ?>"/>
                </div><div class="search-bar-controls submit-btn">
                    <button type="submit" class="search-bar-submit-btn"><span class="hidden-lg">Search Now</span><span class="visible-lg-inline">Search</span></button></div>
            </form>
        </div>
    </div>
</div>