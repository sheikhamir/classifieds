<?php
use App\Categories;
use App\PostalCodes;
use App\Images;
use App\User;
?>
@extends('layouts.search')
<?php
if ( isset( $ad ) ){
    $title = $ad->adTitle . " - " . config('app.name');
    $photos = Images::where( 'adId', $ad->adId )->get();
    $user = User::where( 'id', $ad->userId )->first();
    $fullname = $user->name;
    $email = $user->email;
    if (  preg_match( '/^\+\d(\d{4})(\d{4})(\d{3})$/', $user->phone,  $matches ) ) {
        $phone = '+' . $matches[1] . '-' .$matches[2] . '-' . $matches[3];
    } else {
        $phone = 'No phone number';
    }
    $phone = $user->phone;
    $postalCodeDetail = PostalCodes::where( 'postalCode',$ad->postalCode )->first();
    $city = $postalCodeDetail->city;
    $country = $postalCodeDetail->country;
    $location = "{$city}, {$country}";

    $description = $ad->adDescription;
    //$description = preg_replace( "/\n/", "<br/>", $description );
    $description = preg_replace( '/(\n)+(\n)/', "<br/><br/>", $description );
    $description = preg_replace( '/(\n)+/', "<br/>", $description );

    $category = catName( $ad->category );
    $subCategory = catName( $ad->subCategory, 'subcat' );
    $extraSubCategory = catName( $ad->extraSubCategory, 'extracat' );
    ?>
    @section('title')
    {{ $title }}
    @endsection

    @section('body-content')
    <div class="container-fluid content-container ad-view-container constrained-width">
        <div class="row noMarginRow ad-view">
            <div class="breadcrumb-container">
                <ol class="breadcrumb">
                  <li class="back-button"><a href="{{ url()->previous() }}" class="back"><i class="glyphicon glyphicon-menu-left"></i>Back</a></li>
                  <li class="hidden-xs"><a href="{{ route('index') }}">Home</a></li>
                  <li class="hidden-xs"><a href="{{ route('search.category', ['searchType' => $ad->category]) }}">{{ $category }}</a></li>
                  <li class="hidden-xs"><a href="{{ route('search.subcategory', ['searchType' => $ad->category, 'subCategory' => $ad->subCategory]) }}">{{ $subCategory }}</a></li>
                  <?php if ( !empty( $extraSubCategory ) && $extraSubCategory != 'all' ) { ?>
                  <li class="hidden-xs"><a href="{{ route('search.sub-subcategory', ['searchType' => $ad->category, 'subCategory' => $ad->subCategory, 'extraSubCategory' => $ad->extraSubCategory]) }}">{{ $extraSubCategory }}</a></li>
                  <?php } ?>
                </ol>
            </div>
            <div class="col-lg-8 col-md-8 ad-view-details ad-content">
                <div class="item ad-title">{{ $ad->adTitle }}</div>
                <div class="item ad-location">{{ $location }}</div>
                <div class="item price">
                    <div class="price">&pound;{{ number_format( $ad->adAskingPrice, 2 ) }}</div>
                </div>
                <div class="item ad-gallery">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"/>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
                    <div class="fotorama" data-nav="thumbs" data-width="100%" data-ratio="800/600" data-keyboard="true" data-allowfullscreen="true"><?php
                        foreach( $photos as $photo ) {
                            $photo_url = $photo->image;
                            $photo_alt = $ad->adTitle;
                            ?><img src="{{ asset( $photo_url ) }}" alt="{{ $photo_alt }}"><?php
                        }
                    ?></div>
                </div>
                <div class="item posted">
                Posted {{ $ad->created_at }}
                </div>
                <div class="item description">
                    <div class="title">Description</div>
                    <div class="content"><?php echo $description; ?></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 ad-view-details seller-details-container">
                <div class="seller-details">
                    <div class="item name">
                        <div>{{ $user->name }}</div>
                        <div class="link-container"><a href="#" class="link">User's other ads</a></div>
                    </div>
                    <div class="item user-since">Posting since {{ explode(' ', $user->created_at)[0] }}</div>
                    <div class="item contact-heading">Contact {{ $user->name }}</div>
                    <div class="item contact-phone"><i class="glyphicon glyphicon-earphone"></i> {{ $phone }}</div>
                    <div class="item ad-actions-container">
                        <button type="button" class="btn btn-group btn-default"><i class="glyphicon glyphicon-heart"> </i> Favourite</button>
                        <button type="button" class="btn btn-group btn-default"><i class="glyphicon glyphicon-warning-sign"> </i> Report</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.topinfo')
    @endsection
<?php } else { ?>@include('includes.adNotFound') <?php } ?>