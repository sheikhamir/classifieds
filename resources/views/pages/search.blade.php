<?php
use Illuminate\Support\Facades\Cookie;
use App\PostalCodes;
$AppName = config('app.name');
$search = isset($_searchKeyword) && !empty($_searchKeyword) ? "{$_searchKeyword} - $AppName" : "Search - $AppName";
$resultsFor = result_info( $_searchKeyword, $_searchType, $_search_sub_category, $_search_sub_sub_category, $_search_sub_sub_sub_category, $_postalCode, $_number_of_ads );
$ads_found = ( isset( $cats ) && count( $cats ) > 0 ) && ( isset( $ads ) && count( $ads ) > 0 );
?>@extends('layouts.app')
@section('title')
{{ $search }}
@endsection

@section('body-content')
@if( $ads_found )
<div class="container-fluid category-section searching hidden-xs">
    @include('components.desktopSearch')
</div>
@else
<div class="category-item-overlay hidden"></div>
<div class="container-fluid category-section hidden-xs">
    @include('components.desktopSearch')
    @include('components.categories')
</div>
@endif
@include('components.mobileSearch')
<div class="container-fluid content-container">
    <?php if ( $ads_found ): // If ads are found ?>
    <div class="row noMarginRow search-ads-container"><div class="col-lg-12 search-term ">
            {{ $resultsFor }}
            <div class="pull-right display-block-xs">
            <form action="" method="GET" id="sortForm">
            @csrf
            <?php if ( get_exists( 'searchKeywords' ) ): ?>
            <input type="hidden" name="searchKeywords" value="{{ $_searchKeyword }}"/>
            <?php endif; ?>
            <?php if ( get_exists( 'postalCode' ) ): ?>
            <input type="hidden" name="postalCode" value="{{ $_postalCode }}"/>
            <?php endif; ?>
            <?php if ( get_exists( 'page' ) ): ?>
                <input type="hidden" name="page" value="{{ $_GET['page'] }}"/>
            <?php endif; ?>
            <?php if ( get_exists( 'featured' ) ): ?>
                <input type="hidden" name="featured" value="1"/>
            <?php endif; ?>
            <?php if ( get_exists( 'urgent' ) ): ?>
                <input type="hidden" name="urgent" value="1"/>
            <?php endif; ?>
            <?php if ( get_exists( 'has-images' ) ): ?>
                <input type="hidden" name="has-images" value="1"/>
            <?php endif; ?>
            <select class="form-control" name="sort" id="sort" onchange="document.getElementById('sortForm').submit();"><?php
            if ( isset( $_sortOptions ) ):
                foreach( $_sortOptions as $key => $value ) {
                    if ( $key == $_sortOptionSelected ):
                        echo "<option value=\"$key\" selected=\"selected\">$value</option>";
                    else:
                        echo "<option value=\"$key\">$value</option>";
                    endif;
                }
            endif;
            ?></select></form></div>
            <br class="clear visible-xs visible-xxs"/>
        </div>
        <div class="hidden-xs col-sm-12 col-md-3 col-lg-2 search-filters-container">
            <div class="filter-wrapper hidden-xs">
                <a class="btn btn-block no-radius btn-pink search-action-btn" href="#"><span class="material-icons">add_circle</span> Save search</a>
            </div>
            <div class="filter-wrapper categories-wrapper hidden-sm">
                <div class="title"><span class="material-icons"></span>Categories</div>
                <div class="content"><?php
                    // Categories
                    if ( !$_s_category_selected ) {
                        if( count( $cats ) > 1 ) {
                            echo "<div class='item'><span class='active'>All Categories</span></div>";
                            foreach( $cats as $cat ) {
                                $category_slug = $cat->category;
                                $category = catName( $category_slug );
                                #$catLink = searchQuery( 'category', $category_slug );
                                $catLink = route( 'kw.c', [ 'searchType' => $category_slug, 'searchKeywords' => $_searchKeyword ] );
                                echo "<div class='item wu'><a href=\"{$catLink}\"><span class='glyphicon glyphicon-chevron-right'></span> $category</a></div>";
                            }
                        } elseif( count( $cats ) != 0 ) {
                            $category_slug = $cats[0]->category;
                            $category = catName( $category_slug );
                            $all_categories = route( 'search.basic', [ 'searchType' => 'all', 'searchKeywords' => $_searchKeyword ] );
                            echo "<div class='item'><a href=\"{$all_categories}\"><span class=\"glyphicon glyphicon-chevron-left\"> </span> All Categories</a></div>";
                            echo "<div class='item'><span class='active'>$category</span></div>";
                        }
                    } else {
                        $category_slug = $cats[0]->category;
                        $category = catName( $category_slug );
                        $all_categories = route( 'kw.c', [ 'searchType' => 'all', 'searchKeywords' => $_searchKeyword ] );
                        $category_link = route( 'kw.c', [ 'searchType' => $category_slug, 'searchKeywords' => $_searchKeyword ] );
                        echo "<div class='item'><a href=\"{$all_categories}\"><span class=\"glyphicon glyphicon-chevron-left\"> </span> All Categories</a></div>";
                        echo "<div class='item'><a href=\"{$category_link}\"><span class=\"glyphicon glyphicon-chevron-left\"> </span> $category</a></div>";
                    }
                    // Sub categories
                    if (! $_ss_category_selected ) {
                        if ( isset( $scats ) && $scats != false ) {
                            if( count( $scats ) > 0 ) {
                                if ( $_search_sub_category == 'all' ) {
                                    foreach( $scats as $scat ) {
                                        $category_slug = $scat->subCategory;
                                        $category = catName( $category_slug, 'subcat' );
                                        $catLink = route( 'kw.sc', [ 'searchType' => $_searchType, 'subCategory' => $category_slug, 'searchKeywords' => $_searchKeyword ] );
                                        echo "<div class='item wu'><span class='glyphicon glyphicon-chevron-right'></span> <a href=\"{$catLink}\">$category</a></div>";
                                    }
                                } else {
                                    $category_slug = $scats[0]->subCategory;
                                    $category = catName( $category_slug, 'subcat' );
                                    echo "<div class='item'><span class='active'>$category</span></div>";
                                }
                            }
                        }
                    } else {
                        $category_slug = $scats[0]->subCategory;
                        $category = catName( $category_slug, 'subcat' );
                        $category_link = route( 'kw.sc', [ 'searchType' => $_searchType, 'subCategory' => $category_slug, 'searchKeywords' => $_searchKeyword ] );
                        echo "<div class='item'><a href=\"{$category_link}\"><span class=\"glyphicon glyphicon-chevron-left\"> </span> $category</a></div>";
                    }
                    // Sub sub categories
                    if ( isset( $sscats ) && $sscats != false ) {
                        if( count( $sscats ) > 0 ) {
                            if ( $_search_sub_sub_category == 'all' ) {
                                foreach( $sscats as $sscat ) {
                                    $category_slug = $sscat->extraSubCategory;
                                    if ( !empty( $category_slug ) ) {
                                        $category = catName( $category_slug, 'extracat' );
                                        //$catLink = searchQuery( 'sub-category', $category_slug );
                                        $catLink = route( 'kw.ssc', [ 'searchType' => $_searchType, 'subCategory' => $_search_sub_category, 'extraSubCategory' => $category_slug, 'searchKeywords' => $_searchKeyword ] );
                                        echo "<div class='item wu'><span class='glyphicon glyphicon-chevron-right'></span> <a href=\"{$catLink}\">$category</a></div>";
                                    }
                                }
                            } else {
                                $category_slug = $sscats[0]->extraSubCategory;
                                $category = catName( $category_slug, 'extracat' );
                                echo "<div class='item'><span class='active'>$category</span></div>";
                            }
                        }
                    }
                ?></div>
            </div>
            <div class="filter-wrapper filters hidden-sm">
                <div class="title">Filter</div>
                <div class="content">
                    <div class="sub-filter">
                        <div class="title">Price</div>
                        <div class="content price">
                            <div class="item">
                                <input type="number" class="form-control input-lg" placeholder="Min"/><input type="number" class="form-control input-lg" placeholder="Max"/><button class="btn btn-default btn-lg btn-pink">Go</button>
                            </div>
                        </div>
                    </div>
                    <div class="sub-filter text-primary">
                        <div class="title">Other options</div>
                        <div class="content checkbox-filters"><?php
                        if ( isset ( $_featuredAdsCount ) && $_featuredAdsCount > 0 ) {
                            $active = isset( $_GET['featured'] ) ? ' checked="checked"' : '';
                            $class = isset( $_GET['featured'] ) ? ' active"' : '';
                            echo '<div class="item"><label for="featured" class="'.$class.'"><input type="checkbox" class="theme-control" '.$active.' id="featured" name="featured" onclick="window.location=\''. make_url( $_searchKeyword, $_searchType, $_search_sub_category, $_search_sub_sub_category, $_search_sub_sub_sub_category, $_postalCode, 'featured' ) . '\'"/> Featured Ads <span class="number">' . $_featuredAdsCount . '</span></label></div>';
                        }
                        if ( isset ( $_urgentAdsCount ) && $_urgentAdsCount > 0 ) {
                            $active = isset( $_GET['urgent'] ) ? ' checked="checked"' : '';
                            $class = isset( $_GET['urgent'] ) ? ' active"' : '';
                            echo '<div class="item"><label for="urgent" class="'.$class.'"><input type="checkbox" class="theme-control" '.$active.' id="urgent" name="urgent" onclick="window.location=\''. make_url( $_searchKeyword, $_searchType, $_search_sub_category, $_search_sub_sub_category, $_search_sub_sub_sub_category, $_postalCode, 'urgent' ) . '\'"/> Urgent Ads <span class="number">' . $_urgentAdsCount . '</span></label></div>';
                        }
                        if ( isset ( $_ads_with_images ) && intval( $_ads_with_images ) > 0 ){
                            $active = isset( $_GET['has-images'] ) ? ' checked="checked"' : '';
                            $class = isset( $_GET['has-images'] ) ? ' active"' : '';
                            echo '<div class="item"><label for="with_images" class="'.$class.'"><input type="checkbox" class="theme-control" '.$active.' id="with_images" name="with_images" onclick="window.location=\''. make_url( $_searchKeyword, $_searchType, $_search_sub_category, $_search_sub_sub_category, $_search_sub_sub_sub_category, $_postalCode, 'has-images' ) . '\'"/> Ads with images <span class="number">' . $_ads_with_images . '</span></label></div>';
                        }
                        ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-9 col-lg-10 search-ads"><?php
            foreach( $ads as $adInfo ){
                // Fetch ad details
                $ad = fetch_ad( $adInfo->adId );
                $adSlug = $ad->adSlug;
                $adUrl = app('url')->to( "/ads/$adSlug" );
                $adTitle = $ad->adTitle;
                $adDesc = $ad->adDescription;
                $dateAdded = time_elapsed_string( $ad->created_at, true );
                $adRawCategory = $ad->category;
                $adRawSubCategory = $ad->subCategory;
                $adRawExtraSubCategory = $ad->extraSubCategory;
                // Fetch ad cover image
                $imageInfo = fetch_cover( $adInfo->adId );
                $displayImage = app( 'url' )->to( $imageInfo->image );
                $featured_class = $ad->featured == 1 ? ' featured-ad' : '';
                $urgent_class = $ad->urgent == 1 ? ' urgent-ad' : '';
                $urgent_msg = '<div class="urgent-time">URGENT</div>';
                ?><a href="{{ $adUrl }}" class="ad-item{{ $featured_class }}{{ $urgent_class }}">
                    <div class="ad-details ad-image-container">
                        <span><div class="ad-image" style="background-image:url({{ $displayImage }})"></div></span>
                    </div>
                    <div class="ad-details ad-details-container">
                        <div class="ad-title"><span>{{ $adTitle }}</span></div>
                        <div class="ad-desc hidden-xs hidden-sm">{{ summarise($adDesc, 220) }}</div>
                        <div class="ad-price"><?php echo number_format( $ad->adAskingPrice, 2 ); ?></div>
                        <div class="ad-date"><span class="hidden-xs"></span><?php echo $ad->urgent == 1 ? $urgent_msg : $dateAdded; ?></div>
                    </div>
                </a><?php
            }
            $queryStringArray = ['searchType' => $_searchType, 'searchKeywords' => $_searchKeyword, 'postalCode' => $_postalCode ];
            $ads->appends($queryStringArray);
        ?>@if ( $ads->links() )
            <div class="pagination-container">
                {{ $ads->links() }}
            </div>
        @endif
        </div>
    </div>
    <?php else: // If no ads are found ?>
    <div class="row noMarginRow">
        <div class="col-xs-12 col-lg-4 col-lg-offset-4 no-ads">
            <h1>No Ads Found</h1>
            <p>Search query returned no results!</p>
            <ul class="text-left">
                <li>Check for spelling mistakes</li>
                <li>Remove any plural words</li>
                <li>Try searching for something else</li>
                <li>Try removing or changing the postal code</li>
                <li>Try to search with specific summarised keywords</li>
            </ul>
        </div>
    </div>
    <?php endif; ?>
</div>
@include('components.topinfo')
@endsection