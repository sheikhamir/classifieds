<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostalCodes extends Model
{
    // Table name
    protected $table = 'postal_codes';
    // Primary Key
    public $primaryKey = 'id';
}
