<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/user/{id}/{name}', function( $id, $name ){
    return 'This user is ' . $name . ' with id of ' . $id;
});*/
Route::get('/', 'PagesController@index')->name('index');
Route::get('/post', 'PagesController@post')->name('postAd');
Route::post('/post', 'AdsController@post')->name('postAdAction');
Route::post('/draft', 'AdsController@post')->name('drafts');
Route::post('/upload-image', 'AdsController@image_upload')->name('upload-image');
Route::get('/ads', function(){
    return redirect('/');
});
Route::get('/ads/{adSlug}', 'AdsController@show')->name('ad.show');

Route::get('/search?searchType={searchType}&searchKeywords={searchKeywords}', 'SearchController@search')->name('search.basic');
Route::get('/search?searchType={searchType}&postalCode={postalCode}', 'SearchController@search')->name('search.location');
Route::get('/search/', 'SearchController@search')->name('search');
Route::get('/search/{searchType}/', 'SearchController@search')->name('search.category');
Route::get('/search/{searchType}/{subCategory}', 'SearchController@search')->name('search.subcategory');
Route::get('/search/{searchType}/{subCategory}/{extraSubCategory}', 'SearchController@search')->name('search.sub-subcategory');
Route::get('/search/{searchType}/{subCategory}/{extraSubCategory}/{extraExtraSubCategory}', 'SearchController@search')->name('search.sub-sub-subcategory');
// With keywords
Route::get('/search/{searchType}/?searchKeywords={searchKeywords}', 'SearchController@search')->name('kw.c');
Route::get('/search/{searchType}/{subCategory}/?searchKeywords={searchKeywords}', 'SearchController@search')->name('kw.sc');
Route::get('/search/{searchType}/{subCategory}/{extraSubCategory}/?searchKeywords={searchKeywords}', 'SearchController@search')->name('kw.ssc');
Route::get('/search/{searchType}/{subCategory}/{extraSubCategory}/{extraExtraSubCategory}/?searchKeywords={searchKeywords}', 'SearchController@search')->name('kw.sssc');

Route::get('/about-us', 'PagesController@about')->name('about-us');
Route::get('/services', 'PagesController@services')->name('services');
Route::get('/help', 'PagesController@index')->name('help');
Route::get('/faq', 'PagesController@index')->name('faq');

Route::get('/my-details',       'UsersController@settings')->name('profile');
Route::post('/my-details',      'UsersController@update')->name('update-profile');
Route::put('/change-password',  'UsersController@change_password')->name('change-password');
Route::post('/add-number',      'UsersController@add_number')->name('add-number');
Route::post('/update-number',   'UsersController@add_number')->name('update-number');
Route::get('/favourites',       'UsersController@favourites')->name('favourites');
Route::get('/alerts',           'UsersController@alerts')->name('alerts');
Route::get('/messages',         'UsersController@messages')->name('messages');

Route::resource('blog', 'BlogController');
// User routes
// Without email verification
//Auth::routes();
// Email Verification
Auth::routes(['verify' => true]);
Route::prefix('user')->group(function(){
    Route::get('/search-history', 'UsersController@search_history')->name('search.history');
    Route::get('/saved-searches', 'UsersController@saved_searches')->name('saved.searches');
    /*Route::get('/favourites', 'UsersController@favourites')->name('favourites');
    Route::get('/messages', 'UsersController@messages')->name('alerts');
    Route::get('/messages', 'UsersController@messages')->name('messages');*/
    Route::get('/', function(){
        return redirect()->route('user.settings');
    });
});
// Admin routes
Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/dashboard', 'AdminsController@dashboard')->name('admin.dashboard');
    Route::get('/', function(){
        return redirect()->route('admin.dashboard');
    });
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.update');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@reset')->name('admin.password.reset');
    Route::get('/ads/approval/{id}', function(){
        return 'Area under construction!';
    })->name('approveAd');
    Route::get('/ads/delete/{id}', function(){
        return 'Area under construction!';
    })->name('deleteAd');
});
