@if(Auth::guard('web')->check())
    <p class="text-success">
    You are logged in as <b>USER</b>
    </p>
@else
    <p class="text-danger">
        Logged out as <b>USER</b>
    </p>
@endif

@if(Auth::guard('admin')->check())
    <p class="text-success">
    You are logged in as <b>ADMIN</b>
    </p>
@else
    <p class="text-danger">
        Logged out as <b>ADMIN</b>
    </p>
@endif