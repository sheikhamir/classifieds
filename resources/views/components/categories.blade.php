<div class="category-section-content">
    <div class="visible-xs-block"><div class="category-section-title-xs">Browse by category <i class="glyphicon glyphicon-chevron-down"></i></div></div>
    <?php render_categories(); ?>
</div>