<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 70);
            $table->index(['title']);
            $table->mediumText('description')->nullable();
            $table->string('slug')->nullable();
            $table->double('amount')->nullable();
            $table->integer('userId')->nullable()->unsigned();
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->string('postalCode')->nullable();
            $table->string('city')->nullable();
            $table->string('county')->nullable();
            $table->string('district')->nullable();
            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->string('category')->nullable();
            $table->string('subCategory')->nullable();
            $table->string('extraSubCategory')->nullable();
            $table->string('extraExtraSubCategory')->nullable();
            $table->smallInteger('withPicture');
            $table->smallInteger('sellerType');
            $table->string('videoUrl')->nullable();
            $table->string('draftId')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_drafts');
    }
}
