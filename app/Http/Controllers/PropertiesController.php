<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function for_sale(Request $request)
    {

    }

    public function to_rent(Request $request)
    {

    }
}
