<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categories', function (Blueprint $table) {
            $table->increments('subCategoryId');
            $table->string('name');
            $table->string('slug');
            $table->integer('categoryId')->unsigned();
            $table->foreign('categoryId')->references('categoryId')->on('categories')->onDelete('cascade');
        });
        // Motors
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 1,   'name' => 'Cars',                      'slug' => 'cars',                        'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 2,   'name' => 'Camper-vans & motor homes', 'slug' => 'camper-van-and-motor-homes', 'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 3,   'name' => 'Plant & tractors',          'slug' => 'plant-and-tractors',         'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 4,   'name' => 'Parts',                     'slug' => 'parts',                      'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 5,   'name' => 'Motorbikes & Scooters',     'slug' => 'motorbikes-and-scooters',    'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 6,   'name' => 'Caravans',                  'slug' => 'caravans',                   'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 7,   'name' => 'Other Vehicles',            'slug' => 'other-vehicles',             'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 8,   'name' => 'Wanted',                    'slug' => 'wanted',                     'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 9,   'name' => 'Vans',                      'slug' => 'vans',                       'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 10,  'name' => 'Trucks',                    'slug' => 'trucks',                     'categoryId' => 1 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 11,  'name' => 'Accessories',               'slug' => 'accessories',                'categoryId' => 1 ) );
        // For Sale
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 12,  'name' => 'Appliances',                             'slug' => 'appliances',                              'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 13,  'name' => 'Computer & Software',                      'slug' => 'computer-and-software',                   'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 14,  'name' => 'Musical Instruments & DJ Equipment',     'slug' => 'music-instruments-and-dj-equipment',      'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 15,  'name' => 'Video Games & Consoles',                 'slug' => 'video-games-and-console',                 'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 16,  'name' => 'Audio & Stereo',                         'slug' => 'audio-and-stereo',                        'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 17,  'name' => 'DIY Tools & Materials',                  'slug' => 'diy-tools-and-materials',                 'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 18,  'name' => 'Office Furniture & Equipment',           'slug' => 'office-furniture-and-equipment',          'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 19,  'name' => 'Freebies',                               'slug' => 'freebies',                                'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 20,  'name' => 'Baby & Kids Stuff',                      'slug' => 'baby-and-kids-stuff',                     'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 21,  'name' => 'Health & Beauty',                        'slug' => 'health-and-beauty',                       'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 22,  'name' => 'Phones, Mobile Phones & Telecoms',       'slug' => 'phones-mobile-and-telecoms',              'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 23,  'name' => 'Other Goods',                            'slug' => 'other-goods',                             'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 24,  'name' => 'Cameras, Camcorders & Studio Equipment', 'slug' => 'cameras-camcorders-and-studio-equipment', 'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 25,  'name' => 'Home & Garden',                          'slug' => 'home-and-garden',                         'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 26,  'name' => 'Sports, Leisure & Travel',               'slug' => 'sport-leisure-and-travel',                'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 27,  'name' => 'Stuff Wanted',                           'slug' => 'stiff-wanted',                            'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 28,  'name' => 'Christmas Decorations',                  'slug' => 'christmas-decorations',                   'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 29,  'name' => 'House Clearance',                        'slug' => 'house-clearance',                         'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 30,  'name' => 'Tickets',                                'slug' => 'tickets',                                 'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 31,  'name' => 'Swap Shop',                              'slug' => 'swap-shop',                               'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 32,  'name' => 'Clothes, Footwear & Accessories',        'slug' => 'clothes-footwear-and-accessories',        'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 33,  'name' => 'Music, Films, Books & Games',            'slug' => 'music-films-books-and-games',             'categoryId' => 2 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 34,  'name' => 'TV, DVD, Blu-Ray & Videos',              'slug' => 'tv-dvd-blu-ray-and-videos',               'categoryId' => 2 ) );
        // Property
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 35,  'name' => 'For Sale',         'slug' => 'for-sale',            'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 36,  'name' => 'To Swap',          'slug' => 'to-swap',             'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 37,  'name' => 'International',    'slug' => 'international',       'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 38,  'name' => 'To Rent',          'slug' => 'to-rent',             'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 39,  'name' => 'Commercial',       'slug' => 'commercial',          'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 40,  'name' => 'Holiday Rentals',  'slug' => 'holiday-rentals',     'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 41,  'name' => 'To Share',         'slug' => 'to-share',            'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 42,  'name' => 'Parking & Garage', 'slug' => 'parking-and-garage',  'categoryId' => 3 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 43,  'name' => 'Property Wanted',  'slug' => 'property-wanted',     'categoryId' => 3 ) );
        // Jobs
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 44,  'name' => 'Accountancy',                     'slug' => 'accountancy',                      'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 45,  'name' => 'Customer Service & Call Centre',  'slug' => 'customer-service-and-call-centre', 'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 46,  'name' => 'HR',                              'slug' => 'hr',                               'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 47,  'name' => 'Retail & FMCG',                   'slug' => 'retail-and-fmcg',                  'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 48,  'name' => 'Admin, Secretarial & PA',         'slug' => 'admin-secretarial-and-pa',         'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 49,  'name' => 'Driving & Automotive',            'slug' => 'driving-and-automotive',           'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 50,  'name' => 'Legal',                           'slug' => 'legal',                            'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 51,  'name' => 'Sales',                           'slug' => 'sales',                            'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 52,  'name' => 'Agriculture & Farming',           'slug' => 'agriculture-and-farming',          'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 53,  'name' => 'Engineering',                     'slug' => 'engineering',                      'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 54,  'name' => 'Leisure & Tourism',               'slug' => 'leisure-and-tourism',              'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 55,  'name' => 'Scientific & Research',           'slug' => 'scientific-and-research',          'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 56,  'name' => 'Animals',                         'slug' => 'animals',                          'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 57,  'name' => 'Financial Services',              'slug' => 'financial-services',               'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 58,  'name' => 'Manufacturing & Industrial',      'slug' => 'manufacturing-and-industrial',     'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 59,  'name' => 'Security',                        'slug' => 'security',                         'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 60,  'name' => 'Arts & Heritage',                 'slug' => 'arts-and-heritage',                'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 61,  'name' => 'Gardening',                       'slug' => 'gardening',                        'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 62,  'name' => 'Marketing, Advertising & PR',     'slug' => 'marketing-advertising-pr',         'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 63,  'name' => 'Social & Care Work',              'slug' => 'social-and-care-work',             'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 64,  'name' => 'Charity',                         'slug' => 'charity',                          'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 65,  'name' => 'Health & Beauty',                 'slug' => 'health-and-beauty',                'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 66,  'name' => 'Media, Digital & Creative',       'slug' => 'media-digital-and-creative',       'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 67,  'name' => 'Sport, Fitness & Leisure',        'slug' => 'sport-fitness-and-leisure',        'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 68,  'name' => 'Childcare',                       'slug' => 'childcare',                        'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 69,  'name' => 'Healthcare & Medical',            'slug' => 'healthcare-and-medical',           'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 70,  'name' => 'Performing Arts',                 'slug' => 'performing-arts',                  'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 71,  'name' => 'Teaching & Education',            'slug' => 'teaching-and-education',           'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 72,  'name' => 'Computing & IT',                  'slug' => 'computing-and-it',                 'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 73,  'name' => 'Hospitality & Catering',          'slug' => 'hospitality-and-catering',         'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 74,  'name' => 'Purchasing & Procurement',        'slug' => 'purchasing-and-procurement',       'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 75,  'name' => 'Transport, Logistics & Delivery', 'slug' => 'transport-logistics-and-delivery', 'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 76,  'name' => 'Construction & Property',         'slug' => 'construction-and-property',        'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 77,  'name' => 'Housekeeping & Cleaning',         'slug' => 'housekeeping-and-cleaning',        'categoryId' => 4 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 78,  'name' => 'Recruitment',                     'slug' => 'recruitment',                      'categoryId' => 4 ) );
        // Services
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 79,  'name' => 'Business & Office',           'slug' => 'business-and-office',            'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 80,  'name' => 'Finance & Legal',             'slug' => 'finance-and-legal',              'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 81,  'name' => 'Pets',                        'slug' => 'pets',                           'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 82,  'name' => 'Tuition & Classes',           'slug' => 'tuition--andclasses',            'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 83,  'name' => 'Childcare',                   'slug' => 'childcare',                      'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 84,  'name' => 'Food & Drinks',               'slug' => 'food-and-drinks',                'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 85,  'name' => 'Property & Maintenance',      'slug' => 'property-and-maintenance',       'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 86,  'name' => 'Weddings',                    'slug' => 'weddings',                       'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 87,  'name' => 'Clothing',                    'slug' => 'clothing',                       'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 88,  'name' => 'Goods Suppliers & Retailers', 'slug' => 'goods-suppliers-and-retailers',  'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 89,  'name' => 'Tradesmen & Construction',    'slug' => 'tradesmen-and-construction',     'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 90,  'name' => 'Computers & Telecoms',        'slug' => 'computers-and-telecoms',         'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 91,  'name' => 'Health & Beauty',             'slug' => 'health-and-beauty',              'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 92,  'name' => 'Transport',                   'slug' => 'transport',                      'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 93,  'name' => 'Entertainment',               'slug' => 'entertainment',                  'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 94,  'name' => 'Motoring',                    'slug' => 'motoring',                       'categoryId' => 5 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 95,  'name' => 'Travel & Tourism',            'slug' => 'travel-and-tourism',             'categoryId' => 5 ) );
        // Community
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 96,  'name' => 'Artist & Theatres',        'slug' => 'artist-and-theatres',        'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 97,  'name' => 'Groups & associations',    'slug' => 'groups-and-associations',    'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 98,  'name' => 'Rideshare & Car Pooling',  'slug' => 'rideshare-and-car-pooling',  'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 99,  'name' => 'Sports Teams & Partners',  'slug' => 'sports-teams-and-partners',  'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 100, 'name' => 'Classes',                  'slug' => 'classes',                    'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 101, 'name' => 'Lost & Found Stuff',       'slug' => 'lost-and-found-stuff',       'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 102, 'name' => 'Skills & Language Swap',   'slug' => 'skills-and-language-swap',   'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 103, 'name' => 'Travel & Travel Partners', 'slug' => 'travel-and-travel-partners', 'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 104, 'name' => 'Events, Gigs & Nightlife', 'slug' => 'events-gigs-and-nightlife',  'categoryId' => 6 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 105, 'name' => 'Music, Bands & Musicians', 'slug' => 'music-bands-and-musicians',  'categoryId' => 6 ) );
        // Pets
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 106, 'name' => 'Pets for Sale',           'slug' => 'pets-for-sale',         'categoryId' => 7 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 107, 'name' => 'Missing, Lost & Found',   'slug' => 'missing-lost-and-found',    'categoryId' => 7 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 108, 'name' => 'Equipment & Accessories', 'slug' => 'equipment-and-accessories', 'categoryId' => 7 ) );
        // Dating
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 109, 'name' => 'Men looking for Women',   'slug' => 'men-looking-for-women',   'categoryId' => 8 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 110, 'name' => 'Men looking for Men',     'slug' => 'men-looking-for-men',     'categoryId' => 8 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 111, 'name' => 'Women looking for Men',   'slug' => 'women-looking-for-men',   'categoryId' => 8 ) );
        DB::table('sub_categories')->insert( array( 'subCategoryId' => 112, 'name' => 'Women looking for Women', 'slug' => 'women-looking-for-women', 'categoryId' => 8 ) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_categories', function (Blueprint $table) {
            $table->dropForeign(['categoryId']);
        });
        Schema::dropIfExists('sub_categories');
    }
}
