@section('title')
Ad not found! {{ config('app.name') }}
@endsection

@section('body-content')
@include('components.mobileSearch')
<div class="container-fluid content-container">
    <div class="row noMarginRow">
        <div class="col-xs-12">
            <h1>Ad not found!</h1>
        </div>
    </div>
    <div class="row noMarginRow add-bottom-margin">
        <div class="col-xs-12">
            May be you marked a bookmark and now the item has been sold!
        </div>
    </div>
</div>
<div class="category-item-overlay hidden"></div>
<div class="container-fluid category-section no-bg-image hidden-xs">
    <div class="row noMarginRow">
        <div class="col-xs-12">
            <div class="caption">Search by categories</div>
        </div>
    </div>
    @include('components.categories')
</div>
@endsection