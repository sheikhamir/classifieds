@extends('layouts.admin')

@section('content')
    <section class="col-xs-12 col-no-padding summary-wrapper">
        <div class="summary-box users">
            <div class="box">
                <div class="value box-item">{{ summarise_number( $total_users ) }}</div>
                <div class="info box-item">Users</div>
            </div><div class="box icon">
                <i class="material-icons">face</i>
            </div><div class="action">
                <a href="#">More info</a>
            </div>
        </div><div class="summary-box ads">
            <div class="box">
                <div class="value box-item">{{ summarise_number( $total_ads ) }}</div>
                <div class="info box-item">Live Ads</div>
            </div><div class="box icon">
                <i class="glyphicon glyphicon-list-alt"></i>
            </div><div class="action">
                <a href="#">More info</a>
            </div>
        </div><div class="summary-box new-ads">
            <div class="box">
                <div class="value box-item">{{ summarise_number( $unapproved_ads->count() ) }}</div>
                <div class="info box-item">New Ads</div>
            </div><div class="box icon">
                <i class="material-icons">announcement</i>
            </div><div class="action">
                <a href="#">More info</a>
            </div>
        </div><div class="summary-box searches">
            <div class="box">
                <div class="value box-item">{{ summarise_number( $total_searches ) }}</div>
                <div class="info box-item">Searches</div>
            </div><div class="box icon">
                <i class="glyphicon glyphicon-search"></i>
            </div><div class="action">
                <a href="#">More info</a>
            </div>
        </div><div class="summary-box featured-ads">
        <div class="box">
            <div class="value box-item">{{ summarise_number( $total_featured_ads ) }}</div>
            <div class="info box-item">Featured</div>
        </div><div class="box icon">
            <i class="glyphicon glyphicon-star"></i>
            </div><div class="action">
                <a href="#">More info</a>
            </div>
        </div><div class="summary-box urgent-ads">
            <div class="box">
                <div class="value box-item">{{ summarise_number( $total_urgent_ads ) }}</div>
                <div class="info box-item">Urgent</div>
            </div><div class="box icon">
                <i class="material-icons">error_outline</i>
             </div><div class="action">
                 <a href="#">More info</a>
            </div>
        </div><div class="summary-box spotlighted-ads">
            <div class="box">
                <div class="value box-item">{{ summarise_number( $total_spotlight_ads ) }}</div>
                <div class="info box-item">Spotlight</div>
            </div><div class="box icon">
                <i class="glyphicon glyphicon-fire"></i>
            </div><div class="action">
                <a href="#">More info</a>
            </div>
        </div>
    </section>
    <section class="col-xs-12 col-sm-6 ads-wrapper section-wrapper">
        <div class="handle">Unapproved Ads <span>({{ sprintf("%02d", $unapproved_ads->count()) }})</span></div>
        <div class="inner-wrapper">
            <div class="ads"><?php foreach( $unapproved_ads as $ad ){
                $adId = $ad->adId;
                $adSlug = $ad->adSlug;
                $adTitle = $ad->adTitle;
                $adDesc = crop_text( $ad->adDescription );
                $images = $ad->ad_images;
                $images_array = explode(',',$images); // Create an array of images
                $displayImage = $images_array[0];
                ?><div class="ad-item">
                    <div class="actions pull-right">
                        <a href="{{ route('approveAd', ['id' => $adId ] ) }}" class="btn btn-success">Approve</a>
                        <a href="{{ route('deleteAd', ['id' => $adId ] ) }}" class="btn btn-danger">Delete</a>
                        <a href="{{ route('ad.show', ['adSlug' => $adSlug ] ) }}" class="btn btn-default">View Ad</a>
                    </div>
                    <img src="{{ $displayImage }}" class="thumbnail img-responsive"/>
                    <h4 class="list-group-item-heading">{{ $adTitle }}</h4>
                    <p class="list-group-item-text">{{ $adDesc }}</p>
                </div><?php
            } ?></div>
        </div>
    </section><section class="col-xs-12 col-sm-6 ads-wrapper section-wrapper">
        <div class="handle">Recent Live Ads</div>
        <div class="inner-wrapper">
            <div class="ads"><?php foreach( $approved_ads as $ad ){
                    $adId = $ad->adId;
                    $adSlug = $ad->adSlug;
                    $adTitle = $ad->adTitle;
                    $adDesc = $ad->adDescription;
                    $images = $ad->ad_images;
                    $images_array = explode(',',$images); // Create an array of images
                    $displayImage = $images_array[0];
                ?><div class="list-group-item">
                    <div class="actions pull-right">
                        <a href="{{ route('approveAd', ['id' => $adId ] ) }}" class="btn btn-success">Approve</a>
                        <a href="{{ route('deleteAd', ['id' => $adId ] ) }}" class="btn btn-danger">Delete</a>
                    </div>
                    <img src="{{ $displayImage }}" class="thumbnail img-responsive"/><h4 class="list-group-item-heading">{{ $adTitle }}</h4>
                    <p class="list-group-item-text">{{ crop_text( $adDesc ) }}</p>
                </div><?php
            } ?></div>
        </div>
    </section>
@endsection