<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    // Table name
    protected $table = 'search';
    // Primary Key
    public $primaryKey = 'id';
}
