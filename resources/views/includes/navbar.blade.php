<div class="container-fluid header" role="navigation">
    <div class="row noMarginRow text-center-xs constrained-width">
        <div class="col-xs-2 col-sm-4 header-inline">
            <div class="header-logo">
                <a href="{{ route('index') }}" class="logo"></a>
            </div>
        </div>
        <div class="col-xs-10 col-sm-8 header-inline">
            <div class="header-links">
                <a href="{{ route('postAd') }}" class="btn-post-ad hidden-xs">Post an ad</a>
                @guest
                <a href="{{ route('login') }}" class="nlink hidden-xs">Login</a><span class="nlink divider hidden-xs">&#124;</span><a href="{{ route('register') }}" class="nlink hidden-xs register">Register</a>
                @endguest
                @auth("web")
                <a href="{{ route('logout') }}" class="nlink hidden-xs user-btn dropdown">Hi {{ user_name() }}<span class="glyphicon glyphicon-triangle-bottom"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                @endauth
                <button class="sell-btn visible-xs-inline-block">Sell</button><button class="menu-btn-toggle visible-xs-inline-block">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="text">MENU</span>
                </button>
            </div>
        </div>
    </div>
    @auth("web")
    <div class="user-menu hidden-xs hidden close-all">
        <ul>
            <li><a href="{{ route('favourites') }}">Favourites</a></li>
            <li><a href="{{ route('alerts') }}">Alerts</a></li>
            <li><a href="{{ route('messages') }}">Messages</a></li>
            <li><a href="{{ route('profile') }}">My details</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
        </ul>
    </div>
    <script type="text/javascript">
    $(function(){
        $('.user-btn').click(function(e){
            e.preventDefault();
            $('.user-menu').toggleClass('hidden');
        });
    });
    </script>
    @endauth
    <div class="header-dropdown-overlay hidden"></div>
    <div class="header-dropdown visible-xs">
        <a href="{{ route('index') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-home"></span>
            <span class="header-dropdown-inline text">Home</span>
        </a>
        <a href="{{ route('postAd') }}" class="no-border">
            <span class="header-dropdown-inline glyphicon glyphicon-plus-sign"></span>
            <span class="header-dropdown-inline text">Post and ad</span>
        </a>
        <div class="divider">
            MyClassified
        </div>
        @auth("web")
        <a href="{{ route('favourites') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-heart"></span>
            <span class="header-dropdown-inline text">Favourites</span>
        </a>
        <a href="{{ route('alerts') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-bell"></span>
            <span class="header-dropdown-inline text">Alerts</span>
        </a>
        <a href="{{ route('messages') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-envelope"></span>
            <span class="header-dropdown-inline text">Messages</span>
        </a>
        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <span class="header-dropdown-inline glyphicon glyphicon-log-out"></span>
            <span class="header-dropdown-inline text">Logout</span>
        </a>
        @endauth
        @guest
        <a href="{{ route('login') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-log-in"></span>
            <span class="header-dropdown-inline text">Sign In</span>
        </a>
        <a href="{{ route('register') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-user"></span>
            <span class="header-dropdown-inline text">Register</span>
        </a>
        @endguest
        <div class="divider">
            More information
        </div>
        <a href="{{ route('help') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-exclamation-sign"></span>
            <span class="header-dropdown-inline text">Help & Contact</span>
        </a>
        <a href="{{ route('faq') }}">
            <span class="header-dropdown-inline glyphicon glyphicon-question-sign"></span>
            <span class="header-dropdown-inline text">Frequently Asked Questions</span>
        </a>
    </div>
</div>