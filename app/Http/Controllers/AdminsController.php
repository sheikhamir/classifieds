<?php
/**
 * @author: Sheikh Amir
 * @email: smamir3@gmail.com
 *
 * */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Ads;
use App\Search;

class AdminsController extends Controller
{

    protected $redirectTo = '/admin/login';
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $users      = User::get()->count();
        $ads        = Ads::get()->count();
        $approved   = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where('ads.live','=','1')
                    ->groupBy('ads.adId')
                    ->skip(0)
                    ->take(5)
                    ->orderBy('ads.adId', 'DESC')->get();
        $unapproved = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where('ads.live','=','0')
                    ->groupBy('ads.adId')
                    ->orderBy('ads.adId', 'DESC')->get();
        $featured   = Ads::select('featured')->where('featured','=','1')->get()->count();
        $urgent     = Ads::select('urgent')->where('urgent','=','1')->get()->count();
        $spotlight  = Ads::select('spotlight')->where('spotlight','=','1')->get()->count();
        $searches   = Search::select(DB::raw('SUM(searches) as totalSearches'))->get()[0]['totalSearches'];
        $data       = array(
            'title'               => 'Dashboard', /* Title of the page */
            'activate_dashboard'  => 'active', /* Add activate class to side menu */
            'total_users'         => $users, /* Total number of users*/
            'total_ads'           => $ads, /* Total number of ads */
            'approved_ads'        => $approved, /* List of all unapproved ads */
            'unapproved_ads'      => $unapproved, /* List of all unapproved ads */
            'total_featured_ads'  => $featured, /* Total number of featured ads */
            'total_urgent_ads'    => $urgent, /* Total number of urgent ads*/
            'total_spotlight_ads' => $spotlight, /* Total number of spotlight ads */
            'total_searches'      => $searches /* Total number of searches */
        );
        return view('admin.dashboard')->with($data);
    }
}
