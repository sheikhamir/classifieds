<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="unloaded">
    @include('includes.emptyHeader')
    <body class="unloaded">
        {{--@include('includes.navbar')--}}

        @yield('body-content')
        {{--<div class="container">
            @include('includes.msg')
        </div>--}}
        <script type="text/javascript" src="{{asset('js/selector.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/classifieds.js')}}"></script>
    </body>
    {{--@include('includes.footer')--}}
</html>
