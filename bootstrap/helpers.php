<?php

/**
|--------------------------------------------------------------------------
| Custom Helper Functions
|--------------------------------------------------------------------------
| Author: Sheikh Amir
|
| You can use these functions in all the files and can define your own.
|
*/
use App\Ads;
use App\Images;
use App\Categories;
use App\PostalCodes;
use App\SubCategories;
use App\ExtraSubCategories;
use App\ExtraExtraSubCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

if (! function_exists('generate_price_as_options')) {
    function generate_price_as_options () {
        $interval_1 = 10000;
        $interval_2 = 25000;
        $interval_3 = 50000;
        $interval_4 = 100000;
        $interval_5 = 250000;
        $interval_6 = 500000;
        ?><option value="0">ANY ...</option><?php
        // To 250,000 with with 10,000 multiple
        for ( $i = 1; $i <= 25; $i++  ){
            $amount_1 = $i * $interval_1;
            $formatted = number_format( $amount_1 );
            ?><option value="<?php echo $amount_1; ?>">&pound;<?php echo $formatted; ?></option><?php
        }
        // To 500,000 with with 25,000 multiple
        for ( $i = 1; $i <= 10; $i++  ){
            $amount_2 = $amount_1 + ( $i * $interval_2 );
            $formatted = number_format( $amount_2 );
            ?><option value="<?php echo $amount_2; ?>">&pound;<?php echo $formatted; ?></option><?php
        }
        // To 1,000,000 with with 50,000 multiple
        for ( $i = 1; $i <= 10; $i++  ){
            $amount_3 = $amount_2 + ( $i * $interval_3 );
            $formatted = number_format( $amount_3 );
            ?><option value="<?php echo $amount_3; ?>">&pound;<?php echo $formatted; ?></option><?php
        }
        // To 2,500,000 with with 100,000 multiple
        for ( $i = 1; $i <= 15; $i++  ){
            $amount_4 = $amount_3 + ( $i * $interval_4 );
            $formatted = number_format( $amount_4 );
            ?><option value="<?php echo $amount_4; ?>">&pound;<?php echo $formatted; ?></option><?php
        }
        // To 5,000,000 with with 250,000 multiple
        for ( $i = 1; $i <= 10; $i++  ){
            $amount_5 = $amount_4 + ( $i * $interval_5 );
            $formatted = number_format( $amount_5 );
            ?><option value="<?php echo $amount_5; ?>">&pound;<?php echo $formatted; ?></option><?php
        }
        // To 10,000,000 with with 500,000 multiple
        for ( $i = 1; $i <= 30; $i++  ){
            $amount_6 = $amount_5 + ( $i * $interval_6 );
            $formatted = number_format( $amount_6 );
            ?><option value="<?php echo $amount_6; ?>">&pound;<?php echo $formatted; ?></option><?php
        }
    }
}

if (! function_exists('catName')) {
    function catName ( $slug, $type='cat' ) {
        if( $type == 'cat' ) {
            $cat = Categories::where('slug', $slug)->first();
        } elseif ( $type == 'subcat' ) {
            $cat = SubCategories::where('slug', $slug)->first();
        } elseif ( $type == 'extracat' ) {
            $cat = extraSubCategories::where('slug', $slug)->first();
        } elseif ( $type == 'eec' ) {
            $cat = extraExtraSubCategories::where('slug', $slug)->first();
        }
        return $cat === null ? 'all' : $cat->name;
    }
}

if (! function_exists('getCatSlug')) {
    function getCatSlug ( $category_name, $type='cat' ) {
        if( $type == 'cat' ) {
            $cat = Categories::where('name', $category_name)->first();
            return $cat->slug;
        } elseif ( $type == 'sub' ) {
            $cat = SubCategories::where('name', $category_name)->first();
            return $cat->slug;
        } elseif ( $type == 'ss' ) {
            $cat = ExtraSubCategories::where('name', $category_name)->first();
            return $cat->slug;
        } elseif ( $type == 'sss' ) {
            $cat = ExtraExtraSubCategories::where('name', $category_name)->first();
            return $cat->slug;
        }
    }
}

if (! function_exists('summarise')) {
    function summarise ( $content, $length = 140 ) {
        if ( strlen( $content ) > $length ) {
            return rtrim(substr($content, 0, $length)) . '...';
        } else {
            return $content;
        }
    }
}

if (! function_exists('searchQuery')) {
    function searchQuery ( $what = 'category', $value ) {
        $queryString = $_SERVER['QUERY_STRING'];
        $uri = '';
        if ( $what == 'category' ) {
            parse_str($queryString, $output);
            $category = $value;
            unset($output['searchType']);
            unset($output['page']);
            $url = "/search/{$category}/?" . http_build_query( $output ) . "\n";
            $uri = app('url')->to( $url );
        } elseif ( $what == 'sub-category' ) {
            parse_str($queryString, $output);
            unset($output['searchType']);
            unset($output['page']);
            $category = cat_from_sub_cat( $value );
            $sub_category = $value;
            $url = "/search/{$category}/{$sub_category}/?" . http_build_query( $output ) . "\n";
            $uri = app('url')->to( $url );
        } elseif ( $what == 'sub-sub-category' ) {
            parse_str($queryString, $output);
            unset($output['searchType']);
            unset($output['page']);
            $category = sub_sub_cat_from_sub_cat( $value );
            $sub_category = $value;
            $url = "/search/{$category}/{$sub_category}/?" . http_build_query( $output ) . "\n";
            $uri = app('url')->to( $url );
        }
        return $uri;
    }
}

if (! function_exists('cat_from_sub_cat')) {
    function cat_from_sub_cat ( $sub_category_slug ) {
        $category = Categories::select( 'slug' )->whereIn( 'categoryId', SubCategories::select( 'categoryId' )->where( 'slug', $sub_category_slug ) )->first();
        return $category->slug;
    }
}

if (! function_exists('sub_cat_from_ss_cat')) {
    function sub_cat_from_ss_cat ( $sub_sub_category_slug ) {
        $sub_category = SubCategories::select( 'slug' )->whereIn( 'subCategoryId', ExtraSubCategories::select( 'subCategoryId' )->where( 'slug', $sub_sub_category_slug ) )->first();
        return $sub_category->slug;
    }
}

if (! function_exists('ss_cat_from_sss_cat')) {
    function ss_cat_from_sss_cat ( $sub_sub_sub_category_slug ) {
        #$sub_category = SubCategories::select( 'slug' )->whereIn( 'subCategoryId', ExtraSubCategories::select( 'subCategoryId' )->where( 'slug', $sub_sub_sub_category_slug ) )->first();
        #return $sub_category->slug;
        # Model has no data
    }
}

if (! function_exists('render_search_categories_as_options')) {
    function render_search_categories_as_options( $current_category = false ) {
        $categories = Categories::all();
        foreach( $categories as $cat ) {
            $slug = $cat->slug;
            $name = $cat->name;
            if ( isset ( $current_category ) && !empty( $current_category ) ) {
                if ( $current_category == $slug ) {
                    echo "<option value=\"$slug\" selected=\"selected\">$name</option>";
                } else {
                    echo "<option value=\"$slug\">$name</option>";
                }
            } else {
                echo "<option value=\"$slug\">$name</option>";
            }
        }
    }
}

if (! function_exists('render_categories')) {
    function render_categories() {
        $categories = Categories::all();
        $total_categories = $categories->count();
        $i = 1;
        foreach( $categories as $cat ) {
            $ctid = $cat->categoryId;
            $slug = $cat->slug;
            $name = $cat->name;
            $info = $cat->info;
            if ( $slug != 'datings' ) {
                echo "<button class=\"category-item has-sub-category\">" .
                    "<div class=\"category-item-img $slug\"></div>" .
                    "<div class=\"category-item-title\">$name</div>" .
                    "<div class=\"category-item-desc\">$info</div>" .
                    "<div class=\"sub-category\">" .
                    "<div class=\"sub-category-title\">" .
                    "<a href='" . route('search.category', ['searchType' => $slug]) . "'>View all in $name</a>" .
                    "</div>" .
                    "<div class=\"sub-category-content\">";
                $sub_categories = SubCategories::where('categoryId', $ctid)->get();
                foreach ($sub_categories as $scat) {
                    $_name = $scat->name;
                    $_slug = $scat->slug;
                    echo "<div class=\"sub-category-inline\"><a href='" . route('search.subcategory', ['searchType' => $slug, 'subCategory' => $_slug]) . "'>$_name</a></div>";
                }
                echo "</div></div></button>";
            } else {
                echo "<a href='" . route('search.category', ['searchType' => $slug]) . "' class=\"category-item\">" .
                    "<div class=\"category-item-img $slug\"></div>" .
                    "<div class=\"category-item-title\">$name</div>" .
                    "<div class=\"category-item-desc\">$info</div>".
                    "</div></div></a>";
            }
            if ( $i != $total_categories ) {
                echo '<span class="category-item divider"></span>';
            }
            $i = $i + 1;
        }
    }
}

if (! function_exists('get_current_url')) {
    function get_current_url() {
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            $link = "https";
        } else {
            $link = "http";
        }
        // Here append the common URL characters.
        $link .= "://";
        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];
        // Append the requested resource location to the URL
        $link .= $_SERVER['REQUEST_URI'];
        // Return the link
        return $link;
    }
}

if (! function_exists('crop_text')) {
    function crop_text( $text, $length = 150) {
        if ( strlen( $text ) >= $length ) {
            $text = rtrim( substr( $text, 0, $length ) ) . '...';
        }
        // Return the cropped text
        return $text;
    }
}

if (! function_exists('summarise_number')) {
    function summarise_number ( $number ) {
        if ( $number > 1000 ) {
            $x = round($number);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('K', 'M', 'B', 'T', 'q', 'Q', 's', 'S', 'O', 'N', 'D');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];
            return $x_display;
        }
        return $number;
    }
}

if (! function_exists('fetch_top_searches')) {
    function fetch_top_searches ( $number_of_searches = 5 ) {
        $top_searches = '';
        $i = 1;
        $searches = DB::table('search')
            ->select('search.keywords', DB::raw('SUM(search.searches) as totalSearches, count(search.searches) as keywordTotalSearches'))
            ->where('keywords','!=','null')
            ->where('keywords','!=','')
            ->groupBy('search.keywords')
            ->orderBy(DB::raw('SUM(search.searches)'), 'desc')
            ->skip(0)
            ->take( $number_of_searches )
            ->get();
        foreach( $searches as $search ) {
            if ( !empty( $search ) && $search != NULL ) {
                $keyword = strip_tags( $search->keywords );
                $top_searches .= '<a href="' . route('search.basic', ['searchType' => 'all', 'searchKeywords' => $keyword]) . '">' . strip_tags($keyword) . '</a>';
                if ($i < count($searches)) {
                    $top_searches .= '<span class="divider"></span>';
                }
                $i += 1;
            }
        }
        echo $top_searches;
    }
}

if (! function_exists('fetch_top_locations')) {
    function fetch_top_locations ( $number_of_searches = 5 ) {
        $top_locations = '';
        $i = 1;
        $searches = DB::table('search')
            ->select('search.postalCode', DB::raw('SUM(search.searches) as totalSearches, count(search.searches) as keywordTotalSearches'))
            ->where('postalCode','!=','null')
            ->where('postalCode','!=','')
            ->groupBy('search.postalCode')
            ->orderBy(DB::raw('SUM(search.searches)'), 'desc')
            ->skip(0)
            ->take( $number_of_searches )
            ->get();
        foreach( $searches as $search ) {
            if ( !empty( $search ) && $search != NULL ) {
                $location = strip_tags( $search->postalCode );
                $top_locations .= '<a href="' . route('search.location', ['searchType' => 'all', 'postalCode' => $location]) . '">' . strip_tags($location) . '</a>';
                if ($i < count($searches)) {
                    $top_locations .= '<span class="divider"></span>';
                }
                $i += 1;
            }
        }
        echo $top_locations;
    }
}

if (! function_exists('user_name')) {
    function user_name ( $type = 'first-name' ) {
        if ( Auth::guest() ) {
            $name = 'Guest';
        } else {
            $user = Auth::user();
            $fullName = trim( $user->name );
            if ( $type == 'first-name' ) {
                if ( strpos( $fullName, ' ' ) !== false ) {
                    $name = explode( ' ', $fullName )[0];
                } else {
                    $name = $fullName;
                }
            } elseif ( $type == 'last-name' ) {
                if ( strpos( $fullName, ' ' ) !== false ) {
                    $nameExploded = explode( ' ', $fullName );
                    $name = $nameExploded[ count( $nameExploded ) - 1 ];
                } else {
                    $name = $fullName;
                }
            } else {
                $name = $fullName;
            }
            return $name;
        }
    }
}

if (! function_exists('fetch_ad')) {
    function fetch_ad ( $id ) {
        $ad = Ads::where( 'adId', $id )->first();
        return $ad;
    }
}

if (! function_exists('fetch_cover')) {
    function fetch_cover ( $id ) {
        $cover_image = Images::where( 'adId', $id )->where( 'type', 'cover' )->first();
        return $cover_image;
    }
}

if (! function_exists('time_elapsed')) {
    function time_elapsed ( $time ) {
        /*date_default_timezone_set('Europe/London');
        $posted_time = new DateTime( $time );
        $current_time = new DateTime( date('Y-m-d H:i:s') );
        $diff = $current_time->diff( $posted_time );*/
        $return = time_elapsed_string( $time, true );
        return $return;
    }
}

if (! function_exists('time_elapsed_string')) {
    function time_elapsed_string ( $datetime, $full = false ) {
        date_default_timezone_set('Europe/London');
        $now = new DateTime(date('Y-m-d H:i:s'));
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}

if (! function_exists('result_info')) {
    function result_info( $keyword, $category, $sub_category, $sub_sub_category, $sub_sub_sub_category, $postal_code, $number_of_ads ){
        $resultsFor = "{$number_of_ads} ads";
        if ( isset( $keyword ) && empty( $keyword ) ) {
            //$resultsFor .= " for everything";
        } else {
            $resultsFor .= ' for "'.$keyword.'"';
        }
        if ( isset( $category ) && !empty( $category ) ) {
            if ( $category == 'all' ) {
                $resultsFor .= " in all categories";
            } else {
                $catName = catName( $category );
                $resultsFor .= " in \"{$catName}\"";
            }
        }
        if ( isset( $postal_code ) && !empty ( $postal_code ) ) {
            $pc_info = PostalCodes::where('postalCode',$postal_code)->first();
            if ( $pc_info !== null ) {
                $city = $pc_info->city;
                $country = $pc_info->country;
                $location = "{$city}, {$country}";
                $resultsFor .= " in {$location}";
            }
        }
        return $resultsFor;
    }
}

if (! function_exists('add_filter')) {
    function add_filter () {

    }
}

if (! function_exists('select_all')) {
    function select_all ( $item ) {
        return ( !empty( $item ) && $item == 'all' ) ? true : false;
    }
}

if (! function_exists('make_url')) {
    function make_url( $keyword, $category, $sub_category, $sub_sub_category, $sub_sub_sub_category, $postal_code, $for ) {
        $url_str = '/search';
        if ( !empty( $category ) && !select_all( $category ) )
            $url_str .= "/$category";
        if ( !empty( $sub_category ) && !select_all( $sub_category ) )
            $url_str .= "/$sub_category";
        if ( !empty( $sub_sub_category ) && !select_all( $sub_sub_category ) )
            $url_str .= "/$sub_sub_category";
        if ( !empty( $sub_sub_sub_category ) && !select_all( $sub_sub_sub_category ) )
            $url_str .= "/$sub_sub_sub_category";
        /*if ( !empty( $keyword ) )
            $url_str .= "?searchKeywords=$keyword&";
        if ( !empty( $postal_code ) ):
            if ( empty( $keyword ) ) {
                $url_str .= "?postalCode=$postal_code";
            } else {
                $url_str .= "postalCode=$postal_code";
            }
        endif;*/
        $queryString = $_SERVER['QUERY_STRING'];
        parse_str($queryString, $output);
        if ( isset ( $output[ $for ] ) ) {
            unset( $output[ $for ] );
        }else {
            $output[$for] = 1;
        }
        unset($output['page']);
        $qs = http_build_query( $output );
        $url_str .= !empty( $qs ) ? "?" . http_build_query( $output ) : "";

        return app( 'url' )->to( $url_str );
    }
}

if (! function_exists('trim_request')) {
    function trim_request ( $key ) {
        $queryString = $_SERVER['QUERY_STRING'];
        parse_str($queryString, $output);
        unset($output[ $key ]);
        $qs = http_build_query( $output );
        return !empty( $qs ) ? "?" . http_build_query( $output ) : "";
    }
}

if (! function_exists('get_exists')) {
    function get_exists ( $key ) {
        $queryString = $_SERVER['QUERY_STRING'];
        parse_str($queryString, $output);
        return isset( $output[ $key ] ) AND !empty( $_GET[ $key ] ) ? true : false;
    }
}

if (! function_exists('convertImage')) {
    function convertImage ( $originalImage, $outputImage, $quality = 100 ) {
        // jpg, png, gif or bmp?
        $exploded = explode('.',$originalImage);
        $ext = $exploded[count($exploded) - 1];

        if (preg_match('/jpg|jpeg/i',$ext))
            $imageTmp = imagecreatefromjpeg($originalImage);
        else if (preg_match('/png/i',$ext))
            $imageTmp = imagecreatefrompng($originalImage);
        else if (preg_match('/bmp/i',$ext))
            $imageTmp = function_exists( 'imagecreatefrombmp' ) ? imagecreatefrombmp($originalImage) : 'PHP 7.0 or above required!';
        else
            return 0;

        // quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, $outputImage, $quality);
        imagedestroy($imageTmp);

        return 1;
    }
}

if(! function_exists('create_empty_html_file')) {
    function create_empty_html_file( $path ) {
        if ( !file_exists ( "{$path}/index.html" ) ) {
            $content = "<html><head></head><body></body></html>";
            $handle = fopen( "{$path}/index.html", 'w+' );
            fwrite( $handle, $content );
            fclose( $handle );
        }
    }
}






/**
 *      Make functions to fetch Ad from 'all' categories and fetch Ad from selected categories
 *      function fetch_ads_from_categories( $sort_by, $keyword, $postal_code, $category Default= 'all'){}
 *      function fetch_ads_from_sub_categories( $sort_by, $keyword, $postal_code, $category, $sub_category){}
 *      function fetch_ads_from_sub_sub_categories( $sort_by, $keyword, $postal_code, $category, $sub_category, $sub_sub_category){}
 *      function fetch_ads_from_sub_sub_sub_categories( $sort_by, $keyword, $postal_code, $category, $sub_category, $sub_sub_category, $sub_sub_sub_category){}
 *
 *      Similarly for fetch categories of ads, sub-categories of ads, sub-sub-categories of ads and sub-sub-sub-categories of ads
 *      function fetch_categories( $keyword, $postalCode ){}
 *      function fetch_sub_categories( $keyword, $postalCode, $category ){}
 *      function fetch_sub_sub_categories( $keyword, $postalCode, $sub_category ){}
 *      function fetch_sub_sub_sub_categories( $keyword, $postalCode, $sub_sub_category ){}
 *
 *      use @cat_from_sub_cat(), @sub_cat_from_ss_cat() and @ss_cat_from_sss_cat to retrieve categories
 *      sub-categories and sub-sub-categories
 *
 *
 */

