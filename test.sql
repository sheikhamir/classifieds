INSERT INTO `sub_categories`
(`subCategoryId`, `subCategoryName`, `subCategorySlug`, `categoryId`, `created_at`, `updated_at`) VALUES
-- 
(NULL, 'Vans', 'vans', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Camper-vans & motor-homes', 'camper-van-motor-homes', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Caravans', 'caravans', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Trucks', 'trucks', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Plant & tractors', 'plant-tractors', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Other Vehicles', 'other-vehicles', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Accessories', 'accessories', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Parts', 'parts', '1', CURRENT_TIME(), CURRENT_TIME()),
(NULL, 'Wanted', 'wanted', '1', CURRENT_TIME(), CURRENT_TIME()),
-- For sale --