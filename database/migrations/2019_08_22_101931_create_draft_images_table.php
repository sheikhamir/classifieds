<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('type', 7)->default('gallery');
            $table->integer('draftId')->unsigned();
            $table->foreign('draftId')->references('id')->on('adDrafts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draft_images');
    }
}
