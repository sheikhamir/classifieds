<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    // Table name
    protected $table = 'sub_categories';
    // Primary Key
    public $primaryKey = 'id';
}
