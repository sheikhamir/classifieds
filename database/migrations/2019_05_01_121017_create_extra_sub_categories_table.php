<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateExtraSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('subCategoryId')->unsigned();
            $table->foreign('subCategoryId')->references('subCategoryId')->on('sub_categories')->onDelete('cascade');
        });
        // Motors->Accessories
        DB::table('extra_sub_categories')->insert( array( 'id' => 1,   'name' => 'Car Tuning & Styling',            'slug' => 'car-tuning-and-styling',            'subCategoryId' => 11 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 2,   'name' => 'Clothes, Helmets & Boots',        'slug' => 'clothes-helmets-and-boots',         'subCategoryId' => 11 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 3,   'name' => 'In-Car Audio & GPS',              'slug' => 'in-car-audio-and-gps',              'subCategoryId' => 11 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 4,   'name' => 'Motorbike Accessories & Styling', 'slug' => 'motorbike-accessories-and-styling', 'subCategoryId' => 11 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 5,   'name' => 'Other Accessories',               'slug' => 'other-accessories',                 'subCategoryId' => 11 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 6,   'name' => 'Wheel Rims & Tyres',              'slug' => 'wheel-rims-and-tyres',              'subCategoryId' => 11 ) );
        // Motors->Parts
        DB::table('extra_sub_categories')->insert( array( 'id' => 7,   'name' => 'Car Parts',                 'slug' => 'car-parts',                   'subCategoryId' => 4 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 8,   'name' => 'Motorbike & Scooter Parts', 'slug' => 'motorbike-and-scooter-parts', 'subCategoryId' => 4 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 9,   'name' => 'Van Parts',                 'slug' => 'van-parts',                   'subCategoryId' => 4 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 10,  'name' => 'Campervan & Caravan Parts', 'slug' => 'campervan-and-caravan-parts', 'subCategoryId' => 4 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 11,  'name' => 'Truck Parts',               'slug' => 'truck-parts',                 'subCategoryId' => 4 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 12,  'name' => 'Plant & Tractor Parts',     'slug' => 'plant-and-tractor-parts',     'subCategoryId' => 4 ) );
        // For Sale->Appliances
        DB::table('extra_sub_categories')->insert( array( 'id' => 13,  'name' => 'Dishwashers',                'slug' => 'dishwashers',                  'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 14,  'name' => 'Freezers',                   'slug' => 'freezers',                     'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 15,  'name' => 'Fridge Freezers',            'slug' => 'fridge-freezers',              'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 16,  'name' => 'Health & Beauty Appliances', 'slug' => 'health-and-beauty-appliances', 'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 17,  'name' => 'Home Appliances',            'slug' => 'home-appliances',              'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 18,  'name' => 'Integrated Appliances',      'slug' => 'integrated-appliances',        'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 19,  'name' => 'Other Appliances',           'slug' => 'other-appliances',             'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 20,  'name' => 'Ovens, Hobs & Cookers',      'slug' => 'ovens-hobs-and-cookers',       'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 21,  'name' => 'Refrigerators',              'slug' => 'refrigerators',                'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 22,  'name' => 'Small Appliances',           'slug' => 'small-appliances',             'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 23,  'name' => 'Tumble Dryers',              'slug' => 'tumble-dryers',                'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 24,  'name' => 'Washer Dryers',              'slug' => 'washer-dryers',                'subCategoryId' => 12 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 25,  'name' => 'Washing Machines',           'slug' => 'washing-machines',             'subCategoryId' => 12 ) );
        // For Sale->Audio & Stereo
        DB::table('extra_sub_categories')->insert( array( 'id' => 26,  'name' => 'Home Cinema',           'slug' => 'home-cinema',             'subCategoryId' => 16 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 27,  'name' => 'Microphones',           'slug' => 'microphones',             'subCategoryId' => 16 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 28,  'name' => 'Other Stereo & Audio',  'slug' => 'other-stereo-and-audio',  'subCategoryId' => 16 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 29,  'name' => 'Personal Stereos',      'slug' => 'personal-stereos',        'subCategoryId' => 16 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 30,  'name' => 'Stereos & Accessories', 'slug' => 'stereos-and-accessories', 'subCategoryId' => 16 ) );
        // For Sale->Baby & Kids Stuff
        DB::table('extra_sub_categories')->insert( array( 'id' => 31,  'name' => 'Baby & Child Safety',               'slug' => 'baby-and-child-safety',              'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 32,  'name' => 'Baby Bouncers, Rockers & Swings',   'slug' => 'baby-bouncers-rockers-and-swings',   'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 33,  'name' => 'Baby Clothes',                      'slug' => 'baby-clothes',                       'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 34,  'name' => 'Car Seats & Baby Carriers',         'slug' => 'car-seats-and-baby-carriers',        'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 35,  'name' => 'Changing',                          'slug' => 'changing',                           'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 36,  'name' => 'Feeding',                           'slug' => 'feeding',                            'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 37,  'name' => 'Kids Clothes, Shoes & Accessories', 'slug' => 'kids-clothes-shoes-and-accessories', 'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 38,  'name' => 'Nursery & Children\'s Furniture',   'slug' => 'nursery-and-childrens-furniture',     'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 39,  'name' => 'Other',                             'slug' => 'other',                              'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 40,  'name' => 'Outdoor Toys',                      'slug' => 'outdoor-toys',                       'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 41,  'name' => 'Prams & Strollers',                 'slug' => 'prams-and-strollers',                'subCategoryId' => 20 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 42,  'name' => 'Toys',                              'slug' => 'toys',                               'subCategoryId' => 20 ) );
        // For Sale->Cameras, Camcorders & Studio Equipment
        DB::table('extra_sub_categories')->insert( array( 'id' => 43,  'name' => 'Binoculars & Scopes',         'slug' => 'binoculars-and-scopes',         'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 44,  'name' => 'Camcorders & Video Cameras',  'slug' => 'camcorders-and-video-cameras',  'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 45,  'name' => 'Camera Accessories',          'slug' => 'camera-accessories',            'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 46,  'name' => 'Digital Cameras',             'slug' => 'digital-cameras',               'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 47,  'name' => 'Digital Photo Frames',        'slug' => 'digital-photo-frames',          'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 48,  'name' => 'Film & Disposable Frames',    'slug' => 'film-and-disposable-frames',    'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 49,  'name' => 'Filters',                     'slug' => 'filters',                       'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 50,  'name' => 'Flashguns & Accessories',     'slug' => 'flashguns-and-accessories',     'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 51,  'name' => 'Grips',                       'slug' => 'grips',                         'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 52,  'name' => 'Lenses',                      'slug' => 'lenses',                        'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 53,  'name' => 'Lighting & Studio',           'slug' => 'lighting-and-studio',           'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 54,  'name' => 'Other Cameras & Accessories', 'slug' => 'other-cameras-and-accessories', 'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 55,  'name' => 'Replacement Parts & Tools',   'slug' => 'replacement-parts-and-tools',   'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 56,  'name' => 'Surveillance Cameras',        'slug' => 'surveillance-cameras',          'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 57,  'name' => 'Telescopes',                  'slug' => 'telescopes',                    'subCategoryId' => 24 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 58,  'name' => 'Tripods & Supports',          'slug' => 'tripods-and-supports',          'subCategoryId' => 24 ) );
        // For Sale->Clothes, Footwear & Accessories
        DB::table('extra_sub_categories')->insert( array( 'id' => 59,  'name' => 'Jewellery',                     'slug' => 'jewellery',                       'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 60,  'name' => 'Men\'s Accessories',            'slug' => 'mens-accessories',                'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 61,  'name' => 'Men\'s Clothing',               'slug' => 'mens-clothing',                   'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 62,  'name' => 'Men\'s Shoes & Boots',          'slug' => 'mens-shoes-and-boots',            'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 63,  'name' => 'Men\'s Watches',                'slug' => 'mens-watches',                    'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 64,  'name' => 'Other',                         'slug' => 'other',                           'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 65,  'name' => 'Sunglasses',                    'slug' => 'sunglasses',                      'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 66,  'name' => 'Watches',                       'slug' => 'watches',                         'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 67,  'name' => 'Wedding Clothes & Accessories', 'slug' => 'wedding-clothes-and-accessories', 'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 68,  'name' => 'Women\'s Accessories',          'slug' => 'womens-accessories',              'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 69,  'name' => 'Women\'s Clothing',             'slug' => 'womens-clothing',                 'subCategoryId' => 32 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 70,  'name' => 'Women\'s Shoes',                'slug' => 'womens-shoes',                    'subCategoryId' => 32 ) );
        // For Sale->Computers & Software
        DB::table('extra_sub_categories')->insert( array( 'id' => 71,  'name' => 'Computers, Laptops & Netbooks', 'slug' => 'computers-laptops-and-netbooks', 'subCategoryId' => 13 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 72,  'name' => 'Software',                      'slug' => 'software',                       'subCategoryId' => 13 ) );
        // For Sale->DIY Tools & Materials
        DB::table('extra_sub_categories')->insert( array( 'id' => 73,  'name' => 'Bathroom Fixtures',              'slug' => 'bathroom-fixtures',                'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 74,  'name' => 'Building Materials',             'slug' => 'building-materials',               'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 75,  'name' => 'Doors & Windows',                'slug' => 'doors-and-windows',                'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 76,  'name' => 'Garden Hand Tools',              'slug' => 'garden-hand-tools',                'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 77,  'name' => 'Garden Power Tools',             'slug' => 'garden-power-tools',               'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 78,  'name' => 'Hand Tools',                     'slug' => 'hand-tools',                       'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 79,  'name' => 'Ladders & Handtrucks',           'slug' => 'ladders-and-handtrucks',           'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 80,  'name' => 'Plumbing & Central Heating',     'slug' => 'plumbing-and-central-heating',     'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 81,  'name' => 'Power Tools',                    'slug' => 'power-tools',                      'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 82,  'name' => 'Protective Clothing & Workwear', 'slug' => 'protective-clothing-and-workwear', 'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 83,  'name' => 'Screws & Fixings',               'slug' => 'screws-and-fixings',               'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 84,  'name' => 'Tool Storage & Workbenches',     'slug' => 'tool-storage-and-workbenches',     'subCategoryId' => 17 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 85,  'name' => 'Wood & Timber',                  'slug' => 'wood-and-timber',                  'subCategoryId' => 17 ) );
        // For Sale->Health & Beauty
        DB::table('extra_sub_categories')->insert( array( 'id' => 86,  'name' => 'Bath & Body',                    'slug' => 'bath-and-body',                       'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 87,  'name' => 'Dental Care',                    'slug' => 'dental-care',                         'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 88,  'name' => 'Diet & Weight Loss',             'slug' => 'diet-and-weight-loss',                'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 89,  'name' => 'Facial Skin Care',               'slug' => 'facial-skin-care',                    'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 90,  'name' => 'Fragrances',                     'slug' => 'fragrances',                          'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 91,  'name' => 'Hair Care & Styling',            'slug' => 'hair-care-and-styling',               'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 92,  'name' => 'Health Care',                    'slug' => 'health-care',                         'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 93,  'name' => 'Make Up & Cosmetics',            'slug' => 'make-up-and-cosmetics',               'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 94,  'name' => 'Manicure & Pedicure',            'slug' => 'manicure-and-pedicure',               'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 95,  'name' => 'Massage Products',               'slug' => 'massage-products',                    'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 96,  'name' => 'Mobility, Disability & Medical', 'slug' => 'mobility-and-disability-and-medical', 'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 97,  'name' => 'Shaving & Hair Removal',         'slug' => 'shaving-and-hair-removal',            'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 98,  'name' => 'Sun Care & Tanning',             'slug' => 'sun-care-and-tanning',                'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 99,  'name' => 'Tattoo & Body Art',              'slug' => 'tattoo-and-body-art',                 'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 100, 'name' => 'Vision & Eye Care',              'slug' => 'vision-and-eye-care',                 'subCategoryId' => 21 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 101, 'name' => 'Vitamins & Supplements',         'slug' => 'vitamins-and-supplements',            'subCategoryId' => 21 ) );
        // For Sale->Home & Garden
        DB::table('extra_sub_categories')->insert( array( 'id' => 102, 'name' => 'Beds & Bedroom Furniture',      'slug' => 'beds-and-bedroom-furniture',   'subCategoryId' => 25 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 103, 'name' => 'Dining, Living Room Furniture', 'slug' => 'dining-living-room-furniture', 'subCategoryId' => 25 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 104, 'name' => 'Garden & Patio',                'slug' => 'garden-and-patio',             'subCategoryId' => 25 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 105, 'name' => 'Kitchenware & Accessories',     'slug' => 'kitchenware-and-accessories',  'subCategoryId' => 25 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 106, 'name' => 'Other Household Goods',         'slug' => 'other-household-goods',        'subCategoryId' => 25 ) );
        // For Sale->Music, Films, Books & Games
        DB::table('extra_sub_categories')->insert( array( 'id' => 107, 'name' => 'Books, Comics & Magazines',         'slug' => 'books-comics-and-magazines',        'subCategoryId' => 33 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 108, 'name' => 'Films & TV',                        'slug' => 'films-and-tv',                      'subCategoryId' => 33 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 109, 'name' => 'Games & Board Games',               'slug' => 'games-and-board-games',             'subCategoryId' => 33 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 110, 'name' => 'Music',                             'slug' => 'music',                             'subCategoryId' => 33 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 111, 'name' => 'Other Music, Films, Books & Games', 'slug' => 'other-music-films-books-and-games', 'subCategoryId' => 33 ) );
        // For Sale->Musical Instruments & DJ Equipment
        DB::table('extra_sub_categories')->insert( array( 'id' => 112, 'name' => 'Brass Musical Instruments',     'slug' => 'brass-musical-instruments',       'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 113, 'name' => 'Guitars & Accessories',         'slug' => 'guitars-and-accessories',         'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 114, 'name' => 'Keyboards & Pianos',            'slug' => 'keyboards-and-pianos',            'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 115, 'name' => 'Other',                         'slug' => 'other',                           'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 116, 'name' => 'Percussion & Drums',            'slug' => 'percussion-and-drums',            'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 117, 'name' => 'Performance & DJ Equipment',    'slug' => 'performance-and-dj-equipment',    'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 118, 'name' => 'Sheet Music & Song Books',      'slug' => 'sheet-music-and-song-books',      'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 119, 'name' => 'String Musical Instruments',    'slug' => 'string-musical-instruments',      'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 120, 'name' => 'Studio & Live Music Equipment', 'slug' => 'studio-and-live-music-equipment', 'subCategoryId' => 14 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 121, 'name' => 'Woodwind Musical Instruments',  'slug' => 'woodwind-musical-instruments',    'subCategoryId' => 14 ) );
        // For Sale->Office Furniture & Equipment
        DB::table('extra_sub_categories')->insert( array( 'id' => 122, 'name' => 'Business For Sale',                'slug' => 'business-for-sale',                 'subCategoryId' => 18 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 123, 'name' => 'Medical & Laboratory Equipment',   'slug' => 'medical-and-laboratory-equipment',  'subCategoryId' => 18 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 124, 'name' => 'Office Furniture',                 'slug' => 'office-furniture',                  'subCategoryId' => 18 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 125, 'name' => 'Packaging & Mailing Supplies',     'slug' => 'packaging-and-mailing-supplies',    'subCategoryId' => 18 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 126, 'name' => 'Restaurant & Catering Equipment',  'slug' => 'restaurant-and-catering-equipment', 'subCategoryId' => 18 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 127, 'name' => 'Retail & Shop Fittings',           'slug' => 'retail-and-shop-fittings',          'subCategoryId' => 18 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 128, 'name' => 'Supplies, Equipment & Stationery', 'slug' => 'supplies-equipment-and-stationery', 'subCategoryId' => 18 ) );
        // For Sale->Phones, Mobile Phones & Telecoms
        DB::table('extra_sub_categories')->insert( array( 'id' => 129, 'name' => 'Home Phones & Answering Machines', 'slug' => 'home-phones-and-answering-machines', 'subCategoryId' => 22 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 130, 'name' => 'Mobile Phone Accessories',         'slug' => 'mobile-phone-accessories',           'subCategoryId' => 22 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 131, 'name' => 'Mobile Phones',                    'slug' => 'mobile-phones',                      'subCategoryId' => 22 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 132, 'name' => 'Radio Communication Equipment',    'slug' => 'radio-communication-equipment',      'subCategoryId' => 22 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 133, 'name' => 'SIM Cards',                        'slug' => 'sim-cards',                          'subCategoryId' => 22 ) );
        // For Sale->Sports, Leisure & Travel
        DB::table('extra_sub_categories')->insert( array( 'id' => 134, 'name' => 'Ball & Racquet Sport Equipment',  'slug' => 'ball-and-racquet-sport-equipment',  'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 135, 'name' => 'Bicycle Accessories',             'slug' => 'bicycle-accessories',               'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 136, 'name' => 'Bicycles',                        'slug' => 'bicycles',                          'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 137, 'name' => 'Boxing & Martial Arts Equipment', 'slug' => 'boxing-and-martial-arts-equipment', 'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 138, 'name' => 'Camping & Hiking',                'slug' => 'camping-and-hiking',                'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 139, 'name' => 'Fishing Equipment',               'slug' => 'fishing-equipment',                 'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 140, 'name' => 'Fitness & Gym Equipment',         'slug' => 'fitness-and-gym-equipment',         'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 141, 'name' => 'Golf Equipment',                  'slug' => 'golf-equipment',                    'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 142, 'name' => 'Gym Membership',                  'slug' => 'gym-membership',                    'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 143, 'name' => 'Inline Skates & Skateboards',     'slug' => 'inline-skates-and-skateboards',     'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 144, 'name' => 'Luggage & Travel Equipment',      'slug' => 'luggage-and-travel-equipment',      'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 145, 'name' => 'Other Sports & Leisure',          'slug' => 'Other-sports-and-leisure',          'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 146, 'name' => 'Water Sports',                    'slug' => 'water-sports',                      'subCategoryId' => 26 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 147, 'name' => 'Winter Sports',                   'slug' => 'winter-sports',                     'subCategoryId' => 26 ) );
        // For Sale->Tickets
        DB::table('extra_sub_categories')->insert( array( 'id' => 148, 'name' => 'Comedy & Theatre', 'slug' => 'comedy-and-theatre', 'subCategoryId' => 30 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 149, 'name' => 'Concerts',         'slug' => 'concerts',           'subCategoryId' => 30 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 150, 'name' => 'Days Out',         'slug' => 'days-out',           'subCategoryId' => 30 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 151, 'name' => 'Sports',           'slug' => 'sports',             'subCategoryId' => 30 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 152, 'name' => 'Travel',           'slug' => 'travel',             'subCategoryId' => 30 ) );
        // For Sale->TV, DVD, Blu-ray & Videos
        DB::table('extra_sub_categories')->insert( array( 'id' => 153, 'name' => 'Blu-Ray Players & Recorders',    'slug' => 'blu-ray-players-and-recorders',    'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 154, 'name' => 'DVD Players & Recorders',        'slug' => 'dvd-players-and-recorders',        'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 155, 'name' => 'GPS Devices',                    'slug' => 'gps-devices',                      'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 156, 'name' => 'Other TV, DVD & Video',          'slug' => 'other-tv-dvd-and-video',           'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 157, 'name' => 'Portable DVD & Blu-ray Players', 'slug' => 'portable-dvd-and-blu-ray-players', 'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 158, 'name' => 'Satellite & Cable Equipment',    'slug' => 'satellite-and-cable-equipment',    'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 159, 'name' => 'Security & Surveillance System', 'slug' => 'security-and-surveillance-system', 'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 160, 'name' => 'Televisions, Plasma & LCD TVs',  'slug' => 'televisions-plasma-and-lcd-tvs',   'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 161, 'name' => 'TV Projectors',                  'slug' => 'tv-projectors',                    'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 162, 'name' => 'TV Reception & Set-Top Boxes',   'slug' => 'tv-reception-and-set-top-boxes',   'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 163, 'name' => 'TV, DVD & VCR Accessories',      'slug' => 'tv-dvd-and-vcr-accessories',       'subCategoryId' => 34 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 164, 'name' => 'Video Players & Recorders',      'slug' => 'video-players-and-recorders',      'subCategoryId' => 34 ) );
        // For Sale->Video Games & Consoles
        DB::table('extra_sub_categories')->insert( array( 'id' => 165, 'name' => 'Consoles',                     'slug' => 'consoles',                       'subCategoryId' => 15 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 166, 'name' => 'Games',                        'slug' => 'games',                          'subCategoryId' => 15 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 167, 'name' => 'Gaming Merchandise',           'slug' => 'gaming-merchandise',             'subCategoryId' => 15 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 168, 'name' => 'Other Video Games & Consoles', 'slug' => 'other-video-games-and-consoles', 'subCategoryId' => 15 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 169, 'name' => 'Replacement Parts & Tools',    'slug' => 'replacement-parts-and-tools',    'subCategoryId' => 15 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 170, 'name' => 'Video Game Accessories',       'slug' => 'video-game-accessories',         'subCategoryId' => 15 ) );
        // For Sale->Other Goods
        DB::table('extra_sub_categories')->insert( array( 'id' => 171, 'name' => 'Antiques',                          'slug' => 'antiques',                          'subCategoryId' => 23 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 172, 'name' => 'Aquariums',                         'slug' => 'aquariums',                         'subCategoryId' => 23 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 173, 'name' => 'Health & Beauty',                   'slug' => 'health-and-beauty',                 'subCategoryId' => 23 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 174, 'name' => 'Hobbies, Interests & Collectibles', 'slug' => 'hobbies-interest-and-collectibles', 'subCategoryId' => 23 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 175, 'name' => 'Other',                             'slug' => 'other',                             'subCategoryId' => 23 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 176, 'name' => 'Scrapbooking, Sewing, Art, Craft',  'slug' => 'scrapbooking-sewing-art-craft',     'subCategoryId' => 23 ) );
        // For Sale->Stuff Wanted
        DB::table('extra_sub_categories')->insert( array( 'id' => 177, 'name' => 'Audio & Vision',     'slug' => 'stereos-and-accessories', 'subCategoryId' => 27 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 178, 'name' => 'Computing & Phones', 'slug' => 'computing-and-phones',    'subCategoryId' => 27 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 179, 'name' => 'Household',          'slug' => 'household',               'subCategoryId' => 27 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 180, 'name' => 'Miscellaneous',      'slug' => 'miscellaneous',           'subCategoryId' => 27 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 181, 'name' => 'Sports & Leisure',   'slug' => 'sports-and-leisure',      'subCategoryId' => 27 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 182, 'name' => 'Transport',          'slug' => 'transport',               'subCategoryId' => 27 ) );
        // Property->Commercial
        DB::table('extra_sub_categories')->insert( array( 'id' => 183, 'name' => 'For Sale', 'slug' => 'for-sale', 'subCategoryId' => 39 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 184, 'name' => 'To Rent',  'slug' => 'to-rent',  'subCategoryId' => 39 ) );
        // Property->Parking & Garage
        DB::table('extra_sub_categories')->insert( array( 'id' => 185, 'name' => 'For Sale', 'slug' => 'for-sale', 'subCategoryId' => 42 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 186, 'name' => 'To Rent',  'slug' => 'to-rent',  'subCategoryId' => 42 ) );
        // Jobs - TO BE ADDED
        // Services->Business & Office
        DB::table('extra_sub_categories')->insert( array( 'id' => 187, 'name' => 'Accounting', 'slug' => 'accounting', 'subCategoryId' => 79 ) );
        // Community->Music, Bands & Musicians
        DB::table('extra_sub_categories')->insert( array( 'id' => 188, 'name' => 'Musicians Available', 'slug' => 'musicians-available', 'subCategoryId' => 105 ) );
        DB::table('extra_sub_categories')->insert( array( 'id' => 189, 'name' => 'Musicians Wanted',    'slug' => 'musicians-wanted',    'subCategoryId' => 105 ) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_sub_categories');
    }
}
