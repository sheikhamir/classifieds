@extends('layouts.app')

@section('title')
My detail - {{ config('app.name') }}
@endsection

@section('body-content')
    <div class="container-fluid user-tabs-container">
        <div class="row noMarginRow">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li><a href="{{ route('favourites') }}">Favourites</a></li>
                <li><a href="{{ route('alerts') }}">Alerts</a></li>
                <li><a href="{{ route('messages') }}">Messages</a></li>
                <li class="active"><a href="{{ route('update-profile') }}">My details</a></li>
            </ul>
        </div>
    </div>
    <div class="container user-settings-container">
        <div class="row noMarginRow">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="user-settings">
                    <div class="row noMarginRow">
                        <div class="col-xs-12 heading">{{ $user->name }}</div>
                    </div>
                    <div class="row noMarginRow">
                        <div class="col-sm-12 title">Basic information</div>
                    </div>
                    <div class="row noMarginRow">
                        <div class="col-sm-6 item">
                            <div class="title">Email</div>
                            <div class="content <?php echo $email_verified ? 'verified' : 'not-verified'; ?>">{{ $user->email }}</div>
                            <div class="content"><?php echo isset( $status ) ? $status : ''; ?></div>
                            <?php if ( !$email_verified ): ?>
                            <div class="content norm">Didn't get a verification email? <a href="{{ route( 'verification.resend' ) }}">Resend email</a></div>
                            <?php endif; ?>
                            <?php if( session('success') ): ?>
                            <div class="content norm text-success" style="font-weight:bold;">
                                {{session('success')}}
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-sm-6 item password-updater">
                            <div class="title">Password</div>
                            <div class="content norm"><a href="#" data-toggle="modal" data-target="#_cp">Change password</a></div>
                            @if (session('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif
                            <!-- Modal -->
                            <div class="modal fade" id="_cp" tabindex="-1" role="dialog" aria-labelledby="Change password">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('change-password') }}" method="post" onsubmit="event.preventDefault();changePassword(this)" id="change_password_form">@csrf @method('PUT')
                                            <input type="hidden" name="_type" value="password"/>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="changePassword">Change your password</h4>
                                            </div>
                                            <div class="modal-body inputs-have-no-radius">
                                                <div class="row noMarginRow error-container">
                                                    <div class="col-xs-12 error pwd-error"></div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-6 item">
                                                        <label for="opw">Current password *</label>
                                                        <input type="password" class="form-control" name="old_password" id="opw" required="required" aria-required="required"/>
                                                    </div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-6 item">
                                                        <label for="npw">New password *</label>
                                                        <input type="password" class="form-control" name="new_password" id="npw" required="required" aria-required="required"/>
                                                    </div>
                                                    <div class="col-sm-6 item">
                                                        <label for="cpw">Confirm password *</label>
                                                        <input type="password" class="form-control" name="con_password" id="cpw" required="required" aria-required="required"/>
                                                    </div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-6 item">
                                                        <button type="submit" class="btn btn-pink btn-block change-password-button">Change my password</button>
                                                    </div>
                                                    <div class="col-sm-6 item hidden">
                                                        <button type="button" class="btn btn-warning btn-block" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                    <div class="col-sm-12 item text-danger no-padding">* required fields</div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- Rset form when modal closes --}}
                            <script type="text/javascript">$("#_cp").on("hidden.bs.modal",function(e){document.getElementById("change_password_form").reset();$('.pwd-error').html('')});</script>
                        </div>
                    </div>
                    <div class="row noMarginRow">
                        <div class="col-sm-6 item">
                            <div class="title">Name</div>
                            <div class="content">{{ $user->name }}</div>
                        </div>
                        <div class="col-sm-6 item contact-number">
                            <div class="title">Contact number</div>
                            <?php if ( $user->phone == null ): ?>
                            <div class="content"><a href="#" data-toggle="modal" data-target="#_an">Add contact number</a></div>
                            <!-- Modal -->
                            <div class="modal fade" id="_an" tabindex="-1" role="dialog" aria-labelledby="Add contact number">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('add-number') }}" method="post" onsubmit="event.preventDefault();addContactNumber(this)" id="add_contact_number">@csrf
                                            <input type="hidden" name="_type" value="password"/>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="addContactNumber">Add contact number</h4>
                                            </div>
                                            <div class="modal-body inputs-have-no-radius">
                                                <div class="row noMarginRow error-container">
                                                    <div class="col-xs-12 error cn-error"></div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-12 item">
                                                        <label for="contact_number">Contact number *</label>
                                                        <input type="number" class="form-control" name="contact_number" id="contact_number" required="required" aria-required="required"/>
                                                    </div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-12 item">
                                                        <button type="submit" class="btn btn-pink btn-block add-contact-number">Add number</button>
                                                    </div>
                                                    <div class="col-sm-12 item text-danger no-padding">* required fields</div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- Reset form when modal closes --}}
                            <script type="text/javascript">$("#_an").on("hidden.bs.modal",function(e){document.getElementById("add_contact_number").reset();$('.cn-error').html('')});</script>
                            <?php else: ?>
                            <div class="content ocn">{{ $user->phone }}</div>
                            <div class="content norm"><a href="#" data-toggle="modal" data-target="#_un">Update number</a></div>
                            <!-- Modal -->
                            <div class="modal fade" id="_un" tabindex="-1" role="dialog" aria-labelledby="Add contact number">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('update-number') }}" method="post" onsubmit="event.preventDefault();addContactNumber(this)" id="add_contact_number">@csrf
                                            <input type="hidden" name="_type" value="password"/>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="updateContactNumber">Update contact number</h4>
                                            </div>
                                            <div class="modal-body inputs-have-no-radius">
                                                <div class="row noMarginRow error-container">
                                                    <div class="col-xs-12 error cn-error"></div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-12 item">
                                                        <label for="contact_number">Contact number *</label>
                                                        <input type="number" class="form-control" name="contact_number" id="contact_number" required="required" aria-required="required" value="{{ $user->phone }}"/>
                                                    </div>
                                                </div>
                                                <div class="row noMarginRow">
                                                    <div class="col-sm-12 item">
                                                        <button type="submit" class="btn btn-pink btn-block add-contact-number">Update number</button>
                                                    </div>
                                                    <div class="col-sm-12 item text-danger no-padding">* required fields</div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- Reset form when modal closes --}}
                            <script type="text/javascript">$("#_un").on("hidden.bs.modal",function(e){document.getElementById("add_contact_number").reset();$('.cn-error').html('')});</script>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="row noMarginRow">
                        <div class="col-sm-12 title">Email notifications and promotions</div>
                    </div>

                    <div class="row noMarginRow">
                        <div class="col-xs-6 item">
                            <div class="title">Email notifications</div>
                            <div class="content text-success"><b>Turned on</b></div>
                            <div class="content"><a href="#" class="btn btn-danger btn-sm">Turn off</a></div>
                        </div>
                        <div class="col-xs-6 item">
                            <div class="title">Promotions</div>
                            <div class="content text-success"><b>Turned on</b></div>
                            <div class="content"><a href="#" class="btn btn-danger btn-sm">Turn off</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection