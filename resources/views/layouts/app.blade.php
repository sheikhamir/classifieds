<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="unloaded">
    @include('includes.header')
    <body class="unloaded">
        @include('includes.navbar')

        @yield('body-content')
    </body>
    @include('includes.footer')
</html>
