<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    // Table name
    protected $table = 'images';
    // Primary Key
    public $primaryKey = 'id';
}
