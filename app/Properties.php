<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    // Table name
    protected $table = 'properties';
    // Primary Key
    public $primaryKey = 'id';
}
