<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsMeta extends Model
{
    // Table name
    protected $table = 'ads_meta';
    // Primary Key
    public $primaryKey = 'id';
}
