<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('includes.admin.header')
    <body>
        <div class="wrapper">
            @include('includes.admin.sidebar')
            <!-- Page content -->
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row noMarginRow">
                        <div class="col-xs-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    @include('includes.admin.footer')
</html>
