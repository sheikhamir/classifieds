@extends('layouts.app')

@section('title')
{{ $title }}
@endsection

@section('body-content')
<!-- Advertisement / Ads content here -->
<div class="ads-container position-5">
    <img src="{{ asset( 'img/ads/audi-ad.svg' ) }}"/>
</div>

<div class="container-fluid ad-post-form-container">
    <div class="row noMarginRow">
        <div class="col-xs-12 ad-post-form-container-inside">
            <div class="load-defaults">
                <h2>Post an ad</h2>
                <form action="{{ route('drafts') }}" method="POST" name="post-an-ad-form" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_d" value="{{ $draftId }}"/>
                    <div class="col-xs-12 form-section">
                        <div class="row ad-info-item postal-code">
                            <div class="title">Postal Code <span>(Mandatory)</span></div>
                            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                            <input type="text" class="ad-postal form-control input-lg" id="post-postal_code" name="postal-code"  required autocomplete="off" spellcheck="true" aria-required="required" aria-autocomplete="off" max="100"/>
                            <div class="test0 col-sm-6"></div>
                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                            <script>
                                function mydump(arr,level) {
                                    var dumped_text = "<table class='table table-responsive'>";
                                    if(!level) level = 0;

                                    var level_padding = "";
                                    for(var j=0;j<level+1;j++) level_padding += "    ";

                                    if(typeof(arr) == 'object') {
                                        for(var item in arr) {
                                            var value = arr[item];

                                            if(typeof(value) == 'object') {
                                                dumped_text += level_padding + "<tr><th>" + item + "</th>\n";
                                                dumped_text += '<td>'+mydump(value,level+1)+'</td></tr>';
                                            } else {
                                                dumped_text += level_padding + "<tr><th>" + item + "</th><td>" + value + "</td></tr>\n";
                                            }
                                        }
                                    } else {
                                        dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
                                    }
                                    dumped_text += '</table>';
                                    return dumped_text;
                                }
                              $(function() {
                                $( "#post-postal_code" ).autocomplete({
                                  source: function( request, response ) {
                                    $.ajax({
                                      url: "https://api.postcodes.io/postcodes/" + request.term + "/autocomplete",
                                      dataType: "json",
                                      success: function( data ) {
                                        response( data.result );
                                      }
                                    });
                                  },
                                  minLength: 2,
                                  select: function( event, ui ) {
                                    if ( ui.item ) {
                                    $.ajax({
                                      url: "https://api.postcodes.io/postcodes/" + ui.item.label,
                                      dataType: "json",
                                      success: function( data ) {
                                        $('.test0').html( mydump( data.result ) );
                                      }
                                    });
                                    }
                                    /*console.log( ui.item ?
                                      "Selected: " + ui.item.label :
                                      "Nothing selected, input was " + this.value);*/
                                  },
                                  open: function() {
                                    $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                                  },
                                  close: function() {
                                    $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                                  }
                                });
                              });
                            </script>
                            <br/><sub>Please enter your postal code</sub>
                        </div>
                        <div class="row ad-info-item images-container">
                            <div class="title">Ad Images</div>
                            <div class="upload-images images-inline"><span class="temp-images"></span><span class="uploaded-images"></span></div><button
                                class="image-btn image-add-button images-inline" type="button"><i class="glyphicon glyphicon-plus-sign"></i> Upload Images</button>
                            <input id="adImages" onchange="readmultifiles(this.files)" type="file" name="ad-images[]" multiple accept=".png,.jpg,.jpeg,.bmp" style="display:none;" class="ad-image"/>
                            <div class="video-link-container">
                                <div class="col-sm-6 video-input">
                                    <label for="ad-video">Add a YouTube video link</label>
                                    <input type="text" id="ad-video" onkeyup="loadVideoInIframe(this.value)" class="ad-title form-control input-lg" name="ad-video" autocomplete="off" aria-autocomplete="off"/>
                                </div>
                                <div class="col-sm-6 video-preview hidden">
                                    <label for="ad-video">&nbsp;</label>
                                    <iframe id="previewVideo" class="previewVideo"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="row ad-info-item bg-danger">
                            <div class="title">Category <span>(Mandatory)</span></div>
                            <div class="col-sm-6 category">
                                <select class="form-control input-lg" name="ad-category">
                                    <option>- Category -</option>
                                    <option value="motor">Motor</option>
                                    <option value="for-sale">For Sale</option>
                                    <option value="property">Property</option>
                                    <option value="jobs">Jobs</option>
                                    <option value="services">Services</option>
                                    <option value="community">Community</option>
                                    <option value="pets">Pets</option>
                                    <option value="dating">Dating</option>
                                </select>
                            </div>
                            <div class="col-sm-6 sub-category">
                                <select class="form-control input-lg motor hidden" name="sub-cat[motor]">
                                    <option value="car">Cars</option>
                                    <option value="motorbike-scooter">Motorbike & Scooter</option>
                                    <option value="van">Vans</option>
                                    <option value="campervan-motorhome">Campervan & Motorhome</option>
                                    <option value="caravan">Caravan</option>
                                    <option value="truck">Truck</option>
                                    <option value="plant-tractor">Plant & Tractor</option>
                                    <option value="other">Other Vehicles</option>
                                    <option value="accessories">Accessories</option>
                                    <option value="parts">Parts</option>
                                    <option value="wanted">Wanted</option>
                                </select>
                            </div>
                        </div>
                        <div class="row ad-info-item">
                            <div class="title">Ad Title <span>(Mandatory)</span></div>
                            <input type="text" class="ad-title form-control input-lg" id="post-ad_title" name="ad-title"  required autocomplete="off" spellcheck="true" aria-required="required" aria-autocomplete="off" max="100"/>
                            <br/><sub>Write a short description of your ad and include all of the key highlights.</sub>
                        </div>
                        <div class="row ad-info-item">
                            <div class="title">Description <span>(Mandatory)</span></div>
                            <textarea class="description form-control input-lg" name="description" aria-required="required" required spellcheck="true"></textarea>
                            <br/><sub>Minimum 11 words required! Please enter a brief description of what you are selling!</sub>
                        </div>
                        <div class="row ad-info-item">
                            <div class="title">Seller Type <span>(Mandatory)</span></div>
                            <div class="seller-type-inline">
                                <div class="input-label">
                                    <input type="radio" name="seller-type" value="private" id="private" class="form-control" checked>
                                </div><label for="private" class="input-label">Private</label>
                            </div><div class="seller-type-inline">
                                <div class="input-label">
                                    <input type="radio" name="seller-type" value="business" id="business" class="form-control">
                                </div><label for="business" class="input-label">Business</label>
                            </div>
                        </div>
                        <div class="row ad-info-item">
                            <div class="title">Price <span>(Mandatory)</span></div>
                            <div class="col-sm-6 price-container">
                                <label for="price"><i class="glyphicon glyphicon-gbp"></i></label>
                                <input class="price form-control input-lg" type="number" name="price" id="price" min="0" aria-valuemin="1" step="10"/>
                            </div>
                        </div>
                        <div class="row ad-info-item">
                            <div class="title">Contact information <span>(Mandatory)</span></div>
                            <div class="col-sm-6 contact-info-container">
                                <label for="email"><i class="glyphicon glyphicon-envelope"></i> Email</label>
                                <input class="price form-control input-lg" type="email" name="seller-email" id="email"/>
                            </div>
                            <div class="col-sm-6 contact-info-container">
                                <label for="phone"><i class="glyphicon glyphicon-earphone"></i> Phone Number</label>
                                <input class="price form-control input-lg" type="text" name="seller-phone" id="phone"/>
                            </div>
                        </div>
                        <div class="row noMarginRow text-right submit-container">
                            <div class="col-xs-12 col-sm-5 col-sm-offset-7 col-md-4 col-md-offset-8 remove-padding">
                                <input type="submit" class="btn btn-primary btn-lg btn-block" name="post-an-ad-submit" value="Post My Ad"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function() {
        if (typeof window.FileReader !== 'function') {
            alert("The file API isn't supported on this browser yet, so will not see a preview of the files");
        }
        $('.image-add-button').click(function () {
            $('#adImages').click();
        });
    };
    function readmultifiles( files ) {

        //$('.temp-images').empty();
        var data = new FormData();
        var extra = {_token:'{{ csrf_token() }}',draft:'{{ $draftId }}'};
        for ( var key in extra ) {
            data.append(key, extra[key]);
        }
        jQuery.each(jQuery('#adImages')[0].files, function(i, file) {
            data.append('ad-images['+i+']', file);
        });
        //data.append('_token', <?php echo csrf_token(); ?>);
        console.log(data);
        jQuery.ajax({
            url: '{{ route('upload-image') }}',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            type: 'POST', // For jQuery < 1.9
            success: function(data){
                $('.uploaded-images').html(data);
            }
        });
        //images = $("#adImages")[0].files;

        if ( parseInt( files.length ) > 10){
            alert("You are only allowed to upload a maximum of 10 files");
        } else {
            // Read first file
            setup_reader(files, 0);
        }
    }
    // Don't define functions in functions in functions, when possible.
    function setup_reader(files, i) {
        var file = files[i];
        var name = file.name;
        var reader = new FileReader();
        reader.onload = function(e){
            readerLoaded(e, files, i, name);
        };
        reader.onerror = function(event) {
            console.log("Image Read Error: " + event.target.error.code);
        };
        //reader.readAsBinaryString(file);
        reader.readAsDataURL(file);
        // After reading, read the next file.
    }
    function readerLoaded(e, files, i, name) {
        // get file content
        var bin = e.target.result;
        // Render the image
        $('.temp-images').append('<a href="#" class="tmp-img'+i+' im-ln-cl temp"><img src="'+bin+'" class="image" id="tmp-img'+i+'"/></a><a href="#" class="del-image tmp-img'+i+'"><i class="glyphicon glyphicon-trash"></i> Remove</a>');
        // If there's a file left to load
        if (i < files.length - 1) {
            // Load the next file
            setup_reader(files, i+1);
        }
    }
    function loadVideoInIframe(qs) {
        var videoFrame = $('.previewVideo'), videoPreview = $('.video-preview'), videoUrl;
        videoPreview.addClass('hidden')
        videoFrame.removeAttr('src');
        qs = qs.split('?')[1];
        if ( qs != '' || typeof qs !== 'undefined' ) {
            qs = qs.split('+').join(' ');
            var params = {},
                tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;
            while ( tokens = re.exec( qs ) ) {
                params[ decodeURIComponent( tokens[1] ) ] = decodeURIComponent( tokens[2] );
            }
            if ( typeof params.v === 'undefined' || params.v == '' ) {
                videoPreview.addClass('hidden')
                videoFrame.removeAttr('src');
            } else {
                videoUrl = 'https://youtube.com/embed/' + params.v;
                videoFrame.attr('src', videoUrl);
                videoPreview.removeClass('hidden');
            }
        }
    }
    function rm (el) {
        $("#"+el).remove();
        $("."+el).remove();
    }
</script>
@endsection