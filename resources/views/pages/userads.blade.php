<?php
use App\Categories;
use App\PostalCodes;
use App\Images;
use App\User;
$title = $ad->adTitle . " - " . config('app.name');
?>@extends('layouts.search')

@section('title', $title)

@section('body-content')
<div class="container-fluid content-container ad-view-container constrained-width">
    <div class="row noMarginRow ad-view">
        <div class="col-xs-12 breadcrumb-container">
            <ol class="breadcrumb">
              <li><a href="#">&lt; Back</a></li>
              <li><a href="#">{{ $category }}</a></li>
              <li><a href="#">{{ $subCategory }}</a></li>
              <?php if ( !empty( $extraSubCategory ) ) { ?>
              <li><a href="#">{{ $extraSubCategory }}</a></li>
              <?php } ?>
            </ol>
        </div>
        <div class="col-lg-8 col-md-8 ad-view-details ad-content">
            <div class="item ad-title">{{ $ad->adTitle }}</div>
            <div class="item ad-location">{{ $location }}</div>
            <div class="item price">
                <div class="price">&pound;{{ number_format( $ad->adAskingPrice ) }}</div>
            </div>
            <div class="item ad-gallery">
                <!-- 1. Link to jQuery (1.8 or later), -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <!-- 33 KB -->
                <!-- fotorama.css & fotorama.js. -->
                <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
                <!-- 2. Add images to <div class="fotorama"></div>. -->
                <div class="fotorama" data-nav="thumbs" data-width="100%" data-ratio="800/600" data-keyboard="true" data-allowfullscreen="true"><?php
                    foreach( $photos as $photo ) {
                        $photo_url = $photo->image;
                        $photo_alt = $ad->adTitle;
                        ?><img src="{{ asset( $photo_url ) }}" alt="{{ $photo_alt }}"><?php
                    }
                ?></div>
            </div>
            <div class="item posted">
            Posted {{ $ad->created_at }}
            </div>
            <div class="item description">
                <div class="title">Description</div>
                <div class="content"><?php echo $description; ?></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 ad-view-details seller-details-container">
            <div class="seller-details">
                <div class="item name">
                    <div>{{ $user->name }}</div>
                    <div class="link-container"><a href="#" class="link">View Profile</a></div>
                </div>
                <div class="item user-since">Posting since {{ explode(' ', $user->created_at)[0] }}</div>
                <div class="item contact-heading">Contact {{ $user->name }}</div>
                <div class="item contact-phone"><i class="glyphicon glyphicon-earphone"></i> {{ $phone }}</div>
                <div class="item ad-actions-container">
                    <button type="button" class="btn btn-group btn-default"><i class="glyphicon glyphicon-heart"> </i> Favourite</button>
                    <button type="button" class="btn btn-group btn-default"><i class="glyphicon glyphicon-warning-sign"> </i> Report</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid constrained-width">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="top-info-title searches"><a href="#" class="active" dataopen=".top-info-links.searches">Top searches <i class="glyphicon glyphicon-chevron-down visible-xs-inline"></i></a></div><div class="top-info-links searches">
                <a href="#">cars</a><span class="divider"><!--&#124;--></span><a href="#">free</a><span class="divider"><!--&#124;--></span><a href="#">puppies</a><span class="divider"><!--&#124;--></span><a href="#">kittens</a><span class="divider"><!--&#124;--></span><a href="#">office floor to rent</a><span class="divider"><!--&#124;--></span><a href="#">sofa</a><span class="divider"><!--&#124;--></span><a href="#">garden</a><span class="divider"><!--&#124;--></span><a href="#">cheap cars</a><span class="divider"><!--&#124;--></span><a href="#">dogs</a><span class="divider"><!--&#124;--></span><a href="#">vans</a><span class="divider"><!--&#124;--></span><a href="#">room</a><span class="divider"><!--&#124;--></span><a href="#">caravan</a><span class="divider"><!--&#124;--></span><a href="#">lego</a><span class="divider"><!--&#124;--></span><a href="#">boat</a><span class="divider"><!--&#124;--></span><a href="#">4x4</a><span class="divider"><!--&#124;--></span><a href="#">trailer</a><span class="divider"><!--&#124;--></span><a href="#">tv</a><span class="divider"><!--&#124;--></span><a href="#">motorcycle</a><span class="divider"><!--&#124;--></span><a href="#">7 seater</a><span class="divider"><!--&#124;--></span><a href="#">125cc</a><span class="divider"><!--&#124;--></span><a href="#">house</a><span class="divider"><!--&#124;--></span><a href="#">furniture</a><span class="divider"><!--&#124;--></span><a href="#">massage</a><span class="divider"><!--&#124;--></span><a href="#">bmw</a><span class="divider"><!--&#124;--></span><a href="#">table</a><span class="divider"><!--&#124;--></span><a href="#">room to rent</a><span class="divider"><!--&#124;--></span><a href="#">chair</a><span class="divider"><!--&#124;--></span><a href="#">2 bedroom house</a><span class="divider"><!--&#124;--></span><a href="#">chest of drawers</a><span class="divider"><!--&#124;--></span><a href="#">flat</a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="top-info-title locations"><a href="#" class="" dataopen=".top-info-links.locations">Top locations <i class="glyphicon glyphicon-chevron-down visible-xs-inline"></i></a></div><div class="top-info-links locations">
                <a href="#">cars</a><span class="divider"><!--&#124;--></span><a href="#">free</a><span class="divider"><!--&#124;--></span><a href="#">puppies</a><span class="divider"><!--&#124;--></span><a href="#">kittens</a><span class="divider"><!--&#124;--></span><a href="#">private house to rent</a><span class="divider"><!--&#124;--></span><a href="#">sofa</a><span class="divider"><!--&#124;--></span><a href="#">garden</a><span class="divider"><!--&#124;--></span><a href="#">cheap cars</a><span class="divider"><!--&#124;--></span><a href="#">dogs</a><span class="divider"><!--&#124;--></span><a href="#">vans</a><span class="divider"><!--&#124;--></span><a href="#">room</a><span class="divider"><!--&#124;--></span><a href="#">caravan</a><span class="divider"><!--&#124;--></span><a href="#">lego</a><span class="divider"><!--&#124;--></span><a href="#">boat</a><span class="divider"><!--&#124;--></span><a href="#">4x4</a><span class="divider"><!--&#124;--></span><a href="#">trailer</a><span class="divider"><!--&#124;--></span><a href="#">tv</a><span class="divider"><!--&#124;--></span><a href="#">spares or repair</a><span class="divider"><!--&#124;--></span><a href="#">motorcycle</a><span class="divider"><!--&#124;--></span><a href="#">7 seater</a><span class="divider"><!--&#124;--></span><a href="#">125cc</a><span class="divider"><!--&#124;--></span><a href="#">house</a><span class="divider"><!--&#124;--></span><a href="#">massage</a><span class="divider"><!--&#124;--></span><a href="#">bmw</a><span class="divider"><!--&#124;--></span><a href="#">table</a><span class="divider"><!--&#124;--></span><a href="#">room to rent</a><span class="divider"><!--&#124;--></span><a href="#">chair</a><span class="divider"><!--&#124;--></span><a href="#">2 bedroom house</a><span class="divider"><!--&#124;--></span><a href="#">chest of drawers</a><span class="divider"><!--&#124;--></span><a href="#">flat</a>
            </div>
        </div>
    </div>
</div>
@endsection