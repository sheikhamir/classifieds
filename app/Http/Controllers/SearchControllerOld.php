<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Search;
use App\Ads;
use App\AdsMeta;
use App\Categories;
use App\SubCategories;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){
        $_searchKeyword = ( !isset( $request->_searchKeyword ) ) ? $request->input('searchKeywords') : $request->_searchKeyword;
        if ( isset ( $request->searchType ) ) {
            $_searchType = $request->searchType;
        } else {
            $_searchType = $request->input('searchType');
        }
        $_postalCode = $request->input('postalCode');
        $user = (Auth::guest()) ? 'guest' : Auth::user()->email;
        // Check if the search exists
        $searchExists = Search::where('keywords', '=', $_searchKeyword)
            ->where('searchType', '=', $_searchType)
            ->where('postalCode', '=', $_postalCode)
            ->where('user', '=', $user)
            ->first();
        if ( $searchExists === null ) { // Search does not exist
            // Insert the search in the database
            $search = new Search;
            $search->keywords = $_searchKeyword;
            $search->searchType = $_searchType;
            $search->postalCode = $_postalCode;
            $search->user = $user;
            $search->save();
        } else { // Search exists
            // Update the search in the database
            $searches = $searchExists->searches + 1; // Increment number of searches by 1
            $search = Search::find( $searchExists->id );
            $search->searches = $searches;
            $search->save();
        }
        // Find the ads related to the search term
        if( empty( $_searchKeyword ) && empty( $_postalCode ) ) {// Search everything
            if ( $_searchType == 'all' ) {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->orderBy('category', 'DESC')
                    ->get();
            } else {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where('ads.category','=',$_searchType)
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where('ads.category','=',$_searchType)
                    ->orderBy('category', 'DESC')
                    ->get();
            }
        } elseif ( !empty( $_searchKeyword ) && empty( $_postalCode ) ) { // Search for query
            if ( $_searchType == 'all' ) {
                //DB::statement( DB::raw( "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));" ) );
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->orderBy('category', 'DESC')
                    ->get();
            } else {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where('ads.category','=',$_searchType)
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where('ads.category','=',$_searchType)
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->orderBy('category', 'DESC')
                    ->get();
            }
        } elseif ( empty( $_searchKeyword ) && !empty( $_postalCode ) ) { // Search for postal code
            if ( $_searchType == 'all' ) {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->orderBy('category', 'DESC')
                    ->get();
            } else {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where('ads.category','=',$_searchType)
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where('ads.category','=',$_searchType)
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->orderBy('category', 'DESC')
                    ->get();
            }
        } else { // Search for query and postal code
            if ( $_searchType == 'all' ) {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->orderBy('category', 'DESC')
                    ->get();
            } else {
                $ads = DB::table('ads', 'images')
                    ->select('ads.*', DB::raw('GROUP_CONCAT(images.image) AS ad_images'))
                    ->leftJoin('images', 'ads.adId', '=', 'images.adId')
                    ->where('ads.category','=',$_searchType)
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->groupBy('ads.adId')
                    ->orderBy('ads.featured', 'DESC')
                    ->orderBy('ads.spotlight', 'DESC')
                    ->orderBy('ads.urgent', 'DESC')
                    ->orderBy('ads.adId', 'DESC');
                $cats = DB::table('ads')
                    ->select(DB::raw('DISTINCT category'))
                    ->where('ads.category','=',$_searchType)
                    ->where( function( $query ) use ( $_searchKeyword ) {
                        $query->where('ads.adTitle', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adTitle', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adTitle', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.adDescription', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.adDescription', 'LIKE', "%{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}")
                            ->orWhere('ads.postalCode', 'LIKE', "{$_searchKeyword}%")
                            ->orWhere('ads.postalCode', 'LIKE', "%{$_searchKeyword}%");
                    })
                    ->orderBy('category', 'DESC')
                    ->get();
            }
        }
        $originalAd = $ads;
        $totalAds = $originalAd->get()->count();
        $searchedAds = $ads->paginate(2);
        //$fetchads = $originalAd->groupBy('ads.subCategory')->get();
        //$featuredAds = $originalAd->where('ads.featured','1')->get()->count();
        //$urgentAds = $originalAd->where('ads.urgent','1')->get()->count();
        //$spotlightAds = $originalAd->where('ads.spotlight','1')->get()->count();
        $featuredAds = 2;
        $urgentAds = 1;
        $spotlightAds = 1;

        //$ads = $ads->paginate(2);

        return view('pages.search')
            ->with([
                'ads' => $searchedAds,
                'cats' => $cats,
                '_searchKeyword' => $_searchKeyword,
                '_postalCode' => $_postalCode,
                '_searchType' => $_searchType,
                '_number_of_ads' => $totalAds,
                '_featuredAdsCount' => $featuredAds,
                '_urgentAdsCount' => $urgentAds,
                '_spotlightAdsCount' => $spotlightAds
            ]);
        //var_dump($ads);
    }
    public function show($id)
    {
        // Fetch the post
        $post = Ads::find($id);
        //$post = Post::where('title', 'post one' )->get();
        return view('blog.show')->with('post', $post);
    }
}
