<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('displayName')->nullable(); // Display name
            $table->string('displayPicture')->nullable(); // Display picture url
            $table->integer('email_notification')->default(1); // Subscribed to email notifications
            $table->integer('promotions')->default(1); // Subscribed to promotional emails
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('users')->insert(
            array(
                'name' => 'Sheikh Amir',
                'email' => 'smamir3@gmail.com',
                'email_verified_at' => DB::raw('CURRENT_TIMESTAMP'),
                'password' => '$2y$10$G9iR61sSu5SPJrd4LB7wQ.CwM4cbsKZSplOlwHDaziIVIuXlrp40i',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
