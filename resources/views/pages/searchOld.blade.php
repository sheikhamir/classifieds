<?php
use Illuminate\Support\Facades\Cookie;
use App\PostalCodes;
$AppName = config('app.name');
$search = isset($_searchKeyword) && !empty($_searchKeyword) ? "{$_searchKeyword} - $AppName" : "Search - $AppName";
?>@extends('layouts.search')

@section('title')
{{ $search }}
@endsection

@section('body-content')
<div class="container-fluid category-section searching hidden-xs">
    @include('components.desktopSearch')
</div>
@include('components.mobileSearch')
<div class="container-fluid content-container searching">
    <div class="row noMarginRow search-ads-container"><?php
        if ( ( isset( $cats ) && count( $cats ) > 0 ) && ( isset( $ads ) && count( $ads ) > 0 ) ) {
        ?><div class="col-xs-12 search-term hidden-xs hidden-sm"><?php
            $resultsFor = "{$_number_of_ads} ads for ";
            if ( isset( $_searchKeyword ) ) {
                if( empty( $_searchKeyword ) ) {
                    $resultsFor .= "everything";
                } else {
                    $resultsFor .= '"'.$_searchKeyword.'"';
                }
            }
            if ( isset( $_searchType ) && !empty( $_searchType ) ) {
                if ( $_searchType == 'all' ) {
                    $resultsFor .= " in all categories";
                } else {
                    $catName = catName( $_searchType );
                    $resultsFor .= " in \"{$catName}\"";
                }
            }
            if ( isset( $_postalCode ) && !empty ( $_postalCode ) ) {
                $postalCode = $_postalCode; // Fetch from the controller
                $postalCodeDetail = PostalCodes::where('postalCode',$postalCode)->first();
                $city = $postalCodeDetail->city;
                $country = $postalCodeDetail->country;
                $location = "{$city}, {$country}";
                $resultsFor .= " in {$location}";
            }
            echo "{$resultsFor}";
            ?><div class="search-actions">
                <a href="#">Save this search</a>
            </div>
        </div>
        <div class="hidden-xs col-sm-12 col-md-3 col-lg-2 search-filters-container">
            <div class="filter-wrapper categories-wrapper">
                <div class="title"><span class="material-icons"></span>Categories</div>
                <div class="content"><?php
                    if( count( $cats ) > 1 ) {
                        foreach( $cats as $cat ) {
                            $category_slug = $cat->category;
                            $category = catName( $category_slug );
                            $catLink = searchQuery( 'category', $category_slug );
                            echo "<div class='item'><a href=\"{$catLink}\">$category</a></div>";
                        }
                    } elseif( count( $cats ) != 0 ) {
                        $category_slug = $cats[0]->category;
                        $category = catName( $category_slug );
                        echo "<div class='item'><span class='active'>$category</span></div>";
                    }
                ?></div>
            </div>
            <div class="filter-wrapper filters">
                <div class="title">Filter</div>
                <div class="content">
                    <div class="title">Other options</div>
                    <div class="content"><?php
                    if ( isset ( $_featuredAdsCount ) && $_featuredAdsCount > 0 ) {
                        echo '<div class="item"><a href="#">Featured Ads (' . $_featuredAdsCount . ')</a></div>';
                    }
                    if ( isset ( $_urgentAdsCount ) && $_urgentAdsCount > 0 ) {
                        echo '<div class="item"><a href="#">Urgent Ads (' . $_urgentAdsCount . ')</a></div>';
                    }
                    if ( isset ( $_spotlightAdsCount ) && $_spotlightAdsCount > 0 ) {
                        echo '<div class="item"><a href="#">Spotlight Ads (' . $_spotlightAdsCount . ')</a></div>';
                    }
                    ?></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 hidden-md hidden-lg search-term">
        {{ $resultsFor }}
        <div class="search-actions">
            <a href="#">Save this search</a>
        </div>
        </div>
        <div class="col-sm-12 col-md-9 col-lg-10 search-ads"><?php
            foreach( $ads as $ad ){
                $adSlug = $ad->adSlug;
                $adTitle = $ad->adTitle;
                $adDesc = $ad->adDescription;
                /* Images
                 * Images are separated by ","
                 * We explode "," to get an array of images
                 */
                $images = $ad->ad_images;
                $images_array = explode(',',$images); // Create an array of images
                $displayImage = $images_array[0];
                $dateAdded = $ad->created_at;
                $adRawCategory = $ad->category;
                $adRawSubCategory = $ad->subCategory;
                $adRawExtraSubCategory = $ad->extraSubCategory;
                ?><button data-url="/ads/{{ $adSlug }}" class="ad-item" onclick="window.location=this.getAttribute('data-url');">
                    <div class="ad-details ad-image-container">
                        <a href="/ads/{{ $adSlug }}"><div class="ad-image" style="background-image:url({{ $displayImage }})"></div></a>
                    </div>
                    <div class="ad-details ad-details-container">
                        <div class="ad-title"><a href="/ads/{{ $adSlug }}">{{ $adTitle }}</a></div>
                        <div class="ad-desc hidden-xs hidden-sm">{{ summarise($adDesc, 220) }}</div>
                        <div class="ad-category">
                            <a class="catLink hidden-xs" href="/{{ $adRawCategory }}">{{ catName( $adRawCategory ) }}</a>
                            <span class="hidden-xs hidden-sm"> - </span>
                            <a class="catLink" href="/{{ $adRawCategory }}/{{ $adRawSubCategory }}">{{ catName( $adRawSubCategory, 'subcat' ) }}</a>
                            <?php echo (!empty($adRawExtraSubCategory)) ? "<span class=\"hidden-xs hidden-sm\"> - </span><a class=\"catLink\" href=\"/$adRawCategory/$adRawSubCategory/$adRawExtraSubCategory\">" . catName( $adRawExtraSubCategory, 'extracat' ) . "</a>" : ''; ?></div>
                        <div class="ad-date"><span class="hidden-xs">Added on </span>{{ $dateAdded }}</div>
                    </div>
                </button><?php
            }
            $queryStringArray = ['searchType' => $_searchType, 'searchKeywords' => $_searchKeyword, 'postalCode' => $_postalCode ];
            $ads->appends($queryStringArray);
            Cookie::queue('bts', get_current_url(), 30);
        ?>@if ( $ads->links() )
            <div class="pagination-container">
                {{ $ads->links() }}
            </div>
        @endif
        </div><?php
        } else {
            ?>
            <div class="row NoMarginRow">
                <div class="col-xs-12 col-lg-4 col-lg-offset-4 no-ads">
                    <h1>No Ads Found</h1>
                    <p>Search query returned no results!</p>
                    <ul class="text-left">
                        <li>Check for spelling mistakes</li>
                        <li>Remove any plural words</li>
                    </ul>
                </div>
            </div>
            <?php
        }
    ?></div>
</div>
<div class="container-fluid constrained-width">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="top-info-title searches"><a href="#" class="active" dataopen=".top-info-links.searches">Top searches <i class="glyphicon glyphicon-chevron-down visible-xs-inline"></i></a></div><div class="top-info-links searches">
            {{--<a href="#">cars</a><span class="divider"><!--&#124;--></span><a href="#">free</a><span class="divider"><!--&#124;--></span><a href="#">puppies</a><span class="divider"><!--&#124;--></span><a href="#">kittens</a><span class="divider"><!--&#124;--></span><a href="#">office floor to rent</a><span class="divider"><!--&#124;--></span><a href="#">sofa</a><span class="divider"><!--&#124;--></span><a href="#">garden</a><span class="divider"><!--&#124;--></span><a href="#">cheap cars</a><span class="divider"><!--&#124;--></span><a href="#">dogs</a><span class="divider"><!--&#124;--></span><a href="#">vans</a><span class="divider"><!--&#124;--></span><a href="#">room</a><span class="divider"><!--&#124;--></span><a href="#">caravan</a><span class="divider"><!--&#124;--></span><a href="#">lego</a><span class="divider"><!--&#124;--></span><a href="#">boat</a><span class="divider"><!--&#124;--></span><a href="#">4x4</a><span class="divider"><!--&#124;--></span><a href="#">trailer</a><span class="divider"><!--&#124;--></span><a href="#">tv</a><span class="divider"><!--&#124;--></span><a href="#">motorcycle</a><span class="divider"><!--&#124;--></span><a href="#">7 seater</a><span class="divider"><!--&#124;--></span><a href="#">125cc</a><span class="divider"><!--&#124;--></span><a href="#">house</a><span class="divider"><!--&#124;--></span><a href="#">furniture</a><span class="divider"><!--&#124;--></span><a href="#">massage</a><span class="divider"><!--&#124;--></span><a href="#">bmw</a><span class="divider"><!--&#124;--></span><a href="#">table</a><span class="divider"><!--&#124;--></span><a href="#">room to rent</a><span class="divider"><!--&#124;--></span><a href="#">chair</a><span class="divider"><!--&#124;--></span><a href="#">2 bedroom house</a><span class="divider"><!--&#124;--></span><a href="#">chest of drawers</a><span class="divider"><!--&#124;--></span><a href="#">flat</a>--}}
            {{ fetch_top_searches( 25 ) }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="top-info-title locations"><a href="#" class="" dataopen=".top-info-links.locations">Top locations <i class="glyphicon glyphicon-chevron-down visible-xs-inline"></i></a></div><div class="top-info-links locations">
                <a href="#">cars</a><span class="divider"><!--&#124;--></span><a href="#">free</a><span class="divider"><!--&#124;--></span><a href="#">puppies</a><span class="divider"><!--&#124;--></span><a href="#">kittens</a><span class="divider"><!--&#124;--></span><a href="#">private house to rent</a><span class="divider"><!--&#124;--></span><a href="#">sofa</a><span class="divider"><!--&#124;--></span><a href="#">garden</a><span class="divider"><!--&#124;--></span><a href="#">cheap cars</a><span class="divider"><!--&#124;--></span><a href="#">dogs</a><span class="divider"><!--&#124;--></span><a href="#">vans</a><span class="divider"><!--&#124;--></span><a href="#">room</a><span class="divider"><!--&#124;--></span><a href="#">caravan</a><span class="divider"><!--&#124;--></span><a href="#">lego</a><span class="divider"><!--&#124;--></span><a href="#">boat</a><span class="divider"><!--&#124;--></span><a href="#">4x4</a><span class="divider"><!--&#124;--></span><a href="#">trailer</a><span class="divider"><!--&#124;--></span><a href="#">tv</a><span class="divider"><!--&#124;--></span><a href="#">spares or repair</a><span class="divider"><!--&#124;--></span><a href="#">motorcycle</a><span class="divider"><!--&#124;--></span><a href="#">7 seater</a><span class="divider"><!--&#124;--></span><a href="#">125cc</a><span class="divider"><!--&#124;--></span><a href="#">house</a><span class="divider"><!--&#124;--></span><a href="#">massage</a><span class="divider"><!--&#124;--></span><a href="#">bmw</a><span class="divider"><!--&#124;--></span><a href="#">table</a><span class="divider"><!--&#124;--></span><a href="#">room to rent</a><span class="divider"><!--&#124;--></span><a href="#">chair</a><span class="divider"><!--&#124;--></span><a href="#">2 bedroom house</a><span class="divider"><!--&#124;--></span><a href="#">chest of drawers</a><span class="divider"><!--&#124;--></span><a href="#">flat</a>
            </div>
        </div>
    </div>
</div>
@endsection