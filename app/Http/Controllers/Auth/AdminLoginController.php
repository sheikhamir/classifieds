<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    public function login(Request $request)
    {
        // Validate the form
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        // Set credentials
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // Attempt to log the admin in
        if ( Auth::guard('admin')->attempt( $credentials, $request->remember ) ) {
            // Login success
            return redirect()->intended( route('admin.dashboard') );
        }
        // Login unsuccessful
        return redirect()->back()->withInput( $request->only( 'email', 'remember' ) );
    }
}
