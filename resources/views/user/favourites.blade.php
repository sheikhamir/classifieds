@extends('layouts.app')

@section('title')
Favourites - {{ config('app.name') }}
@endsection

@section('body-content')
    <div class="container-fluid user-tabs-container">
        <div class="row noMarginRow">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="{{ route('favourites') }}">Favourites</a></li>
                <li><a href="{{ route('alerts') }}">Alerts</a></li>
                <li><a href="{{ route('messages') }}">Messages</a></li>
                <li><a href="{{ route('update-profile') }}">My details</a></li>
            </ul>
        </div>
    </div>
    <div class="container-fluid default text-center">
        <div class="row noMarginRow">
            <div class="col-xs-12">
                <h1>Favourites</h1>
            </div>
        </div>
        <div class="row noMarginRow">
            <div class="col-xs-12">
                <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
            </div>
        </div>
    </div>
@endsection