@extends('layouts.app')

@section('title', config('app.name') . ' - Buy and sell online')

@section('body-content')
<div class="category-item-overlay hidden"></div>
<div class="container-fluid category-section hidden-xs">
    @include('components.desktopSearch')
    @include('components.categories')
</div>
@include('components.mobileSearch')
<div class="container-fluid content-container">
    <div class="constrained-width">
        <div class="content-inline ads">
            <div class="row noMarginRow ads-header hidden-xs">
                <div class="col-xs-4 text-left">
                    <a href="#" class="active"><div class="ads-header-title ads-header-inline">Most viewed</div><div class="ads-header-img top-viewed ads-header-inline"></div></a>
                </div><div class="col-xs-4 text-center">
                    <a href="#"><div class="ads-header-title ads-header-inline">Favorites</div><div class="ads-header-img my-favorites ads-header-inline"></div></a>
                </div><div class="col-xs-4 text-right">
                    <a href="#"><div class="ads-header-title ads-header-inline">My alerts</div><div class="ads-header-img my-alerts ads-header-inline"></div></a>
                </div>
            </div>
            <div class="row noMarginRow ads-header hidden-sm hidden-md hidden-lg">
                <h2>Most Viewed Ads</h2>
            </div>
            <div class="row noMarginRow ads-content">
                <a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-01.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">BOSCH Serie 4 Classixx DWW077A50B Chimney Cooker Hood - Stainless Steel</div>
                    <div class="ad-box-area">Easingwold</div>
                    <div class="ad-box-category">Cooker Hoods & Spashbacks</div>
                    <div class="ad-box-price">&pound;225</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-01.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">BOSCH Serie 4 Classixx DWW077A50B Chimney Cooker Hood - Stainless Steel</div>
                    <div class="ad-box-area">Easingwold</div>
                    <div class="ad-box-category">Cooker Hoods & Spashbacks</div>
                    <div class="ad-box-price">&pound;225</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-01.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">BOSCH Serie 4 Classixx DWW077A50B Chimney Cooker Hood - Stainless Steel</div>
                    <div class="ad-box-area">Easingwold</div>
                    <div class="ad-box-category">Cooker Hoods & Spashbacks</div>
                    <div class="ad-box-price">&pound;225</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-01.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">BOSCH Serie 4 Classixx DWW077A50B Chimney Cooker Hood - Stainless Steel</div>
                    <div class="ad-box-area">Easingwold</div>
                    <div class="ad-box-category">Cooker Hoods & Spashbacks</div>
                    <div class="ad-box-price">&pound;225</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-02.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Bedsit with balcony all bills &pound;850 a month</div>
                    <div class="ad-box-area">North West London</div>
                    <div class="ad-box-category">Property To Share</div>
                    <div class="ad-box-price">&pound;850pm</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url(' {{ asset('img/test-02.jpg') }} ') center no-repeat"></div>
                    <div class="ad-box-title">Bedsit with balcony all bills &pound;850 a month</div>
                    <div class="ad-box-area">North West London</div>
                    <div class="ad-box-category">Property To Share</div>
                    <div class="ad-box-price">&pound;850pm</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-02.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Bedsit with balcony all bills &pound;850 a month</div>
                    <div class="ad-box-area">North West London</div>
                    <div class="ad-box-category">Property To Share</div>
                    <div class="ad-box-price">&pound;850pm</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-02.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Bedsit with balcony all bills &pound;850 a month</div>
                    <div class="ad-box-area">North West London</div>
                    <div class="ad-box-category">Property To Share</div>
                    <div class="ad-box-price">&pound;850pm</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-03.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Kingspan Styrozone H350R-T Insulation</div>
                    <div class="ad-box-area">Leigh-on-Sea</div>
                    <div class="ad-box-category">Insulation</div>
                    <div class="ad-box-price">&pound;700</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-03.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Kingspan Styrozone H350R-T Insulation</div>
                    <div class="ad-box-area">Leigh-on-Sea</div>
                    <div class="ad-box-category">Insulation</div>
                    <div class="ad-box-price">&pound;700</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-03.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Kingspan Styrozone H350R-T Insulation</div>
                    <div class="ad-box-area">Leigh-on-Sea</div>
                    <div class="ad-box-category">Insulation</div>
                    <div class="ad-box-price">&pound;700</div>
                </a><a href="#" class="ad-box">
                    <div class="ad-box-img" style="background: url( {{ asset('img/test-03.jpg') }} ) center no-repeat"></div>
                    <div class="ad-box-title">Kingspan Styrozone H350R-T Insulation</div>
                    <div class="ad-box-area">Leigh-on-Sea</div>
                    <div class="ad-box-category">Insulation</div>
                    <div class="ad-box-price">&pound;700</div>
                </a>
            </div>
            <div class="row noMarginRow load-more">
                <a href="#" class="load-more-btn"><i class="glyphicon glyphicon-menu-down"></i> Show more ads <i class="glyphicon glyphicon-menu-down"></i></a>
            </div>
        </div><div class="content-inline adsense hidden-xs hidden-sm hidden-md">
            <div class="adsense-ad">Ads Here<br/><br/>Or<br/><br/>Featured Here</div>
            <div class="adsense-ad">Ads Here<br/><br/>Or<br/><br/>Featured Here</div>
        </div>
    </div>
</div>
@include('components.topinfo')
@endsection