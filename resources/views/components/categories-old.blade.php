<div class="category-section-content">
    <div class="visible-xs-block"><div class="category-section-title-xs">Browse by category <i class="glyphicon glyphicon-chevron-down"></i></div></div>
    <button class="category-item has-sub-category">
        <div class="category-item-img motors"></div>
        <div class="category-item-title">Motors</div>
        <div class="category-item-desc">Vehicles &amp; parts</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all in Motors</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><button class="category-item has-sub-category">
        <div class="category-item-img for-sale"></div>
        <div class="category-item-title">For Sale</div>
        <div class="category-item-desc">Everyday items</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all For Sale</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><button class="category-item has-sub-category">
        <div class="category-item-img properties"></div>
        <div class="category-item-title">Properties</div>
        <div class="category-item-desc">Homes for all</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all in Properties</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><button class="category-item has-sub-category">
        <div class="category-item-img jobs"></div>
        <div class="category-item-title">Jobs</div>
        <div class="category-item-desc">Discover your next role</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all in Jobs</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><button class="category-item has-sub-category">
        <div class="category-item-img services"></div>
        <div class="category-item-title">Services</div>
        <div class="category-item-desc">From trades to tutors</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all in Services</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><button class="category-item has-sub-category">
        <div class="category-item-img community"></div>
        <div class="category-item-title">Community</div>
        <div class="category-item-desc">Bands, clubs &amp; classes</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all in Community</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><button class="category-item has-sub-category">
        <div class="category-item-img pets"></div>
        <div class="category-item-title">Pets</div>
        <div class="category-item-desc">Find a cute pet</div>
        <div class="sub-category">
            <div class="sub-category-title"><a href="#">View all in Pets</a></div>
            <div class="sub-category-content"><div class="sub-category-inline"><a href="#">Cars</a></div><div class="sub-category-inline"><a href="#">Camper-vans & Motor-homes</a></div><div class="sub-category-inline"><a href="#">Plant & Tractors</a></div><div class="sub-category-inline"><a href="#">Parts</a></div><div class="sub-category-inline"><a href="#">Motorbikes & Scooters</a></div><div class="sub-category-inline"><a href="#">Caravans</a></div><div class="sub-category-inline"><a href="#">Other Vehicles</a></div><div class="sub-category-inline"><a href="#">Wanted</a></div><div class="sub-category-inline"><a href="#">Vans</a></div><div class="sub-category-inline"><a href="#">Trucks</a></div><div class="sub-category-inline"><a href="#">Accessories</a></div></div>
        </div>
    </button><span class="category-item divider"></span><a href="{{ route('dating') }}" class="category-item">
        <div class="category-item-img dating"></div>
        <div class="category-item-title">Dating</div>
        <div class="category-item-desc">Find a partner</div>
    </a>
</div>