<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function settings()
    {
        $user = ( Auth::user() ) ? Auth::user() : '';
        if ( $user->email_verified_at == null ) {
            $email_verified = false;
            $status = '<span class="badge badge-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Not verified!</span>';
        } else {
            $email_verified = true;
            $status = '<span class="badge badge-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Verified!</span>';
        }
        $data = [
            'user' => $user,
            'email_verified' => $email_verified,
            'status' => $status
        ];
        return view('user.settings', $data);
    }

    public function update(Request $request)
    {
        $user = Auth::user() or die();
        $user_id = $user->id;

        // Validate
        $this->validate($request, [
            '_type' => 'required',
            'body' => 'required'
        ]);

        // Update post
        #$post = Blog::find($id);
        #$post->title = $request->input('title');
        #$post->body = $request->input('body');
        #$post->save();


        // Redirect to posts page
        #return redirect('/posts')->with('success', 'Blog post updated successfully!');
    }

    public function change_password(Request $request)
    {
        if( request()->ajax() ) {
            header("Content-Type: text/json");
        }
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|min:4',
            'con_password' => 'required|same:new_password'
        ];
        $messages = [
            'old_password.required'     => 'Current password is required!',
            'new_password.required'     => 'New password is required!',
            'new_password.min'          => 'The password must have minimum 8 characters long!',
            'con_password.required'     => 'Please retype the password for confirmation!',
            'con_password.same'         => 'Passwords to not match!'
        ];

        $this->validate($request, $rules, $messages);

        $success_msg = 'Password has been updated!';

        if ( !Auth()->guest() ) {
            $user = Auth()->user();
            $current_password = $user->password;
            if (Hash::check($request->input('old_password'), $current_password)) {
                $fetch_user = User::find($user->id);
                $fetch_user->password = Hash::make($request->input('new_password'));
                $fetch_user->save();
                return response()->json(['message' => $success_msg], 200);
            } else {
                $error_msg = 'The current password you entered is incorrect!';
                $data = [
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'old_password' => $error_msg
                    ]
                ];
                return response()->json($data, 422);
            }

            /*if( request()->ajax() ) {

            } else {
                return redirect()->route('profile')->with('message', $success_msg);
            }*/
        } else {
            $error_msg = 'Please login first!';
            $data = [
                'message' => 'Please login!',
                'errors' => [
                    'no_user_login' => $error_msg
                ]
            ];
            return response()->json($data, 422);
        }
    }

    public function add_number(Request $request)
    {
        if( request()->ajax() ) {
            header("Content-Type: text/json");
        }
        $rules = [
            'contact_number' => 'required|numeric|max:999999999999'
        ];
        $messages = [
            'contact_number.required' => 'Contact number is required!',
            'contact_number.numeric'  => 'Contact number must only be numeric!',
            'contact_number.max'      => 'Contact number can contain max 14 numbers!'
        ];

        $this->validate($request, $rules, $messages);

        $success_msg = 'Contact number has been added!';

        if ( !Auth()->guest() ) {
            $user = Auth()->user();
            $fetch_user = User::find($user->id);
            $fetch_user->phone = $request->input('contact_number');
            $fetch_user->save();
            return response()->json(['message' => $success_msg], 200);

            #return redirect()->route('profile')->with('message', $success_msg);
        } else {
            $error_msg = 'Please login first!';
            $data = [
                'message' => 'Please login!',
                'errors' => [
                    'no_user_login' => $error_msg
                ]
            ];
            return response()->json($data, 422);
        }
    }

    public function search_history()
    {
        return view('user.search_history');
    }

    public function saved_searches()
    {
        return view('user.saved_searches');
    }

    public function viewed_properties()
    {
        return view('user.viewed_properties');
    }

    public function saved_properties()
    {
        return view('user.saved_properties');
    }

    public function favourites()
    {
        return view('user.favourites');
    }

    public function messages()
    {
        return view('user.messages');
    }

    public function alerts()
    {
        return view('user.alerts');
    }
}
