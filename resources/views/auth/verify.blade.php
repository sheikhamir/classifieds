@extends('layouts.empty')

@section('title')
Verify Email Address - {{ config('app.name') }}
@endsection

@section('body-content')
<div class="container-fluid normal verify-email-box-container inputs-have-no-radius">
    <div class="row noMarginRow">{{-- This will only be visible on the "layout.nothing" layout --}}
        <div class="col-xs-12">
            <div class="logo-container show-at-nothing">
                <a href="{{ route('index') }}" class="logo"></a>
            </div>
        </div>
    </div>
    <div class="row noMarginRow">
        <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <div class="verify-email-box">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
