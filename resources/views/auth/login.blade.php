@extends('layouts.empty')

@section('title')
Login - {{ config('app.name') }}
@endsection

@section('body-content')
<div class="container-fluid normal login-form-container inputs-have-no-radius">
    <div class="login-form">
        <h1 class="text-center">{{ __('Login') }}</h1>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row noMarginRow input-container">
                <div class="col-sm-12">
                    @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            <strong>The email or password is incorrect!</strong>
                        </div>
                    @elseif( session('error') )
                        <div class="alert alert-danger text-center">
                            {{ session('error') }}
                        </div>
                    @else
                        <div class="alert alert-normal">Sign in with your credentials below</div>
                    @endif
                </div>
            </div>
            <div class="row noMarginRow input-container">
                <div class="col-sm-12">
                    <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="{{ 'example@example.com' }}" required autofocus>
                </div>
            </div>
            <div class="row noMarginRow input-container">
                <div class="col-sm-12">
                    <input id="password" type="password" class="form-control input-lg" name="password" placeholder="Password" required>
                </div>
            </div>

            <div class="row noMarginRow input-container">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-lg btn-theme btn-block">
                        {{ __('Sign In') }}
                    </button>
                </div>
            </div>
            <div class="row noMarginRow input-container">
                <div class="col-md-12">
                    <a href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    <div class="remember-box pull-right">
                        <input class="inline check" type="checkbox" name="remember" id="_remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="_remember" class="inline">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
