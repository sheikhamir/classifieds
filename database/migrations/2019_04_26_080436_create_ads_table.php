<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('adId', 16);
            $table->string('adTitle', 70);
            $table->index(['adTitle']);
            $table->mediumText('adDescription');
            $table->string('adSlug');
            $table->double('adAskingPrice');
            $table->integer('userId')->unsigned();
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->string('postalCode');
            $table->string('city')->nullable(); // Newly added
            $table->string('county')->nullable(); // Newly added
            $table->string('district')->nullable(); // Newly added
            $table->string('country')->nullable(); // Newly added
            $table->string('address')->nullable(); // Newly added
            $table->string('category'); // category
            $table->string('subCategory'); // sub category
            $table->string('extraSubCategory')->nullable(); // sub-sub category
            $table->string('extraExtraSubCategory')->nullable(); // Sub-sub-sub category
            $table->smallInteger('featured')->default(0); // Keep Ad on top with FEATURED label
            $table->smallInteger('urgent')->default(0); // Add URGENT label on the Ad
            $table->smallInteger('spotlight')->default(0); // Show Ad on homepage
            $table->smallInteger('live')->default(1); // Ad live or not
            $table->smallInteger('withPicture'); // Ad has pictures or not
            $table->smallInteger('sellerType'); // Seller type [1=normal,2=business)
            $table->string('videoUrl')->nullable();
            $table->integer('views')->default(0);
            $table->timestamps();
        });
        DB::table('ads')->insert(array(
            'adId' => 1,
            'adTitle' => 'Yamaha F310',
            'adDescription' => 'Yamaha F310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition Yamaha F-310 for sale in Excellent condition ',
            'adSlug' => 'yamaha-f310-1454896',
            'adAskingPrice' => '140',
            'userId' => 1,
            'postalCode' => 'OX495NU',
            'city' => 'Brightwell Baldwin',
            'county' => 'Oxfordshire',
            'district' => 'South Oxfordshire District',
            'country' => 'England',
            'address' => 'Cadwell Lane',
            'category' => 'for-sale',
            'subCategory' => 'music-instruments-and-dj-equipment',
            'extraSubCategory' => 'guitars-and-accessories',
            'extraExtraSubCategory' => 'guitars',
            'featured' => '1',
            'urgent' => '0',
            'spotlight' => '0',
            'live' => '1',
            'withPicture' => '1',
            'created_at' => '2019-04-25 22:19:51',
            'updated_at' => '2019-04-25 22:19:51'
        ));
        DB::table('ads')->insert(array(
            'adId' => 2,
            'adTitle' => 'Yamaha R1',
            'adDescription' => 'Yamaha R1 for sale in excellent condition',
            'adSlug' => 'yamaha-r1-184956',
            'adAskingPrice' => '8000',
            'userId' => 1,
            'postalCode' => 'OX495NU',
            'city' => 'Brightwell Baldwin',
            'county' => 'Oxfordshire',
            'district' => 'South Oxfordshire District',
            'country' => 'England',
            'address' => 'Cadwell Lane',
            'category' => 'motors',
            'subCategory' => 'motorbikes-and-scooters',
            'featured' => '0',
            'urgent' => '0',
            'spotlight' => '0',
            'live' => '1',
            'withPicture' => '1',
            'created_at' => '2019-04-25 22:19:51',
            'updated_at' => '2019-04-25 22:19:51'
        ));
        $longData = 'YAMAHA R6 2008 MODEL

RAD LEGAL WITH V5 X2 KEYS

7,000 MILES

BUILT FOR THE BSB SUPERSTOCK RACE CLASS

WHEN LAST USED A FEW YEARS AGO THIS WON A SUPERSTOCK RACE!

THIS HAS HAS £1000S SPENT TO GET IT FULLY SET UP.

QUICK LIST

FULL TRITON SUSPENSION FRONT AND REAR SET UP BEEN TOLD COST £4000 ALONE

FULL RACE EXHAUST SYSTEM POSSIBLY (ARROW) LOOKS TITANIUM

FULL RACE FAIRINGS AND ALSO X2 MORE FULL KITS AVAILABLE!

CNC ADJUSTABLE REAR SETS

GPR ADJUSTABLE STEERING DAMPER

RACE SHIFT GEAR BOX

SLIPPER CLUTCH FITTED

SOME VERY TRICK FRONT BRAKE SET UP

ENGINE HAS BEEN PLAYED WITH ONLY LEGAL MODS FOR THE CHAMPIONSHIP RULES AND BEEN TOLD VERY FAST MOTOR AND LOW MILES!

THERE IS SO MUCH TO LIST I WOULD RECOMMEND SOME ONE VIEWING!

£7195.00 AS IT IS

£7,800 WITH SPARE WHEELS AND WETS, ANOTHER SET OF WHEELS, A FULL BOX OF RENTHAL SPROCKETS FRONT AND REAR, SPARE BRAKE PADS,
X2 SETS OF RACE FAIRINGS CLIP ONS STD CLUTCH KIT TO MUCH TO LIST

CAN BE PUT BACK ON THE ROAD AS DAYTIME MOT IF YOU WANTED TO RIDE IT TO A TRACK!



THIS BIKE IS READY TO GO RACING JUST NEEDS A GOOD PILOT!



RUNS AND RIDES GREAT

GREAT CONDITION THROUGH OUT

THIS BIKE WILL BE FULLY SERVICED AND CLEANED BEFORE THE SALE! THIS HAS JUST ARRIVED AND WILL NOT HANG AROUND!

PX WELCOME ALL BIKES CONSIDERED/CARS/JETSKIS/BOATS!

CAN DELIVER ANYWHERE IN THE UK FROM
JUST £49.99

FINANCE AVAILABLE

[Website URL removed]
[Website URL removed]
[Website URL removed]
[Website URL removed]

ANYMORE INFO CALL 01243 829521
WHATTS APP PXS 07463701010

MUST HAVE APPOINTMENT TO VIEW PLEASE CALL!!



About Us:
We do things different at cw bikes ltd :

All Bike\'s are HPI checked

Cleaned & valeted before delivery/collection

Part Exchange Welcome motorcycles/scooters/cars/vans Email your part exchange inquires to [Email address removed. Click Email button to contact us.] or call one of the team on 01243 696886

Nationwide Delivery

from off road bikes/ road bikes/ buggy/quads/pitbikes/jetskis
Full dealer facilities, Visa/Mastercard accepted, Px Welcome, Finance Available, call one of the team at Chichester Motorcycles on 01243 829521 or email us at [Email address removed. Click Email button to contact us.] web [Website URL removed]

Want To Part Exchange Your Current Bike? We aim to offer you the very best price for your part exchange, whatever the make or model.

Have Your New Bike Delivered to Your Door! We deliver bikes throughout mainland UK so we can bring your new bike straight to your door (and collect your part-exchange)

cw bikes ltd
rear of homelea
bognor regis
west sussex
po21 2dn

Opening Hours:
Mo: 09:00 AM to 06:00 PM
Tu: 09:00 AM to 06:00 PM
We: 09:00 AM to 06:00 PM
Th: 09:00 AM to 06:00 PM
Fr: 09:00 AM to 06:00 PM
Sa: 09:00 AM to 04:00 PM
Su: Closed';
        DB::table('ads')->insert(array(
            'adId' => 3,
            'adTitle' => 'Yamaha R6 Track Bike SUPERSTOCK',
            'adDescription' => $longData,
            'adSlug' => 'yamaha-r6-164587',
            'adAskingPrice' => '6000',
            'userId' => 1,
            'postalCode' => 'OX495NU',
            'city' => 'Brightwell Baldwin',
            'county' => 'Oxfordshire',
            'district' => 'South Oxfordshire District',
            'country' => 'England',
            'address' => 'Cadwell Lane',
            'category' => 'motors',
            'subCategory' => 'motorbikes-and-scooters',
            'featured' => '1',
            'urgent' => '1',
            'spotlight' => '1',
            'live' => '1',
            'withPicture' => '1',
            'created_at' => '2019-04-25 22:19:51',
            'updated_at' => '2019-04-25 22:19:51'
        ));
        DB::table('ads')->insert(array(
            'adId' => 4,
            'adTitle' => 'Yamaha MM1',
            'adDescription' => 'Yamaha MM1 for sale in excellent condition',
            'adSlug' => 'yamaha-mm1-92873',
            'adAskingPrice' => '900',
            'userId' => 1,
            'postalCode' => 'OX495NU',
            'city' => 'Brightwell Baldwin',
            'county' => 'Oxfordshire',
            'district' => 'South Oxfordshire District',
            'country' => 'England',
            'address' => 'Cadwell Lane',
            'category' => 'motors',
            'subCategory' => 'cars',
            'featured' => '0',
            'urgent' => '1',
            'spotlight' => '0',
            'live' => '1',
            'withPicture' => '1',
            'created_at' => '2019-04-25 22:19:51',
            'updated_at' => '2019-04-25 22:19:51'
        ));
        DB::table('ads')->insert(array(
            'adId' => 5,
            'adTitle' => 'Casio tone Piano',
            'adDescription' => 'Casio Tone piano for sale in immaculate condition',
            'adSlug' => 'casio-tone-11587',
            'adAskingPrice' => '200',
            'userId' => 1,
            'postalCode' => 'OX495NU',
            'city' => 'Brightwell Baldwin',
            'county' => 'Oxfordshire',
            'district' => 'South Oxfordshire District',
            'country' => 'England',
            'address' => 'Cadwell Lane',
            'category' => 'for-sale',
            'subCategory' => 'music-instruments-and-dj-equipment',
            'extraSubCategory' => 'keyboards-and-pianos',
            'extraExtraSubCategory' => 'electric-keyboards',
            'featured' => '0',
            'urgent' => '0',
            'spotlight' => '1',
            'live' => '1',
            'withPicture' => '1',
            'created_at' => '2019-04-25 22:19:51',
            'updated_at' => '2019-04-25 22:19:51'
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->dropIndex(['adTitle']);
            $table->dropForeign(['userId','category','subCategory']);
        });
        Schema::dropIfExists('ads');
    }
}
