<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@yield('keywords', 'classifieds,for-sale,for sale,motors,vehicles,cars,bikes,motorcycles,trucks,parts,auto parts,pets,jobs,dating')">
    <meta name="description" content="@yield('description', 'Search for items for sale in the U.K! U.K\'s best classified website to find new and used goods!')">
    <meta name="theme-color" content="#570075">
    <title>@yield('title', 'MyClassified - Buy and sell online')</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-31252215-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-31252215-3');
    </script>
    <link rel="stylesheet" href="{{asset('css/init.css')}}">
    <link rel="stylesheet" href="{{asset('css/empty.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Google fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Cabin|Montserrat" rel="stylesheet">-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        //CKEDITOR.replace( 'article-ckeditor' );
    </script>
    <style>html.unloaded{background:#fff}html.unloaded:after{content:"Loading...";display:block;height:1000%;width:1000%;position:fixed;font-size:20px;text-align:left;transform:translate(-45%,44%);z-index:1929;left:0;bottom:0;right:0;background:#fff url(https://ui-ex.com/images/background-transparent-loading-3.gif) center no-repeat;background-size:200px 200px}</style>
</head>