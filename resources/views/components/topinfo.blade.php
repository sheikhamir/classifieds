<div class="container-fluid constrained-width">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="top-info-title searches"><a href="#" class="active" dataopen=".top-info-links.searches">Top searches <i class="glyphicon glyphicon-chevron-down visible-xs-inline"></i></a></div><div class="top-info-links searches">
            {{--<a href="#">cars</a><span class="divider"><!--&#124;--></span><a href="#">free</a><span class="divider"><!--&#124;--></span><a href="#">puppies</a><span class="divider"><!--&#124;--></span><a href="#">kittens</a><span class="divider"><!--&#124;--></span><a href="#">office floor to rent</a><span class="divider"><!--&#124;--></span><a href="#">sofa</a><span class="divider"><!--&#124;--></span><a href="#">garden</a><span class="divider"><!--&#124;--></span><a href="#">cheap cars</a><span class="divider"><!--&#124;--></span><a href="#">dogs</a><span class="divider"><!--&#124;--></span><a href="#">vans</a><span class="divider"><!--&#124;--></span><a href="#">room</a><span class="divider"><!--&#124;--></span><a href="#">caravan</a><span class="divider"><!--&#124;--></span><a href="#">lego</a><span class="divider"><!--&#124;--></span><a href="#">boat</a><span class="divider"><!--&#124;--></span><a href="#">4x4</a><span class="divider"><!--&#124;--></span><a href="#">trailer</a><span class="divider"><!--&#124;--></span><a href="#">tv</a><span class="divider"><!--&#124;--></span><a href="#">motorcycle</a><span class="divider"><!--&#124;--></span><a href="#">7 seater</a><span class="divider"><!--&#124;--></span><a href="#">125cc</a><span class="divider"><!--&#124;--></span><a href="#">house</a><span class="divider"><!--&#124;--></span><a href="#">furniture</a><span class="divider"><!--&#124;--></span><a href="#">massage</a><span class="divider"><!--&#124;--></span><a href="#">bmw</a><span class="divider"><!--&#124;--></span><a href="#">table</a><span class="divider"><!--&#124;--></span><a href="#">room to rent</a><span class="divider"><!--&#124;--></span><a href="#">chair</a><span class="divider"><!--&#124;--></span><a href="#">2 bedroom house</a><span class="divider"><!--&#124;--></span><a href="#">chest of drawers</a><span class="divider"><!--&#124;--></span><a href="#">flat</a>--}}
            {{ fetch_top_searches( 25 ) }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="top-info-title locations"><a href="#" class="" dataopen=".top-info-links.locations">Top locations <i class="glyphicon glyphicon-chevron-down visible-xs-inline"></i></a></div><div class="top-info-links locations">
            {{ fetch_top_locations( 25 ) }}
            </div>
        </div>
    </div>
</div>