@extends('layouts.app')

@section('title', 'Property View')

@section('body-content')
    <div class="container add-top-gap-sm margin-bottom-small">
        @if(count($ads) > 0)
            @foreach($ads as $ad)
                <a href="/posts/{{$ad->adId}}" class="list-group-item">
                    <h3>{{$ad->adTitle}}</h3>
                    <small>Created on {{$ad->created_at}}</small>
                </a>
            @endforeach
            {{-- Displays the pagination links --}}
            {{--{{$posts->links()}}--}}
        @else
            <h1>No Ads Found</h1>
        @endif
    </div>
@endsection