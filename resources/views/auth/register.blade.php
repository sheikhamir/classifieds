@extends('layouts.empty')

@section('title')
Register - {{ config('app.name') }}
@endsection

@section('body-content')
<div class="container-fluid normal register-form-container inputs-have-no-radius">
    <div class="row noMarginRow register-form">
        <div class="col-xs-12">
            <div class="row noMarginRow input-container">
                <div class="col-sm-12">
                    <h3 class="text-center">{{ __('Create an account') }}</h3>
                </div>
            </div>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="row noMarginRow input-container">
                    <div class="col-sm-12">
                        <input id="name" type="text" class="form-control fullname{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Full Name" required>
                        @if ($errors->has('name'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row noMarginRow input-container">
                    <div class="col-sm-12">
                        <input id="email" type="email" class="form-control email{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                        @if ($errors->has('email'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row noMarginRow input-container">
                    <div class="col-sm-12">
                        <input id="password" type="password" class="form-control password{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row noMarginRow input-container">
                    <div class="col-sm-12">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                    </div>
                </div>

                <div class="row noMarginRow input-container agree-to-policy-container{{ $errors->has('agree_to_policy') ? ' bg-danger has-error' : '' }}">
                    <div class="col-sm-12">
                        <div class="policy-agree-box">
                            <input class="checkbox" type="checkbox" name="agree_to_policy" id="agree_to_policy" value="1"/><label for="agree_to_policy" class="checkbox">
                                I have read and accept the <a href="#">Policy & Terms</a>.
                            </label>
                            @if ($errors->has('agree_to_policy'))
                                <div class="error policy-accept-error">
                                    <strong>You must agree to our Policy & Terms!</strong>
                                </div>
                                <script type="text/javascript">
                                $(function(){
                                    setTimeout(function(){
                                        $('#agree_to_policy').focus();
                                    },100);
                                    setTimeout(function(){
                                        $('.agree-to-policy-container').removeClass('bg-danger');
                                        $('.policy-accept-error').html('<strong>Please read and accept our Policy and Terms!</strong>');
                                    }, 2000);
                                });
                                </script>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row noMarginRow input-container">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-theme btn-block">
                            {{ __('Create an account') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    if($('input').hasClass('is-invalid')) {
        if($('.is-invalid').hasClass('email')) {
            $('#email.is-invalid').select().focus();
        } else if ($('.is-invalid').hasClass('password')) {
            $('.password').focus();
        }
    } else {
        $('#name').focus();
    }
});
</script>
@endsection
